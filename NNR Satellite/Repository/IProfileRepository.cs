﻿using NNR_Satellite.Models;

namespace NNR_Satellite.Repository
{
    public interface IProfileRepository : IGenericRepository<Profile>
    {

    }
}