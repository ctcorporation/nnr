﻿using NNR_Satellite.Models;

namespace NNR_Satellite.Repository
{
    public interface IProcessingErrorRepository : IGenericRepository<Processing_Errors>
    {

    }
}