﻿using NNR_Satellite.Models;

namespace NNR_Satellite.Repository
{
    public interface IImportedConsolRepository : IGenericRepository<Imported_Consol>
    {
        SatelliteEntity SatelliteEntity { get; }
    }
}