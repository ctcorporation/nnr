﻿using NNR_Satellite.Models;

namespace NNR_Satellite.Repository
{
    public class ImportedShipmentRepository : GenericRepository<Imported_Shipments>, IImportedShipmentRepository
    {
        public ImportedShipmentRepository(SatelliteEntity context)
            : base(context)
        {

        }
        public SatelliteEntity SatelliteEntity
        {
            get
            {
                return Context as SatelliteEntity;
            }
        }

    }
}
