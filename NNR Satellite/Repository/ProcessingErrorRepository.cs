﻿using NNR_Satellite.Models;

namespace NNR_Satellite.Repository
{
    public class ProcessingErrorRepository : GenericRepository<Processing_Errors>, IProcessingErrorRepository
    {
        public ProcessingErrorRepository(SatelliteEntity context)
            : base(context)
        {

        }

        private SatelliteEntity SatelliteEntity
        {
            get
            {
                return Context as SatelliteEntity;
            }
        }
    }
}
