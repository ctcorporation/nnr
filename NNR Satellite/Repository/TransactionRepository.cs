﻿using NNR_Satellite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NNR_Satellite.Repository
{
    public class TransactionRepository : GenericRepository<Transaction>,ITransactionRepository
    {
        public TransactionRepository(SatelliteEntity context)
            : base(context)
        {

        }

        private SatelliteEntity SatelliteEntity
        {
            get { return Context as SatelliteEntity; }
        }
    }
}
