﻿using NNR_Satellite.Models;

namespace NNR_Satellite.Repository
{
    public interface ICargowiseEnumRepository : IGenericRepository<Cargowise_Enums>
    {
        SatelliteEntity SatelliteEntity { get; }
    }
}