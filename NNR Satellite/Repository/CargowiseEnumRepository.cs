﻿using NNR_Satellite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NNR_Satellite.Repository
{
    public class CargowiseEnumRepository : GenericRepository<Cargowise_Enums>, ICargowiseEnumRepository
    {
        public CargowiseEnumRepository(SatelliteEntity context)
            :base(context)
        {

        }

        public SatelliteEntity SatelliteEntity
        {
            get
            {
                return Context as SatelliteEntity;
            }
        }
    }
}
