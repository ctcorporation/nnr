﻿using NNR_Satellite.Models;

namespace NNR_Satellite.Repository
{
    public class CustomerProfileViewRepository : GenericRepository<vw_CustomerProfile>, ICustomerProfileViewRepository
    {
        public CustomerProfileViewRepository(SatelliteEntity context)
            : base(context)
        {

        }

        private CNodeEntity CNodeEntity
        {
            get { return Context as CNodeEntity; }
        }
    }
}
