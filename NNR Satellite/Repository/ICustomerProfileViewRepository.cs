﻿using NNR_Satellite.Models;

namespace NNR_Satellite.Repository
{
    public interface ICustomerProfileViewRepository : IGenericRepository<vw_CustomerProfile>
    {

    }
}