﻿using NNR_Satellite.Models;

namespace NNR_Satellite.Repository
{
    public interface IImportedShipmentRepository : IGenericRepository<Imported_Shipments>
    {
        SatelliteEntity SatelliteEntity { get; }
    }
}