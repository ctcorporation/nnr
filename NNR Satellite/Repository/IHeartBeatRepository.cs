﻿namespace NNR_Satellite.Repository
{
    public interface IHeartBeatRepository : IGenericRepository<Models.HeartBeat>
    {

    }
}