﻿using NNR_Satellite.Models;

namespace NNR_Satellite.Repository
{
    public class CustomerRepository : GenericRepository<Models.Customer>, ICustomerRepository
    {
        public CustomerRepository(CNodeEntity context)
            : base(context)
        {

        }

        private CNodeEntity CNodeEntity
        {
            get { return Context as CNodeEntity; }
        }
    }
}
