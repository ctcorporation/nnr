﻿using NNR_Satellite.Models;

namespace NNR_Satellite.Repository
{
    public class ProfileRepository : GenericRepository<Profile>, IProfileRepository
    {
        public ProfileRepository(SatelliteEntity context)
            : base(context)
        {

        }

        private SatelliteEntity SatelliteEntity
        {
            get
            {
                return Context as SatelliteEntity;
            }
        }
    }
}
