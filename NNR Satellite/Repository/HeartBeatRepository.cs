﻿using NNR_Satellite.Models;

namespace NNR_Satellite.Repository
{
    public class HeartBeatRepository : GenericRepository<Models.HeartBeat>, IHeartBeatRepository
    {
        public HeartBeatRepository(CNodeEntity context)
            : base(context)
        {

        }

        private CNodeEntity CNodeEntity
        {
            get { return Context as CNodeEntity; }
        }
    }
}
