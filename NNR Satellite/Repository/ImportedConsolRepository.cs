﻿using NNR_Satellite.Models;

namespace NNR_Satellite.Repository
{
    public class ImportedConsolRepository : GenericRepository<Imported_Consol>, IImportedConsolRepository
    {
        public ImportedConsolRepository(SatelliteEntity context)
            : base(context)
        {

        }

        public SatelliteEntity SatelliteEntity
        {
            get
            {
                return Context as SatelliteEntity;
            }
        }

    }
}
