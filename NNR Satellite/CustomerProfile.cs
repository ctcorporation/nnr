﻿using NNR_Satellite.DTO;
using NNR_Satellite.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NNR_Satellite
{
    public class CustomerProfile
    {
        #region members
        private string _connstring;
        #endregion

        #region properties
        public Guid CustID
        {
            get;
        }
        public string ConnString
        {
            get
            {
                return _connstring;
            }
            set
            {
                _connstring = value;
            }
        }
        #endregion

        #region constructors
        public CustomerProfile()
        {

        }
        public CustomerProfile(string connString)
        {
            _connstring = connString;
        }

        #endregion

        #region methods

        public vw_CustomerProfile GetCustomerProfile(string senderId, string recipientId, string reasonCode, string eventCode, string isActive)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteEntity(_connstring)))
            {
                var custProfile = (uow.CustomerProfileViews.Find(v => v.P_SENDERID == senderId
                                                                && v.P_RECIPIENTID == recipientId
                                                                && v.C_IS_ACTIVE == isActive
                                                                && (string.IsNullOrEmpty(reasonCode) || v.P_REASONCODE == reasonCode)
                                                                && (string.IsNullOrEmpty(eventCode) || v.P_EVENTCODE == eventCode)
                                                                )).FirstOrDefault();
                if (custProfile != null)
                {
                    return custProfile;
                }
                else
                {
                    Profile profile = new Profile
                    {
                        P_C = CustID,
                        P_EVENTCODE = eventCode,
                        P_REASONCODE = reasonCode,
                        P_DELIVERY = "R",
                        P_MSGTYPE = "Original",
                        P_BILLTO = senderId,
                        P_SENDERID = senderId,
                        P_RECIPIENTID = recipientId,
                        P_CHARGEABLE = "Y",
                        P_ACTIVE = "Y",
                        P_DESCRIPTION = "No Transport Profile found. Store in Folder",

                    };
                    uow.Profile.Add(profile);
                    uow.Complete();
                    GetCustomerProfile(senderId, recipientId, reasonCode, eventCode, isActive);
                }

            }
            return null;
        }

        //where P_XSD is not null and P_ACTIVE='Y'
        public List<vw_CustomerProfile> GetXMLProfiles()
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteEntity(_connstring)))
            {
                var profiles = uow.CustomerProfileViews.Find(v => v.P_XSD != null && v.P_ACTIVE == "Y").ToList();
                return profiles;
            }
        }
        #endregion

        #region helpers

        #endregion


    }
}
