﻿using NNR_Satellite.Models;
using NNR_Satellite.Repository;
using System;

namespace NNR_Satellite.DTO
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SatelliteEntity _context;
        private bool _disposed = false;

        public UnitOfWork(SatelliteEntity context)
        {
            _context = context;
            CustomerProfileViews = new CustomerProfileViewRepository(_context);
            Profile = new ProfileRepository(_context);
            ProcessingErrors = new ProcessingErrorRepository(_context);
            ImportedShipments = new ImportedShipmentRepository(_context);
            ImportedConsols = new ImportedConsolRepository(_context);
            CargowiseEnums = new CargowiseEnumRepository(_context);
            Transactions = new TransactionRepository(_context);
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }



        #region Repositories
        public ITransactionRepository Transactions
        {
            get;
            private set;
        }
        public ICustomerProfileViewRepository CustomerProfileViews
        {
            get;
            private set;
        }

        public IProfileRepository Profile
        {
            get;
            private set;
        }

        public IProcessingErrorRepository ProcessingErrors
        {
            get;
            private set;
        }
        public IImportedShipmentRepository ImportedShipments
        {
            get;
            private set;
        }
        public IImportedConsolRepository ImportedConsols
        {
            get;
            private set;
        }

        public ICargowiseEnumRepository CargowiseEnums
        {
            get;
            private set;
        }
        #endregion

        #region Cleanup
        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                _context.Dispose();
            }
            _disposed = true;

        }
        public void Dispose()
        {
            if (_disposed)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }
        #endregion

    }
}
