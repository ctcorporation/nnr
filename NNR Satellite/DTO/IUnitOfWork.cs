﻿using NNR_Satellite.Repository;
using System;

namespace NNR_Satellite.DTO
{
    public interface IUnitOfWork : IDisposable
    {
        ITransactionRepository Transactions { get; }
        ICustomerProfileViewRepository CustomerProfileViews { get; }
        IProfileRepository Profile { get; }
        IProcessingErrorRepository ProcessingErrors { get; }
        IImportedShipmentRepository ImportedShipments { get; }
        IImportedConsolRepository ImportedConsols { get; }
        ICargowiseEnumRepository CargowiseEnums { get; }
        int Complete();
    }
}