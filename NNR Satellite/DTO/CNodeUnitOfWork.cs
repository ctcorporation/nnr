﻿using NNR_Satellite.Models;
using NNR_Satellite.Repository;
using System;

namespace NNR_Satellite.DTO
{
    public class CNodeUnitOfWork : ICNodeUnitOfWork
    {
        private readonly CNodeEntity _context;
        private bool _disposed = false;

        public CNodeUnitOfWork(CNodeEntity context)
        {
            _context = context;
            HeartBeat = new HeartBeatRepository(_context);
            CustRepository = new CustomerRepository(_context);
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }



        #region Repositories
        public IHeartBeatRepository HeartBeat
        {
            get;
            private set;
        }

        public ICustomerRepository CustRepository
        {
            get;
            private set;
        }
        #endregion

        #region Cleanup
        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                _context.Dispose();
            }
            _disposed = true;

        }
        public void Dispose()
        {
            if (_disposed)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }
        #endregion

    }
}