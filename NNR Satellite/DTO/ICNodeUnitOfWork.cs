﻿using NNR_Satellite.Repository;
using System;

namespace NNR_Satellite.DTO
{
    public interface ICNodeUnitOfWork : IDisposable
    {
        ICustomerRepository CustRepository { get; }
        IHeartBeatRepository HeartBeat { get; }

        int Complete();
        void Dispose();
        void Dispose(bool disposing);
    }
}