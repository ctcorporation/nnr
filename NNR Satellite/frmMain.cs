﻿using CNodeBE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using static NNR_Satellite.SatelliteErrors;

namespace NNR_Satellite
{

    public partial class frmMain : Form
    {



        public SqlConnection sqlConn, sqlCTCConn;
        public int totfilesRx;
        public int totfilesTx;
        public int cwRx;
        public int cwTx;
        public int filesRx;
        public int filesTx;
        public System.Timers.Timer tmrMain;
        public frmMain()
        {
            InitializeComponent();
            tmrMain = new System.Timers.Timer();
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);

        }

        private void OnProcessExit(object sender, EventArgs e)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, "Stopping", m.GetParameters());
        }

        public static DateTime? TryParse(string text) =>
    DateTime.TryParse(text, out var date) ? date : (DateTime?)null;

        private void tmrMain_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (btnTimer.Text == "&Stop")
            {
                tmrMain.Stop();
                GetFiles("Message_MLASYDSYD *.xml");
                GetFiles("*.*");
                tmrMain.Start();
            }
            else
            {
                tmrMain.Stop();
            }


        }

        private void btnTimer_Click(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "&Start")
            {
                ((Button)sender).Text = "&Stop";
                ((Button)sender).BackColor = Color.LightCoral;
                tslMain.Text = "Timed Processes Started";
                GetFiles("Message_MLASYDSYD*.xml");
                GetFiles("*.*");
                tmrMain.Start();
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Waiting for Files.");
            }
            else
            {
                ((Button)sender).Text = "&Start";
                ((Button)sender).BackColor = Color.LightGreen;
                tslMain.Text = "Timed Processes Stopped";
                tmrMain.Stop();
            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void clearLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbLog.Text = "";
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout AboutBox = new frmAbout();
            AboutBox.ShowDialog();
            LoadSettings();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

            LoadSettings();
            tslMode.Text = "Production";
            productionToolStripMenuItem.Checked = true;
            HeartBeat.RegisterHeartBeat(Globals.glCustCode, "Starting", null);
            this.tslMain.Width = this.Width / 2;
            this.tslSpacer.Width = (this.Width / 2) - (productionToolStripMenuItem.Width + tslCmbMode.Width);


        }

        private void LoadSettings()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appDataPath, System.Diagnostics.Process.GetCurrentProcess().ProcessName);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            tmrMain.Interval = (1000 * 5) * 60;
            tmrMain.Elapsed += new ElapsedEventHandler(tmrMain_Elapsed);
            Globals.glAppConfig = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".XML";
            Globals.glAppConfig = Path.Combine(path, Globals.glAppConfig);
            frmSettings Settings = new frmSettings();
            if (!File.Exists(Globals.glAppConfig))
            {
                XDocument xmlConfig = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "Yes"),
                    new XElement("Satellite",
                    new XElement("Customer"),
                    new XElement("Database"),
                    new XElement("CTCNode"),
                    new XElement("Communications")));
                xmlConfig.Save(Globals.glAppConfig);
                Settings.ShowDialog();
            }
            else
            {
                try
                {
                    bool loadConfig = false;
                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load(Globals.glAppConfig);
                    XmlNodeList nodeList = xmlConfig.SelectNodes("/Satellite/Database");
                    XmlNode nodeCat = nodeList[0].SelectSingleNode("Catalog");
                    if (nodeCat != null)
                    {
                        Globals.glDbInstance = nodeCat.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlServer = nodeList[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.glDbServer = xmlServer.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlUser = nodeList[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.glDbUserName = xmlUser.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlPassword = nodeList[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.glDbPassword = xmlPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    sqlConn = new SqlConnection();
                    sqlConn.ConnectionString = Globals.connString();
                    nodeList = null;
                    nodeList = xmlConfig.SelectNodes("/Satellite/Customer");
                    XmlNode xCustCode = nodeList[0].SelectSingleNode("Code");
                    if (xCustCode != null)
                    {
                        Globals.glCustCode = xCustCode.InnerText;
                        Globals.Customer cust = new Globals.Customer();
                        cust = Globals.GetCustomer(Globals.glCustCode);
                        if (cust != null)
                        {
                            Globals.glCustCode = cust.Code;
                            Globals.gl_CustId = cust.Id;
                        }
                        else
                        {
                            loadConfig = true;
                        }
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xPath = nodeList[0].SelectSingleNode("ProfilePath");
                    if (xPath != null)
                    {
                        Globals.glProfilePath = xPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xOutPath = nodeList[0].SelectSingleNode("OutputPath");
                    if (xOutPath != null)
                    {
                        Globals.glOutputDir = xOutPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xInPath = nodeList[0].SelectSingleNode("PickupPath");
                    if (xInPath != null)
                    {
                        Globals.glPickupPath = xInPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xLib = nodeList[0].SelectSingleNode("LibraryPath");
                    if (xLib != null)
                    {
                        Globals.glLibPath = xLib.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xFail = nodeList[0].SelectSingleNode("FailPath");
                    if (xFail != null)
                    {
                        Globals.glFailPath = xFail.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xArchive = nodeList[0].SelectSingleNode("ArchiveLocation");
                    if (xArchive != null)
                    {
                        Globals.glArcLocation = xArchive.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xTesting = nodeList[0].SelectSingleNode("TestPath");
                    if (xTesting != null)
                    {
                        Globals.glTestLocation = xTesting.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    nodeList = xmlConfig.SelectNodes("/Satellite/Communications");
                    XmlNode xmlAlertsTo = nodeList[0].SelectSingleNode("AlertsTo");
                    if (xmlAlertsTo != null)
                    {
                        Globals.glAlertsTo = xmlAlertsTo.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xSmtp = nodeList[0].SelectSingleNode("SMTPServer");
                    if (xSmtp != null)
                    {
                        SMTPServer.Server = xSmtp.InnerText;
                        SMTPServer.Port = xSmtp.Attributes[0].InnerXml;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xEmail = nodeList[0].SelectSingleNode("EmailAddress");
                    if (xEmail != null)
                    { SMTPServer.Email = xEmail.InnerText; }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xUserName = nodeList[0].SelectSingleNode("SMTPUsername");
                    if (xUserName != null)
                    {
                        SMTPServer.Username = xUserName.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xPassword = nodeList[0].SelectSingleNode("SMTPPassword");
                    if (xPassword != null)
                    {
                        SMTPServer.Password = xPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    if (loadConfig)
                    {
                        throw new System.Exception("Mandatory Settings missing");
                    }
                    //CTC Node Database Connection
                    nodeList = null;
                    nodeList = xmlConfig.SelectNodes("/Satellite/CTCNode");
                    nodeCat = null;
                    nodeCat = nodeList[0].SelectSingleNode("Catalog");
                    if (nodeCat != null)
                    {
                        Globals.glCTCDbInstance = nodeCat.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlServer = null;
                    xmlServer = nodeList[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.glCTCDbServer = xmlServer.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlUser = null;
                    xmlUser = nodeList[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.glCTCDbUserName = xmlUser.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlPassword = null;
                    xmlPassword = nodeList[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.glCTCDbPassword = xmlPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    sqlCTCConn = new SqlConnection();
                    sqlCTCConn.ConnectionString = Globals.CTCconnString();
                }

                catch (Exception)
                {
                    MessageBox.Show("There was an error loading the Config files. Please check the settings", "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    frmSettings FormSettings = new frmSettings();
                    FormSettings.ShowDialog();
                    DialogResult dr = FormSettings.DialogResult;
                    if (dr == DialogResult.OK)
                    {
                        LoadSettings();
                    }
                }
                var Cust = Globals.GetCustomer(Globals.glCustCode);
                this.Text = "CTC Satellite - " + Cust.CustomerName;
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "System Ready");
            }
        }

        private void profilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProfiles Profiles = new frmProfiles();
            Profiles.ShowDialog();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSettings FormSettings = new frmSettings();
            FormSettings.ShowDialog();
            DialogResult dr = FormSettings.DialogResult;
            if (dr == DialogResult.OK)
            {
                LoadSettings();
            }
        }

        private void GetFiles(string pattern)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            string filesPath = string.Empty;
            if (tslMode.Text == "Production")
            {
                filesPath = Globals.glPickupPath;
            }
            else
            {
                filesPath = Globals.glTestLocation;
            }
            DirectoryInfo diTodo = new DirectoryInfo(filesPath);
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            FileInfo[] list = null;
            try
            {
                list = diTodo.GetFiles();

            }
            catch (Exception ex)
            {
                NodeResources.AddRTBText(rtbLog, ex.GetType().Name + " error found." + ex.Message);
                return;
            }
            if (list.Length > 0)
            {
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Now Processing " + list.Length + " Files");
                foreach (var fileName in diTodo.GetFiles(pattern))
                {

                    ProcessResult thisResult = new ProcessResult();
                    string messageNamespace = NodeResources.GetMessageNamespace(fileName.FullName);
                    if (!string.IsNullOrEmpty(messageNamespace))
                    {
                        ProcessCargowiseFiles(fileName.FullName);
                    }
                    else
                    {
                        thisResult = ProcessCustomFiles(fileName.FullName);
                    }
                    if (thisResult.Processed)
                    {
                        try
                        {
                            if (File.Exists(fileName.FullName))
                            {
                                System.GC.Collect();
                                System.GC.WaitForPendingFinalizers();
                                File.Delete(fileName.FullName);
                            }

                        }
                        catch (Exception ex)
                        {
                            NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Unable to Delete File. Error :" + ex.Message);
                        }
                    }
                }
                stopwatch.Stop();
                double t = stopwatch.Elapsed.TotalSeconds;
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Processing Complete. " + list.Length + " files processed in " + t + " seconds");
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Waiting for Files.");
            }
        }

        private ProcessResult ProcessCustomFiles(String xmlFile)
        {
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;
            NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File " + Path.GetFileName(xmlFile) + " Is not a Cargowise File. Checking for Against Customer.");
            FileInfo processingFile = new FileInfo(xmlFile);
            filesRx++;
            NodeResources.AddLabel(lblFilesRx, filesRx.ToString());
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            TransReference trRef = new TransReference();
            String archiveFile;
            string recipientID = "";
            string senderID = "";
            archiveFile = Globals.ArchiveFile(Globals.glArcLocation, xmlFile);
            if (processingFile.Extension.ToUpper() == ".XML")
            {
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " New XML file found " + Path.GetFileName(processingFile.FullName) + ". Processing" + "");
                SqlDataAdapter daXML = new SqlDataAdapter("SELECT C_CODE, C_ID, C_PATH, P_XSD, P_DESCRIPTION, P_ID, P_RECIPIENTID, P_SENDERID, P_SERVER, P_USERNAME, P_PASSWORD, P_LIBNAME, P_BILLTO,  P_PARAMLIST, P_METHOD from VW_CustomerProfile where P_XSD is not null and P_ACTIVE='Y'", sqlConn);
                DataSet dsXML = new DataSet();
                daXML.Fill(dsXML, "XMLFile");
                Boolean processed = false;
                int iLineno = 0;
                while (!processed)
                {
                    foreach (DataRow dr in dsXML.Tables[0].Rows)
                    {
                        try
                        {
                            //string schemaFile = Path.Combine(Globals.glProfilePath, "Lib", dr["P_XSD"].ToString());
                            //XmlTextReader sXsd = new XmlTextReader(schemaFile);
                            //XmlSchema schema = new XmlSchema();
                            //string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                            //schema = XmlSchema.Read(sXsd, CustomValidateHandler.HandlerErrors);
                            //XmlReaderSettings settings = new XmlReaderSettings();
                            //settings.ValidationType = ValidationType.Schema;
                            //settings.Schemas.Add(schema);
                            //settings.ValidationEventHandler += (o, ea) =>
                            //{
                            //    throw new XmlSchemaValidationException(
                            //        string.Format("Schema Not Found: {0}",
                            //                      ea.Message),
                            //        ea.Exception);
                            //};
                            //using (var stream = new FileStream(xmlFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                            //using (var cusXML = XmlReader.Create(stream, settings))
                            //{
                            //    while (cusXML.Read())
                            //    {
                            //        iLineno++;
                            //    }
                            //    stream.Close();
                            //    NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Schema File Found (" + schemaFile + "). Continue processing Profile.");
                            //    stream.Dispose();
                            //    cusXML.Close();
                            //    cusXML.Dispose();
                            //    string custMethod = dr["P_METHOD"].ToString();
                            //    string custParams = dr["P_PARAMLIST"].ToString();
                            ProcessJapanNNR(xmlFile);
                            //}
                            recipientID = dr["P_RECIPIENTID"].ToString();
                            senderID = dr["P_SENDERID"].ToString();
                            processed = true;
                            thisResult.Processed = true;
                        }

                        catch (XmlSchemaValidationException ex)
                        {

                            //MailModule.sendMsg("", Globals.glAlertsTo, "XML Schema Validation issue", "Invalid schema definition error:" + ex.Message);
                            ProcessingErrors procerror = new ProcessingErrors();
                            CTCErrorCode error = new CTCErrorCode();
                            error.Code = NodeError.e111;
                            error.Description = "Customer XML Error :" + ex.Message;
                            error.Severity = "Warning";
                            procerror.ErrorCode = error;
                            procerror.SenderId = senderID;
                            procerror.RecipientId = recipientID;
                            procerror.FileName = xmlFile;

                            MailModule.sendMsg(xmlFile, Globals.glAlertsTo, "MLASYDSYD: " + ex.Message + "Line No: " + iLineno.ToString(), ex);
                            NodeResources.MoveFile(xmlFile, Path.Combine(Globals.glFailPath));
                            NodeResources.AddProcessingError(procerror);
                            thisResult.FolderLoc = Globals.glFailPath;
                            thisResult.Processed = true;
                            processed = true;
                            NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Invalid schema definition error:" + ex.Message + ". " + "", Color.Red);


                        }
                        catch (XmlException ex)
                        {

                            ProcessingErrors procerror = new ProcessingErrors();

                            CTCErrorCode error = new CTCErrorCode();
                            error.Code = NodeError.e111;
                            error.Description = "Customer XML Error :" + ex.Message;
                            error.Severity = "Warning";
                            procerror.ErrorCode = error;
                            procerror.SenderId = senderID;
                            procerror.RecipientId = recipientID;
                            procerror.FileName = xmlFile;
                            NodeResources.MoveFile(xmlFile, Path.Combine(Globals.glFailPath));
                            NodeResources.AddProcessingError(procerror);
                            thisResult.FolderLoc = Globals.glFailPath;
                            thisResult.Processed = true;
                            processed = true;
                            NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node Exception Found: Custom Processing:" + ex.Message + ". " + "", Color.Red);
                            //   return thisResult;

                        }
                        catch (Exception ex)
                        {
                            ProcessingErrors procerror = new ProcessingErrors();
                            CTCErrorCode error = new CTCErrorCode();
                            error.Code = NodeError.e111;
                            error.Description = "Customer XML Error :" + ex.Message;
                            error.Severity = "Warning";
                            procerror.ErrorCode = error;
                            procerror.SenderId = senderID;
                            procerror.RecipientId = recipientID;
                            procerror.FileName = xmlFile;
                            NodeResources.AddProcessingError(procerror);
                            // File.Move(xmlFile, Path.Combine(Globals.glFailPath, Path.GetFileName(xmlFile)));
                            thisResult.FolderLoc = Globals.glFailPath;
                            thisResult.Processed = false;
                            processed = true;
                            NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node Exception Found: Custom Processing:" + ex.Message + ". " + "", Color.Red);
                            //   return thisResult;
                        }
                    }
                    if (!processed)
                    {
                        ProcessingErrors procerror = new ProcessingErrors();
                        CTCErrorCode error = new CTCErrorCode();
                        error.Code = NodeError.e111;
                        error.Description = "Unable to process " + Path.GetFileName(xmlFile) + "No matching routines found. Moving to Failed.";
                        error.Severity = "Warning";
                        procerror.ErrorCode = error;
                        procerror.SenderId = senderID;
                        procerror.RecipientId = recipientID;
                        procerror.FileName = xmlFile;
                        NodeResources.AddProcessingError(procerror);
                        thisResult.FolderLoc = Globals.glFailPath;
                        thisResult.Processed = false;
                        NodeResources.MoveFile(xmlFile, Path.Combine(Globals.glFailPath));
                        NodeResources.AddProcessingError(procerror);
                        thisResult.Processed = false;
                        thisResult.FolderLoc = Globals.glFailPath;
                        NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Unable to process " + Path.GetFileName(xmlFile) + "No matching routines found. Moving to Failed." + "", Color.Red);
                        processed = true;
                        //  return thisResult;
                    }

                }
            }
            return thisResult;
        }

        private void GetProcErrors()
        {
            SqlConnection sqlConn = new SqlConnection();
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            try
            {
                sqlConn.Open();
                SqlDataAdapter daProcErrors = new SqlDataAdapter("SELECT E_P, P_DESCRIPTION, E_SENDERID, E_RECIPIENTID, E_PROCDATE, E_FILENAME, E_ERRORDESC, E_ERRORCODE, " +
                                                                "E_PK, P_PATH FROM VW_ProcessngErrors ORDER BY E_PROCDATE", sqlConn);
                DataSet dsProcErrors = new DataSet();
                dgProcessingErrors.AutoGenerateColumns = false;
                daProcErrors.Fill(dsProcErrors, "PROCERRORS");
                dgProcessingErrors.DataSource = dsProcErrors;
                dgProcessingErrors.DataMember = "PROCERRORS";
                sqlConn.Close();
            }
            catch (Exception)
            {

            }
        }

        private void ProcessJapanNNR(string xmlFile)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());

            root nnrJapan = new root();
            FileInfo fi = new FileInfo(xmlFile);
            NodeResources.AddRTBText(rtbLog, "Now Processing NNR Global (JPN) file :" + fi.Name);
            Globals.ArchiveFile(Globals.glArcLocation, xmlFile);
            string flightPrefix = string.Empty;
            StreamReader reader = new StreamReader(xmlFile);
            try
            {
                XmlSerializer serJapan = new XmlSerializer(typeof(root));
                nnrJapan = (root)serJapan.Deserialize(reader);
            }
            catch (Exception ex)
            {
                ProcessingErrors procerror = new ProcessingErrors();
                MailModule.sendMsg(xmlFile, Globals.glAlertsTo, Globals.glCustCode + ": De-Serialization Error in XML File", ex);
                NodeResources.MoveFile(xmlFile, Path.Combine(Globals.glFailPath));
                CTCErrorCode error = new CTCErrorCode();
                error.Code = NodeError.e111;
                error.Description = "Serialization Error :" + ex.Message;
                error.Severity = "Warning";
                procerror.ErrorCode = error;
                procerror.SenderId = string.Empty;
                procerror.RecipientId = string.Empty;
                procerror.FileName = xmlFile;

                //NodeResources.AddProcessingError(procerror);
                //thisResult.FolderLoc = Globals.glFailPath;
                //thisResult.Processed = true;
                //processed = true;
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node Exception Found: Serialization Error:" + ex.Message + ". " + "", Color.Red);
                return;
            }
            finally
            {
                reader.Close();
            }
            var nnrForwarder = GetPartyCountryCode(nnrJapan.Master.Parties, "IMFORW");
            CNodeBE.Company company = new CNodeBE.Company();
            switch (nnrForwarder.country_code)
            {
                case "AU":

                    company.Code = "SYD";

                    break;
                case "NZ":
                    company.Code = "AKL";

                    break;
            }
            company.Name = nnrForwarder.name;
            company.Country = new CNodeBE.Country { Code = nnrForwarder.country_code };
            string fileType = string.Empty;
            //Create the XUS Interchange Header
            CNodeBE.Shipment consol = new CNodeBE.Shipment();
            CNodeBE.DataContext consolDataContext = new CNodeBE.DataContext();

            List<CNodeBE.DataTarget> dataConsolTargetColl = new List<CNodeBE.DataTarget>();
            CNodeBE.DataTarget dataConsolTarget = new CNodeBE.DataTarget();
            CNodeBE.CodeDescriptionPair transMode = new CNodeBE.CodeDescriptionPair();
            CNodeBE.CodeDescriptionPair paymentMode = new CNodeBE.CodeDescriptionPair();
            CNodeBE.CodeDescriptionPair cdPlace = new CNodeBE.CodeDescriptionPair();
            CNodeBE.WayBillType wayBill = new CNodeBE.WayBillType();
            CNodeBE.Date date = new CNodeBE.Date();
            //try
            //{
            // Create the Consol (Master House Bill Level)

            consolDataContext.DataTargetCollection = dataConsolTargetColl.ToArray();
            List<CNodeBE.Shipment> shipmentColl = new List<CNodeBE.Shipment>();
            //Create the Shipment Object to hold the constructing Consol and shipments
            switch (nnrJapan.Master.mode_code.ToUpper().Trim())
            {
                case "OCEAN":
                    // Create the Shipment(House Bill Level)
                    dataConsolTarget.Type = "ForwardingShipment";
                    dataConsolTargetColl.Add(dataConsolTarget);
                    transMode.Code = "SEA";
                    transMode.Description = "Sea Freight";
                    //consol.ContainerMode = new ContainerMode { Code = nnrJapan.Master.load_type };
                    shipmentColl = ProcessNNRShipment(nnrJapan.houses);
                    consol = null;
                    fileType = "S";
                    break;
                case "AIR":
                    // Create the Consol (Master House Bill Level)
                    dataConsolTarget.Type = "ForwardingConsol";
                    dataConsolTargetColl.Add(dataConsolTarget);
                    transMode.Code = "AIR";
                    transMode.Description = "Air Freight";
                    fileType = "A";
                    consol = ProcessNNRConsol(nnrJapan.Master);
                    CNodeBE.Shipment subShipment = new CNodeBE.Shipment();
                    if (nnrJapan.houses == null)
                    {
                        NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "(B/L: " + nnrJapan.Master.bill_no + "No Houses contained in Master. " + "", Color.Blue);
                    }
                    else
                    {
                        shipmentColl = ProcessNNRShipment(nnrJapan.houses);
                    }
                    break;
            }

            if (consol == null)
            {
                foreach (Shipment shipment in shipmentColl)
                {
                    CreateSeaShipmentFile(shipment);
                }
                return;

            }
            else
            {
                if (shipmentColl != null)
                {
                    SqlCommand checkObl = new SqlCommand("Check_OBL", sqlConn);
                    checkObl.CommandType = CommandType.StoredProcedure;
                    SqlParameter foundOBL = checkObl.Parameters.Add("@FOUNDOBL", SqlDbType.Char, 5);
                    foundOBL.Direction = ParameterDirection.Output;
                    SqlParameter consolno = checkObl.Parameters.Add("@CONSOL", SqlDbType.NChar, 15);
                    consolno.Direction = ParameterDirection.Output;
                    SqlParameter oblVessel = checkObl.Parameters.Add("@VESSEL", SqlDbType.VarChar, 50);
                    SqlParameter oblVoy = checkObl.Parameters.Add("@VOYAGE", SqlDbType.VarChar, 50);
                    SqlParameter oblMode = checkObl.Parameters.Add("@MODE", SqlDbType.Char, 3);
                    SqlParameter eta = checkObl.Parameters.Add("@ETA", SqlDbType.DateTime);
                    SqlParameter etd = checkObl.Parameters.Add("@ETD", SqlDbType.DateTime);
                    SqlParameter oblContNo = checkObl.Parameters.Add("@CONTNO", SqlDbType.VarChar, 20);
                    SqlParameter oblPol = checkObl.Parameters.Add("@POL", SqlDbType.Char, 5);
                    SqlParameter oblPod = checkObl.Parameters.Add("@POD", SqlDbType.Char, 5);
                    SqlParameter obl = checkObl.Parameters.Add("@OBL", SqlDbType.VarChar, 50);


                    if (consol.WayBillType.Code == "MWB")
                    {
                        obl.Value = consol.WayBillNumber;
                    }
                    oblPol.Value = consol.PortOfLoading.Code;
                    oblPod.Value = consol.PortOfDischarge.Code;
                    //oblVessel.Value = consol.VesselName;
                    oblMode.Value = consol.TransportMode.Code;
                    oblVoy.Value = consol.VoyageFlightNo;
                    foreach (Shipment sh in shipmentColl)
                    {
                        dataConsolTarget = new DataTarget();
                        dataConsolTarget.Type = "ForwardingShipment";
                        dataConsolTargetColl.Add(dataConsolTarget);
                        foreach (Date dt in sh.DateCollection)
                        {
                            switch (dt.Type)
                            {
                                case DateType.Arrival:
                                    eta.Value = dt.Value;
                                    break;
                                case DateType.Departure:
                                    etd.Value = dt.Value;
                                    break;
                            }
                        }

                        //    oblContNo.Value = cn.ContainerNumber;
                        checkObl.ExecuteNonQuery();
                        string oblresults = foundOBL.Value.ToString();
                        // oblresults = "01111";
                        if (oblresults[0] == '1')
                        {
                            //meaning that the OBL was found in the DB. Going to Ignore it and send warning that data existed already. 
                            string ignoreObl = string.Empty;
                            ignoreObl = "Master Bill Number has been found and will be ignored.";
                            if (oblresults[1] == '0')
                            {
                                ignoreObl += "\n Found that the Dates were not the same as the Cargowse data.";
                            }

                            if (oblresults[2] == '0')
                            {
                                ignoreObl += "\n Found that the Ports were not the same as the Cargowise data.";
                            }

                            if (oblresults[3] == '0')
                            {
                                ignoreObl += "\n Found that the Vessel/ or Voyage details were not the same as Cargowise Data";
                            }

                            string msg = "Master Details found in a previously Registered Job. (" + consolno.Value + ") and will be rejected because: \n " + ignoreObl;
                            try
                            {
                                MailModule.sendMsg(xmlFile, Globals.glAlertsTo, "NNR Japan File Rejected: " + nnrJapan.Master.bill_no, msg);
                            }
                            catch (Exception)
                            {

                            }
                            NodeResources.AddRTBText(rtbLog, msg);

                            return;
                        }

                    }
                }
                consol.SubShipmentCollection = shipmentColl.ToArray();
            }
            if (dataConsolTargetColl.Count > 0)
            {
                consolDataContext.DataTargetCollection = dataConsolTargetColl.ToArray();
                consol.DataContext = consolDataContext;
            }
            consol.DataContext.Company = company;
            consol.DataContext.DataProvider = "NNRGLOJPN";
            switch (company.Country.Code)
            {
                case "AU":
                    consol.DataContext.ServerID = "SYD";
                    break;
                case "NZ":
                    consol.DataContext.ServerID = "AKL";
                    break;
            }

            consol.TransportMode = transMode;
            //consolDataContext.DataTargetCollection = dataConsolTargetColl.ToArray();
            //consol.DataContext = consolDataContext;
            CNodeBE.UniversalShipmentData bodyField = new CNodeBE.UniversalShipmentData();
            bodyField.Shipment = consol;
            CNodeBE.UniversalInterchangeBody body = new CNodeBE.UniversalInterchangeBody();
            body.BodyField = bodyField;
            CNodeBE.UniversalInterchangeHeader header = new CNodeBE.UniversalInterchangeHeader
            {
                SenderID = "NNRGLOJPN"
            };
            string filecode = string.Empty;
            switch (company.Country.Code)
            {
                case "AU":
                    header.RecipientID = "MLASYDSYD";
                    filecode = "NNR-AU";
                    break;
                case "NZ":
                    filecode = "NNR-NZ";
                    header.RecipientID = "GATNNRAKL";
                    break;

            }

            CNodeBE.UniversalInterchange interchange = new CNodeBE.UniversalInterchange();
            interchange.Body = body;
            interchange.version = "1.1";
            interchange.Header = header;

            String cwXML = Path.Combine(Globals.glOutputDir, filecode + "-" + fileType + "-" + FixNumber(nnrJapan.Master.bill_no) + ".xml");
            int iFileCount = 0;
            while (File.Exists(cwXML))
            {
                iFileCount++;
                cwXML = Path.Combine(Globals.glOutputDir, filecode + "-" + fileType + "-" + FixNumber(nnrJapan.Master.bill_no) + iFileCount + ".xml");
            }
            Stream outputCW = File.Open(cwXML, FileMode.Create);
            StringWriter writer = new StringWriter();
            interchange.Body = body;
            XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
            var cwNSUniversal = new XmlSerializerNamespaces();
            cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
            xSer.Serialize(outputCW, interchange, cwNSUniversal);
            outputCW.Flush();
            outputCW.Close();
            Globals.ArchiveFile(Globals.glArcLocation, cwXML);
        }

        private PartiesPartyCompany GetPartyCountryCode(PartiesParty[] parties, string PartyType)
        {
            var comp = (from p in parties
                        where p.party_type == PartyType
                        select p.Company).FirstOrDefault();
            if (comp != null)
            {
                return comp;
            }
            return null;
        }

        private void CreateSeaShipmentFile(Shipment shipment)
        {
            string fileType = string.Empty;
            //Create the XUS Interchange Header
            fileType = shipment.TransportMode.Code[0].ToString().Trim();
            CNodeBE.Shipment consol = new CNodeBE.Shipment();
            CNodeBE.DataContext consolDataContext = new CNodeBE.DataContext();
            CNodeBE.Company company = new CNodeBE.Company
            {
                Code = "SYD",
                Name = "NNR Global Logistics Australia Pty Ltd",
                Country = new CNodeBE.Country { Code = "AU", Name = "Australia" }

            };
            List<CNodeBE.DataTarget> dataConsolTargetColl = new List<CNodeBE.DataTarget>();
            CNodeBE.DataTarget dataConsolTarget = new CNodeBE.DataTarget();
            CNodeBE.CodeDescriptionPair transMode = new CNodeBE.CodeDescriptionPair();
            CNodeBE.CodeDescriptionPair paymentMode = new CNodeBE.CodeDescriptionPair();
            CNodeBE.CodeDescriptionPair cdPlace = new CNodeBE.CodeDescriptionPair();
            CNodeBE.WayBillType wayBill = new CNodeBE.WayBillType();
            CNodeBE.Date date = new CNodeBE.Date();
            //try
            //{
            // Create the Consol (Master House Bill Level)
            consol = shipment;

            consolDataContext.DataTargetCollection = dataConsolTargetColl.ToArray();
            if (dataConsolTargetColl.Count > 0)
            {
                consolDataContext.DataTargetCollection = dataConsolTargetColl.ToArray();
                consol.DataContext = consolDataContext;
            }
            consol.DataContext.Company = company;
            consol.DataContext.DataProvider = "NNRGLOJPN";
            consol.DataContext.ServerID = "SYD";
            //consolDataContext.DataTargetCollection = dataConsolTargetColl.ToArray();
            //consol.DataContext = consolDataContext;
            CNodeBE.UniversalShipmentData bodyField = new CNodeBE.UniversalShipmentData();
            bodyField.Shipment = consol;
            CNodeBE.UniversalInterchangeBody body = new CNodeBE.UniversalInterchangeBody();
            body.BodyField = bodyField;
            CNodeBE.UniversalInterchangeHeader header = new CNodeBE.UniversalInterchangeHeader
            {
                RecipientID = Globals.glCustCode,
                SenderID = "NNRGLOJPN"
            };
            CNodeBE.UniversalInterchange interchange = new CNodeBE.UniversalInterchange();
            interchange.Body = body;
            interchange.version = "1.1";
            interchange.Header = header;

            String cwXML = Path.Combine(Globals.glOutputDir, "NNR-AUS-" + fileType + "-" + FixNumber(shipment.WayBillNumber) + ".xml");
            int iFileCount = 0;
            while (File.Exists(cwXML))
            {
                iFileCount++;
                cwXML = Path.Combine(Globals.glOutputDir, "NNR-AUS-" + fileType + "-" + FixNumber(shipment.WayBillNumber) + iFileCount + ".xml");
            }
            Stream outputCW = File.Open(cwXML, FileMode.Create);
            StringWriter writer = new StringWriter();
            interchange.Body = body;
            XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
            var cwNSUniversal = new XmlSerializerNamespaces();
            cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
            xSer.Serialize(outputCW, interchange, cwNSUniversal);
            outputCW.Flush();
            outputCW.Close();
        }

        private Shipment ProcessNNRConsol(rootMaster master)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            Shipment result = new Shipment();
            CodeDescriptionPair paymentMode = new CodeDescriptionPair();
            if (master.mode_code == "AIR")
            {
                result.ContainerMode = new ContainerMode { Code = "LSE" };
                result.TransportMode = new CodeDescriptionPair { Code = "AIR" };
            }
            else
            {
                result.ContainerMode = new ContainerMode { Code = master.load_type };
                result.TransportMode = new CodeDescriptionPair { Code = "SEA" };
            }

            switch (master.freight_term_code)
            {
                case "PPD":
                    paymentMode.Code = "PPD";
                    paymentMode.Description = "Pre-Paid";
                    break;
                default:
                    paymentMode.Code = "CCX";
                    paymentMode.Description = "Collect";
                    break;
            }
            result.DocumentedChargeable = master.chargeable_weight;
            result.DocumentedChargeableSpecified = true;
            result.DocumentedVolume = master.volume;
            try
            {

                try
                {
                    if (master.no_of_originals == "EXPRESS")
                    {
                        result.ReleaseType = new CodeDescriptionPair { Code = "EBL", Description = "Express Bill of Lading" };
                    }
                    else
                    {
                        if (master.no_of_originals != null)
                        {
                            result.NoOriginalBills = (sbyte)int.Parse(master.no_of_originals);
                            result.NoOriginalBillsSpecified = true;
                        }

                    }

                }
                catch (Exception)
                {


                }
                result.PaymentMethod = paymentMode;
                CNodeBE.Currency payCurrency = new CNodeBE.Currency
                {
                    Code = master.currency_code
                };
                result.FreightRateCurrency = payCurrency;
                try
                {
                    result.BookingConfirmationReference = master.booking_no.ToString();
                }
                catch (NullReferenceException)
                {
                    NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "(B/L: " + master.bill_no + " No booking reference found on Master. " + "", Color.Blue);
                }
                try
                {
                    result.CoLoadMasterBillNumber = master.second_bill_no.ToString();
                }
                catch (NullReferenceException)
                {
                    NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "(B/L: " + master.bill_no + " No Second MBL found on Master. " + "", Color.Blue);
                }
                result.AgentsReference = master.file_no;
                result.WayBillNumber = master.bill_no;
                WayBillType wayBill = new WayBillType();
                switch (master.bill_type)
                {
                    case "D":
                        wayBill.Code = "MWB";
                        wayBill.Description = "Master Waybill";
                        break;

                    case "M":
                        wayBill.Code = "MWB";
                        wayBill.Description = "Master Waybill";
                        break;
                }
                result.WayBillType = wayBill;
                result.TotalNoOfPieces = master.total_pieces;
                result.TotalNoOfPiecesSpecified = true;
                decimal allocVol = 0;
                if (decimal.TryParse(master.allocated_volume, out allocVol))
                {
                    result.TotalPreallocatedVolume = allocVol;
                }

                result.TotalPreallocatedVolumeSpecified = true;
                result.TotalVolume = master.volume;
                result.TotalVolumeSpecified = true;
                result.TotalWeight = master.gross_weight;

            }
            catch (Exception ex)
            {
                ProcessingErrors procerror = new ProcessingErrors();

                CTCErrorCode error = new CTCErrorCode();
                error.Code = NodeError.e111;
                error.Description = "Serialization Error :" + ex.Message;
                error.Severity = "Warning";
                procerror.ErrorCode = error;
                procerror.SenderId = string.Empty;
                procerror.RecipientId = string.Empty;
                //procerror.FileName = xmlFile;
                //NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                //NodeResources.AddProcessingError(procerror);
                //thisResult.FolderLoc = Globals.glFailPath;
                //thisResult.Processed = true;
                //processed = true;
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node Exception Found: Master WayBill Error:" + ex.Message + ". " + "", Color.Red);
            }

            try
            {
                //Loop through Locations to create the locations in the Consol. 
                foreach (LocationsLocation loc in master.Locations)
                {
                    switch (loc.location_type)
                    {
                        case "DELIVERY":
                            result.PlaceOfDelivery = new CNodeBE.UNLOCO { Code = loc.location_code };
                            break;
                        case "LOADING":
                            result.PortOfLoading = new CNodeBE.UNLOCO { Code = loc.location_code };
                            break;
                        case "RECEIPT":
                            result.PlaceOfReceipt = new CNodeBE.UNLOCO { Code = loc.location_code };
                            break;
                        case "DISCHARGE":
                            result.PortOfDischarge = new CNodeBE.UNLOCO { Code = loc.location_code };
                            break;
                        case "DEST":
                            result.PortOfDestination = new CNodeBE.UNLOCO { Code = loc.location_code };
                            break;
                        case "ORG":
                            result.PortOfOrigin = new CNodeBE.UNLOCO { Code = loc.location_code };
                            break;
                    }
                }
            }
            catch (NullReferenceException)
            {
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "(B/L: " + master.bill_no + " No Locations found on Master. " + "", Color.Blue);
            }
            // Loop through the Parties to create the Organization collection for the consol
            List<CNodeBE.OrganizationAddress> masterOrgColl = new List<CNodeBE.OrganizationAddress>();
            CNodeBE.OrganizationAddress masterOrg = new CNodeBE.OrganizationAddress();
            string flightPrefix = string.Empty;
            try
            {
                foreach (PartiesParty party in master.Parties)
                {
                    masterOrg = new OrganizationAddress();
                    masterOrg.AddressType = NodeResources.GetEnum("ORGTYPE", party.party_type);
                    if (masterOrg.AddressType == "ShippingLineAddress")
                    {
                        flightPrefix = party.Carrier.air_iata_carrier_code;

                    }
                    masterOrg.CompanyName = party.Company.name;
                    masterOrg.Address1 = party.Address.street_address_1;
                    masterOrg.Address2 = party.Address.street_address_2;
                    masterOrg.City = party.Address.city;
                    masterOrg.State = party.Address.state_province;
                    masterOrg.Postcode = party.Address.postal_code;
                    masterOrg.Country = new Country { Code = party.Address.country_code };
                    masterOrg.Phone = party.Address.tel_no;
                    //List<CNodeBE.RegistrationNumber> regoNumberColl = new List<RegistrationNumber>();
                    //CNodeBE.RegistrationNumber regonumber = new RegistrationNumber();

                    //regonumber.CountryOfIssue = new Country { Code =  };
                    //regonumber.Type = new RegistrationNumberType { Code = "LSC" };
                    //regonumber.Value = party.Company.company_code;
                    //regoNumberColl.Add(regonumber);
                    //masterOrg.RegistrationNumberCollection = regoNumberColl.ToArray();
                    masterOrgColl.Add(masterOrg);
                }
            }
            catch (NullReferenceException)
            {
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "(B/L: " + master.bill_no + " No Parties found on Master. " + "", Color.Blue);
            }
            catch (Exception ex)
            {
                ProcessingErrors procerror = new ProcessingErrors();

                CTCErrorCode error = new CTCErrorCode();
                error.Code = NodeError.e111;
                error.Description = "Serialization Error :" + ex.Message;
                error.Severity = "Warning";
                procerror.ErrorCode = error;
                procerror.SenderId = string.Empty;
                procerror.RecipientId = string.Empty;
                //procerror.FileName = xmlFile;
                //NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                string strEx = ex.GetType().Name;
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node " + strEx + " Exception Found: Parties Error:" + ex.Message + ". " + "", Color.Red);
            }
            result.OrganizationAddressCollection = masterOrgColl.ToArray();

            // Loop through the Dates element to create the DateCollection element
            List<CNodeBE.Date> masterDateColl = new List<CNodeBE.Date>();
            Date date = new Date();
            try
            {
                if (master.Dates == null)
                {
                    if (master.Routings[0].sequence_id == 1)
                    {
                        date = new CNodeBE.Date();
                        date.Value = master.Routings[0].etd;
                        date.Type = DateType.Departure;
                        masterDateColl.Add(date);
                        date = new CNodeBE.Date();
                        date.Value = master.Routings[0].eta;
                        date.Type = DateType.Arrival;
                        masterDateColl.Add(date);
                    }
                }
                else
                {
                    foreach (DatesDate masterDate in master.Dates)
                    {
                        try
                        {
                            date = new CNodeBE.Date();
                            date.Value = masterDate.date_value;
                            date.Type = (CNodeBE.DateType)Enum.Parse(typeof(CNodeBE.DateType), NodeResources.GetEnum("DATETYPE", masterDate.date_type));
                            masterDateColl.Add(date);
                        }
                        catch (Exception)
                        {

                        }
                    }
                }

                result.DateCollection = masterDateColl.ToArray();
            }
            catch (NullReferenceException)
            {
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "(B/L: " + master.bill_no + " No Dates found on Master. " + "", Color.Blue);
            }
            List<RoutingsRouting> masterRoutings = new List<RoutingsRouting>();
            masterRoutings = master.Routings.ToList();
            int routes = masterRoutings.Count;

            // Loop through the Transport Entries element to create the TrnasportLeg Collection element
            List<CNodeBE.TransportLeg> consolTransportCollection = new List<CNodeBE.TransportLeg>();
            CNodeBE.TransportLeg consolTransportLeg = new CNodeBE.TransportLeg();
            try
            {
                foreach (RoutingsRouting masterRouting in masterRoutings)
                {
                    consolTransportLeg = new TransportLeg();
                    switch (masterRouting.routing_type.ToUpper())
                    {
                        case "MAIN":
                            if (result.TransportMode.Code.ToUpper() == "AIR")
                            {
                                if (masterRouting.sequence_id == 1)
                                {
                                    consolTransportLeg.LegType = CNodeBE.TransportLegLegType.Flight1;
                                }
                                else
                                {
                                    consolTransportLeg.LegType = CNodeBE.TransportLegLegType.Flight2;
                                }
                            }
                            else
                            {
                                consolTransportLeg.LegType = CNodeBE.TransportLegLegType.Main;
                            }
                            break;
                        case "SECOND":
                            if (result.TransportMode.Code.ToUpper() == "AIR")
                            {
                                consolTransportLeg.LegType = CNodeBE.TransportLegLegType.Flight2;
                            }
                            else
                            {
                                consolTransportLeg.LegType = CNodeBE.TransportLegLegType.OnForwarding;
                            }
                            break;
                    }
                    consolTransportLeg.LegOrder = (sbyte)masterRouting.sequence_id;
                    // Look at Routes when the Routes count ==1. this should set the Port of Loading. Place of Receipt.
                    if (masterRouting.sequence_id == routes)
                    {
                        date = new CNodeBE.Date();
                        date.Type = CNodeBE.DateType.FirstForeignArrival;
                        date.Value = masterRouting.eta;
                        masterDateColl.Add(date);
                        result.PortOfDischarge = new UNLOCO { Code = masterRouting.destination_locode };

                    }

                    if ((masterRouting.sequence_id == 1 && routes > 1) || (routes == 1))
                    {
                        date = new CNodeBE.Date();
                        date.Type = CNodeBE.DateType.LastForeignDeparture;
                        date.Value = masterRouting.etd;
                        masterDateColl.Add(date);
                        result.PlaceOfReceipt = new UNLOCO { Code = masterRouting.departure_locode };
                        result.PortOfLoading = new UNLOCO { Code = masterRouting.departure_locode };
                        result.VoyageFlightNo = flightPrefix + masterRouting.flight_voyage_no;

                    }
                    consolTransportLeg.PortOfLoading = new UNLOCO { Code = masterRouting.departure_locode };
                    consolTransportLeg.PortOfDischarge = new UNLOCO { Code = masterRouting.destination_locode };
                    if (masterRouting.mode_code.ToUpper() == "OCEAN")
                    {
                        consolTransportLeg.TransportMode = CNodeBE.TransportLegTransportMode.Sea;
                        consolTransportLeg.VoyageFlightNo = masterRouting.flight_voyage_no;
                        consolTransportLeg.VesselName = masterRouting.vessel_name;
                    }
                    if (masterRouting.mode_code.ToUpper() == "AIR")
                    {
                        consolTransportLeg.TransportMode = CNodeBE.TransportLegTransportMode.Air;
                        consolTransportLeg.VoyageFlightNo = flightPrefix + masterRouting.flight_voyage_no;
                    }
                    if (!string.IsNullOrEmpty(masterRouting.etd))
                    {
                        consolTransportLeg.EstimatedDeparture = masterRouting.etd;
                    }

                    if (!string.IsNullOrEmpty(masterRouting.eta))
                    {
                        consolTransportLeg.EstimatedArrival = masterRouting.eta;
                    }

                    consolTransportCollection.Add(consolTransportLeg);
                }
                result.TransportLegCollection = consolTransportCollection.ToArray();
            }
            catch (NullReferenceException)
            {
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "(B/L: " + master.bill_no + " No Routings found on Master. " + "", Color.Blue);
            }
            catch (Exception ex)
            {
                ProcessingErrors procerror = new ProcessingErrors();

                CTCErrorCode error = new CTCErrorCode();
                error.Code = NodeError.e111;
                error.Description = "Serialization Error :" + ex.Message;
                error.Severity = "Warning";
                procerror.ErrorCode = error;
                procerror.SenderId = string.Empty;
                procerror.RecipientId = string.Empty;
                //procerror.FileName = xmlFile;
                //NodeResources.MoveFile(xmlFile, Globals.glFailPath);

                //NodeResources.AddProcessingError(procerror);
                //thisResult.FolderLoc = Globals.glFailPath;
                //thisResult.Processed = true;
                //processed = true;
                string strEx = ex.GetType().Name;
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node " + strEx + " Exception Found: Routing Error:" + ex.Message + ". " + "", Color.Red);
            }
            return result;
        }

        private List<Shipment> ProcessNNRShipment(rootHousesHouse[] houses)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            Shipment subShipment = new Shipment();
            string flightPrefix = string.Empty;
            List<Shipment> result = new List<Shipment>();
            string transportMode = string.Empty;
            foreach (rootHousesHouse house in houses)
            {
                try
                {

                    // Add the Datacontext to the Shipment and Add the ForwardingShipment to the Consol
                    List<CNodeBE.DataTarget> dsShipmentTargetColl = new List<CNodeBE.DataTarget>();
                    CNodeBE.DataTarget dsShipmentTarget = new CNodeBE.DataTarget();
                    dsShipmentTarget.Type = "ForwardingShipment";
                    //dataConsolTarget = new DataTarget { Type = "ForwardingShipment" };
                    //dataConsolTargetColl.Add(dataConsolTarget);
                    dsShipmentTargetColl.Add(dsShipmentTarget);
                    subShipment = new Shipment();
                    subShipment.DataContext = new CNodeBE.DataContext { DataTargetCollection = dsShipmentTargetColl.ToArray() };
                    bool containsString = house.bill_no.Substring(0, 3).Any(char.IsLetter);
                    if (!containsString)
                    {
                        subShipment.WayBillNumber = "NNR" + house.bill_no.Replace(" ", string.Empty);
                    }
                    else
                    {
                        subShipment.WayBillNumber = house.bill_no;
                    }

                    if (house.bill_type == "H")
                    {
                        subShipment.WayBillType = new CNodeBE.WayBillType { Code = "HWB", Description = "House WayBill" };
                    }
                    if (house.Routings[0] != null)
                    {
                        switch (house.mode_code)
                        {
                            case "OCEAN":

                                subShipment.TransportMode = new CodeDescriptionPair { Code = "SEA" };
                                subShipment.ContainerMode = new ContainerMode { Code = house.load_type };
                                break;
                            case "AIR":

                                subShipment.TransportMode = new CodeDescriptionPair { Code = "AIR" };
                                subShipment.ContainerMode = new ContainerMode { Code = "LSE" };
                                break;
                        }
                    }


                    subShipment.DocumentedChargeable = house.chargeable_weight;
                    subShipment.DocumentedChargeableSpecified = true;
                    subShipment.DocumentedVolume = house.volume;
                    subShipment.PaymentMethod = new CodeDescriptionPair { Code = NodeResources.GetEnum("FREIGHTTERM", house.freight_term_code) };

                    try
                    {
                        if (house.no_of_originals == "EXPRESS")
                        {
                            subShipment.ReleaseType = new CodeDescriptionPair { Code = "EBL", Description = "Express Bill of Lading" };
                        }
                        else
                        {
                            if (house.no_of_originals != null)
                            {
                                int noBills;
                                if (int.TryParse(house.no_of_originals, out noBills))
                                {
                                    subShipment.NoOriginalBills = (sbyte)noBills;
                                    subShipment.NoOriginalBillsSpecified = true;
                                }

                            }
                        }
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        //Loop through Locations to create the locations in the Shipment. 
                        foreach (LocationsLocation loc in house.Locations)
                        {
                            switch (loc.location_type.ToUpper())
                            {
                                case "DELIVERY":
                                    subShipment.PlaceOfDelivery = new CNodeBE.UNLOCO { Code = loc.location_code };
                                    break;
                                case "LOADING":
                                    subShipment.PortOfLoading = new CNodeBE.UNLOCO { Code = loc.location_code };
                                    if (subShipment.TransportMode.Code == "SEA")
                                    {
                                        subShipment.PortOfOrigin = new CNodeBE.UNLOCO { Code = loc.location_code };
                                    }
                                    break;
                                case "RECEIPT":
                                    subShipment.PlaceOfReceipt = new CNodeBE.UNLOCO { Code = loc.location_code };
                                    break;
                                case "DISCHARGE":
                                    subShipment.PortOfDischarge = new CNodeBE.UNLOCO { Code = loc.location_code };
                                    if (subShipment.TransportMode.Code == "SEA")
                                    {
                                        subShipment.PortOfDestination = new CNodeBE.UNLOCO { Code = loc.location_code };
                                    }
                                    break;
                                case "DEST":
                                    subShipment.PortOfDestination = new CNodeBE.UNLOCO { Code = loc.location_code };
                                    break;
                                case "ORG":
                                    subShipment.PortOfOrigin = new CNodeBE.UNLOCO { Code = loc.location_code };
                                    break;
                            }
                        }
                    }
                    catch (NullReferenceException)
                    {
                        NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "(B/L: " + house.bill_no + " No Locations found on HouseBill. " + "", Color.Blue);
                    }
                    subShipment.AgentsReference = house.file_no;
                    //try
                    //{
                    //    subShipment.BookingConfirmationReference = house.booking_no.ToString();
                    //}
                    //catch (NullReferenceException ex)
                    //{
                    //    NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "(B/L: " + house.bill_no + " No Booking Reference found on HouseBill. " + "", Color.Blue);
                    //}

                    //   subShipment.BookingConfirmationReference = house.booking_no;
                    subShipment.WayBillType = new WayBillType { Code = NodeResources.GetEnum("BILLTYPE", house.bill_type) };
                    List<CNodeBE.OrganizationAddress> houserOrgColl = new List<CNodeBE.OrganizationAddress>();
                    CNodeBE.OrganizationAddress houserOrg = new CNodeBE.OrganizationAddress();
                    List<CNodeBE.Date> shipmentDateColl = new List<CNodeBE.Date>();
                    try
                    {
                        Date date = new Date();
                        foreach (DatesDate shipDate in house.Dates)
                        {
                            date = new CNodeBE.Date();
                            date.Value = shipDate.date_value;
                            date.Type = (CNodeBE.DateType)Enum.Parse(typeof(CNodeBE.DateType), NodeResources.GetEnum("DATETYPE", shipDate.date_type));
                            shipmentDateColl.Add(date);
                        }

                        subShipment.DateCollection = shipmentDateColl.ToArray();
                    }
                    catch (NullReferenceException)
                    {
                        NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "(B/L: " + house.bill_no + " No Dates found on HouseBill. " + "", Color.Blue);
                    }

                    decimal houseallocVol = 0;
                    if (decimal.TryParse(house.allocated_volume, out houseallocVol))
                    {
                        subShipment.TotalPreallocatedVolume = houseallocVol;
                    }


                    subShipment.TotalPreallocatedVolumeSpecified = true;
                    subShipment.TotalVolume = house.volume;
                    subShipment.TotalVolumeSpecified = true;
                    subShipment.TotalWeight = house.gross_weight;
                    subShipment.TotalWeightSpecified = true;


                    subShipment.TotalWeightUnit = new UnitOfWeight { Code = NodeResources.GetEnum("UOM", house.weight_unit) };
                    subShipment.FreightRateCurrency = new CNodeBE.Currency { Code = house.currency_code };
                    subShipment.DocumentedWeight = house.gross_weight;
                    subShipment.DocumentedWeightSpecified = true;
                    subShipment.ManifestedWeight = house.gross_weight;
                    subShipment.ManifestedWeightSpecified = true;
                    subShipment.ManifestedVolume = house.volume;
                    subShipment.ManifestedVolumeSpecified = true;
                    subShipment.ActualChargeable = house.chargeable_weight;
                    subShipment.ActualChargeableSpecified = true;
                    subShipment.OrganizationAddressCollection = houserOrgColl.ToArray();
                    subShipment.ShipmentIncoTerm = new CNodeBE.IncoTerm { Code = house.incoterm_code };
                    subShipment.GoodsDescription = house.commodity;
                    subShipment.OuterPacks = house.total_pieces;
                    subShipment.TotalVolume = house.volume;
                    foreach (PartiesParty party in house.Parties)
                    {
                        houserOrg = new OrganizationAddress();
                        switch (party.party_type)
                        {
                            case "OVSADD":

                                if (party.type_description == "Overseas Address (Exp)")
                                {
                                    houserOrg.AddressType = NodeResources.GetEnum("ORGTYPE", party.party_type);
                                    houserOrg.CompanyName = party.Address.name;
                                }
                                break;
                            case "CARRIER":
                                houserOrg.AddressType = NodeResources.GetEnum("ORGTYPE", party.party_type);
                                houserOrg.CompanyName = party.Address.name;
                                flightPrefix = party.Carrier.air_iata_carrier_code;
                                break;
                            default:
                                houserOrg.AddressType = NodeResources.GetEnum("ORGTYPE", party.party_type);
                                houserOrg.CompanyName = party.Company.name;
                                break;
                        }
                        if (string.IsNullOrEmpty(houserOrg.AddressType))
                        {
                            NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + house.bill_no + " - Party Type: " + party.party_type + " has not been mapped. " + "", Color.Red);
                            continue;
                        }
                        else
                        {
                            houserOrg.Address1 = party.Address.street_address_1;
                            houserOrg.Address2 = party.Address.street_address_2;
                            houserOrg.City = party.Address.city;
                            houserOrg.State = party.Address.state_province;
                            houserOrg.Postcode = party.Address.postal_code;
                            houserOrg.Country = new Country { Code = party.Address.country_code };
                            List<CNodeBE.RegistrationNumber> regoNumberColl = new List<RegistrationNumber>();
                            CNodeBE.RegistrationNumber regonumber = new RegistrationNumber();
                            regonumber.CountryOfIssue = new Country { Code = "AU" };
                            regonumber.Type = new RegistrationNumberType { Code = "LSC" };
                            regonumber.Value = party.Address.company_code;
                            regoNumberColl.Add(regonumber);
                            houserOrg.RegistrationNumberCollection = regoNumberColl.ToArray();
                            houserOrgColl.Add(houserOrg);
                        }

                    }
                    subShipment.OrganizationAddressCollection = houserOrgColl.ToArray();
                    switch (house.volume_unit.ToUpper())
                    {
                        case "M":
                            subShipment.TotalVolumeUnit = new CNodeBE.UnitOfVolume { Code = "M3" };
                            break;
                        case "F":
                            subShipment.TotalVolumeUnit = new CNodeBE.UnitOfVolume { Code = "CF" };
                            break;
                    }
                    List<CNodeBE.Container> contCollection = new List<CNodeBE.Container>();
                    List<CNodeBE.PackingLine> packCollection = new List<CNodeBE.PackingLine>();
                    List<RoutingsRouting> shipRoutings = new List<RoutingsRouting>();
                    shipRoutings = house.Routings.ToList();
                    int shipRoutes = shipRoutings.Count;
                    List<CNodeBE.TransportLeg> shipmentRouteColl = new List<CNodeBE.TransportLeg>();
                    if (house.mode_code == "AIR")
                    {

                    }
                    foreach (RoutingsRouting shipRoute in house.Routings)
                    {
                        CNodeBE.TransportLeg shipmentRoute = new CNodeBE.TransportLeg();
                        switch (shipRoute.routing_type.ToUpper())
                        {
                            case "MAIN":
                                if (house.mode_code.ToUpper() == "AIR")
                                {
                                    shipmentRoute.TransportMode = TransportLegTransportMode.Air;

                                    if (shipRoute.sequence_id == 1)
                                    {
                                        shipmentRoute.LegType = CNodeBE.TransportLegLegType.Flight1;


                                    }
                                    else
                                    {
                                        shipmentRoute.LegType = CNodeBE.TransportLegLegType.Flight2;
                                    }

                                }
                                else
                                {
                                    shipmentRoute.TransportMode = TransportLegTransportMode.Sea;
                                    shipmentRoute.LegType = CNodeBE.TransportLegLegType.Main;

                                }
                                break;
                            case "SECOND":
                                if (subShipment.TransportMode.Code == "AIR")
                                {
                                    shipmentRoute.LegType = CNodeBE.TransportLegLegType.Flight2;
                                    shipmentRoute.TransportMode = TransportLegTransportMode.Air;

                                }
                                else
                                {
                                    shipmentRoute.LegType = CNodeBE.TransportLegLegType.OnForwarding;
                                    shipmentRoute.TransportMode = TransportLegTransportMode.Sea;

                                }
                                break;
                        }
                        shipmentRoute.TransportModeSpecified = true;
                        if (shipRoutes == 1)
                        {
                            Date date = new Date();
                            shipmentRoute.EstimatedArrival = shipRoute.eta;
                            shipmentRoute.EstimatedDeparture = shipRoute.etd;
                            date.Value = shipRoute.etd;
                            date.Type = CNodeBE.DateType.Departure;
                            date.IsEstimate = true;
                            date.IsEstimateSpecified = true;
                            shipmentDateColl.Add(date);
                            date = new Date();
                            shipmentRoute.EstimatedArrival = shipRoute.eta;
                            shipmentRoute.EstimatedDeparture = shipRoute.etd;
                            date.Value = shipRoute.eta;
                            date.Type = CNodeBE.DateType.Arrival;
                            date.IsEstimate = true;
                            date.IsEstimateSpecified = true;
                            shipmentDateColl.Add(date);
                        }
                        else if (shipRoute.sequence_id == shipRoutes)
                        {
                            //     subShipment.PortOfDestination = new CNodeBE.UNLOCO { Code = route.destination_locode };
                            Date date = new Date();
                            shipmentRoute.EstimatedArrival = shipRoute.eta;
                            shipmentRoute.EstimatedDeparture = shipRoute.etd;
                            date.Value = shipRoute.eta;
                            date.Type = CNodeBE.DateType.Arrival;
                            date.IsEstimate = true;
                            date.IsEstimateSpecified = true;
                            shipmentDateColl.Add(date);
                        }
                        if ((shipRoute.sequence_id == 1 && shipRoutes > 1))
                        {
                            shipmentRoute.EstimatedArrival = shipRoute.eta;
                            //   subShipment.PortOfOrigin = new CNodeBE.UNLOCO { Code = route.departure_locode };
                            shipmentRoute.EstimatedDeparture = shipRoute.etd;
                            Date date = new Date();
                            date.Value = shipRoute.etd;
                            date.Type = CNodeBE.DateType.Departure;
                            date.IsEstimate = true;
                            date.IsEstimateSpecified = true;
                            shipmentDateColl.Add(date);
                        }
                        shipmentRoute.LegOrder = (sbyte)shipRoute.sequence_id;
                        //subShipment.PortOfDestination = new CNodeBE.UNLOCO { Code = route.destination_locode };
                        shipmentRoute.PortOfLoading = new CNodeBE.UNLOCO { Code = shipRoute.departure_locode };
                        shipmentRoute.PortOfDischarge = new CNodeBE.UNLOCO { Code = shipRoute.destination_locode };
                        shipmentRoute.LegTypeSpecified = true;
                        subShipment.DateCollection = shipmentDateColl.ToArray();
                        switch (shipRoute.mode_code.ToUpper())
                        {
                            case "OCEAN":
                                shipmentRoute.TransportMode = CNodeBE.TransportLegTransportMode.Sea;
                                break;
                            case "AIR":
                                shipmentRoute.TransportMode = CNodeBE.TransportLegTransportMode.Air;

                                break;
                            case "RAIL":
                                shipmentRoute.TransportMode = CNodeBE.TransportLegTransportMode.Rail;
                                break;
                        }
                        shipmentRoute.TransportModeSpecified = true;
                        try
                        {
                            if (sqlConn.State == ConnectionState.Open)
                            {
                                sqlConn.Close();
                            }
                            sqlConn.Open();
                        }
                        catch (Exception)
                        {

                        }
                        shipRoutes = house.Routings.Length;
                        shipmentRoute.VesselName = shipRoute.vessel_name;
                        shipmentRoute.VesselLloydsIMO = shipRoute.vessel_code;
                        shipmentRoute.VoyageFlightNo = flightPrefix + shipRoute.flight_voyage_no;
                        //shipmentRouteColl.Add(shipmentRoute);
                    }
                    subShipment.TransportLegCollection = shipmentRouteColl.ToArray();
                    if (house.load_type != "FCL")
                    {

                    }
                    int pcCount = 0;
                    string pkgType = string.Empty;
                    foreach (ItemsItem houseItem in house.Items)
                    {
                        CNodeBE.PackingLine pl = new CNodeBE.PackingLine();
                        pl.GoodsDescription = houseItem.item_description;
                        pl.PackQty = houseItem.pieces;
                        pcCount += houseItem.pieces;
                        if (string.IsNullOrEmpty(houseItem.pieces_unit_type))
                        {
                            pl.PackType = new CNodeBE.PackageType { Code = "PCE" };
                            pkgType = "PCE";
                        }
                        else
                        {
                            pl.PackType = new CNodeBE.PackageType { Code = NodeResources.GetEnum("PACKAGE", houseItem.pieces_unit_type) };
                            pkgType = houseItem.pieces_unit_type;

                        }
                        subShipment.OuterPacksSpecified = true;
                        pl.MarksAndNos = houseItem.marks_numbers;
                        pl.PackQtySpecified = true;
                        pl.Weight = houseItem.gross_weight;
                        pl.WeightSpecified = true;
                        switch (house.weight_unit.ToUpper())
                        {
                            case "K":

                                pl.WeightUnit = new CNodeBE.UnitOfWeight { Code = "KG" };
                                pl.WeightSpecified = true;
                                break;
                            case "L":

                                pl.WeightUnit = new CNodeBE.UnitOfWeight { Code = "LB" };
                                pl.WeightSpecified = true;
                                break;
                            default:
                                pl.WeightUnit = new CNodeBE.UnitOfWeight { Code = "KG" };
                                break;
                        }
                        if (houseItem.volume != 0)
                        {
                            pl.Volume = houseItem.volume;
                        }
                        else
                        {
                            pl.Volume = houseItem.cubic_volume / 1000000;
                        }
                        pl.VolumeSpecified = true;
                        try
                        {
                            switch (house.volume_unit.ToUpper())
                            {
                                case "M":
                                    pl.VolumeUnit = new CNodeBE.UnitOfVolume { Code = "M3" };

                                    break;
                                case "F":
                                    pl.VolumeUnit = new CNodeBE.UnitOfVolume { Code = "CF" };

                                    break;
                                default:
                                    pl.VolumeUnit = new CNodeBE.UnitOfVolume { Code = "M3" };
                                    break;
                            }
                        }
                        catch (Exception)
                        {
                            pl.VolumeUnit = new CNodeBE.UnitOfVolume { Code = "M3" };
                        }
                        if (houseItem.container_no != null && house.mode_code == "AIR")
                        {
                            CNodeBE.Container cnt = new CNodeBE.Container();
                            pl.ContainerNumber = houseItem.container_no.Replace(" ", string.Empty);
                            cnt.ContainerNumber = pl.ContainerNumber;
                            cnt.Seal = houseItem.seal_no.ToString().Trim();
                            if (!string.IsNullOrEmpty(cnt.ContainerNumber))
                            {
                                contCollection.Add(cnt);
                            }
                        }
                        if (houseItem.Dimensions != null)
                        {

                            foreach (ItemsItem.ItemsItemDimensionsDimension dim in houseItem.Dimensions)
                            //foreach (Items. dim in houseItem.Dimensions)
                            {
                                if (dim.unit_type.Trim() == "C")
                                {
                                    if (dim.height > 0)
                                    {
                                        pl.Height = dim.height / 100;
                                    }
                                    else
                                    {
                                        pl.Height = 0;
                                    }

                                    if (dim.width > 0)
                                    {
                                        pl.Width = dim.width / 100;
                                    }
                                    else
                                    {
                                        pl.Width = 0;
                                    }

                                    if (pl.Length > 0)
                                    {
                                        pl.Length = dim.length / 100;
                                    }
                                    else
                                    {
                                        pl.Length = 0;
                                    }

                                    pl.LengthUnit = new UnitOfLength { Code = "M", Description = "Metres" };
                                    pl.LengthSpecified = true;
                                    pl.HeightSpecified = true;
                                    pl.WidthSpecified = true;
                                }
                                else
                                {
                                }
                            }
                        }
                        packCollection.Add(pl);
                    }
                    subShipment.PackingLineCollection = packCollection.ToArray();
                    result.Add(subShipment);
                    subShipment.OuterPacks = pcCount;
                    subShipment.OuterPacksPackageType = new PackageType { Code = NodeResources.GetEnum("PACKAGE", pkgType) };
                    subShipment.OuterPacksSpecified = true;
                    subShipment.TotalNoOfPieces = pcCount;
                    subShipment.TotalNoOfPiecesSpecified = true;
                    if (contCollection.Count > 0)
                    {
                        subShipment.ContainerCollection = new CNodeBE.ShipmentContainerCollection { Container = contCollection.ToArray() };
                        subShipment.ContainerCount = contCollection.Count;
                        subShipment.ContainerCollection.Content = CollectionContent.Complete;
                        subShipment.ContainerCollection.ContentSpecified = true;
                    }
                }
                catch (Exception ex)
                {
                    //ProcessingErrors procerror = new ProcessingErrors();
                    //CTCErrorCode error = new CTCErrorCode();
                    //error.Code = NodeError.e111;
                    //error.Description = "Serialization Error :" + ex.Message;
                    //error.Severity = "Warning";
                    //procerror.ErrorCode = error;
                    //procerror.SenderId = string.Empty;
                    //procerror.RecipientId = string.Empty;
                    //procerror.FileName = xmlFile;
                    //NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                    ////NodeResources.AddProcessingError(procerror);
                    ////thisResult.FolderLoc = Globals.glFailPath;
                    ////thisResult.Processed = true;
                    ////processed = true;
                    string strEx = ex.GetType().Name;
                    NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node " + strEx + " Exception Found: Housebill Error:" + ex.Message + ". " + "", Color.Red);
                }
            }
            return result;
        }

        private string FixNumber(string bl)
        {
            string result;
            if (bl.Contains("/"))
            {
                result = bl.Replace("/", "-");
            }
            else
            {
                result = bl;
            }

            return result;
        }

        private ProcessResult ProcessCargowiseFiles(string xmlFile)
        {
            FileInfo fi = new FileInfo(xmlFile);

            NodeResources.AddRTBText(rtbLog, "Cargowise File Processing: " + fi.Name);
            ProcessResult thisResult = new ProcessResult();
            FileInfo processingFile = new FileInfo(xmlFile);
            thisResult.Processed = true;
            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            String eventCode = String.Empty;
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            cwRx++;
            NodeResources.AddLabel(lblCwRx, cwRx.ToString());
            TransReference trRef = new TransReference();
            String archiveFile;
            String nativeString = String.Empty;
            UniversalInterchange toConvert;
            Shipment consol = new Shipment();
            try
            {
                using (FileStream fStream = new FileStream(xmlFile, FileMode.Open))
                {
                    XmlSerializer cwConvert = new XmlSerializer(typeof(UniversalInterchange));
                    toConvert = (UniversalInterchange)cwConvert.Deserialize(fStream);
                    fStream.Close();
                }
                consol = toConvert.Body.BodyField.Shipment;
                senderID = toConvert.Header.SenderID;
                recipientID = toConvert.Header.RecipientID;
                reasonCode = consol.DataContext.ActionPurpose.Code;
                eventCode = consol.DataContext.EventType.Code;
            }
            catch (Exception ex)
            {
                ProcessingErrors procerror = new ProcessingErrors();
                CTCErrorCode error = new CTCErrorCode();
                error.Code = NodeError.e110;
                error.Description = "Error Processing NDM File. :" + ex.Message;
                error.Severity = "Warning";
                procerror.ErrorCode = error;
                procerror.SenderId = senderID;
                procerror.RecipientId = recipientID;
                procerror.FileName = xmlFile;
                //                AddProcessingError(procerror);
                thisResult.FolderLoc = Globals.glFailPath;
                thisResult.Processed = false;
                thisResult.FileName = xmlFile;
                return thisResult;
            }
            SqlCommand sqlCustProfile = new SqlCommand("Select C_ID, P_ID, C_NAME, C_IS_ACTIVE, C_ON_HOLD, C_PATH, P_EVENTCODE, " +
                                                           "C_FTP_CLIENT, P_REASONCODE, P_SERVER, P_USERNAME, P_DELIVERY, " +
                                                           "P_PATH, P_PASSWORD, P_MSGTYPE, P_RECIPIENTID, P_SENDERID, P_EMAILADDRESS, P_SUBJECT," +
                                                           "P_DESCRIPTION, P_BILLTO, P_PORT, P_DIRECTION, C_CODE, P_DTS, P_METHOD, " +
                                                           "P_ACTIVE from vw_CustomerProfile WHERE P_SENDERID = @SENDERID " + //Removed the P_DIRECTION ='Y' not sure why it was there. 
                                                           "and P_RECIPIENTID =@RECIPIENTID and (@REASONCODE IS NULL OR " +
                                                           "P_REASONCODE = @REASONCODE) and (@EVENTCODE is NULL or P_EVENTCODE = @EVENTCODE)" +
                                                           "and P_ACTIVE='Y'", sqlConn);
            sqlCustProfile.Parameters.AddWithValue("@SENDERID", senderID);
            sqlCustProfile.Parameters.AddWithValue("@RECIPIENTID", recipientID);
            sqlCustProfile.Parameters.AddWithValue("@REASONCODE", reasonCode);
            sqlCustProfile.Parameters.AddWithValue("@EVENTCODE", eventCode);

            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            sqlConn.ConnectionString = Globals.connString();
            sqlConn.Open();
            CustProfileRecord custProfile = new CustProfileRecord();
            using (SqlDataReader drCustProfile = sqlCustProfile.ExecuteReader(CommandBehavior.CloseConnection))
            {
                if (drCustProfile.HasRows)
                {
                    while (drCustProfile.Read())
                    {
                        CustProfileRecord result = new CustProfileRecord();
                        custProfile.C_id = (Guid)drCustProfile["C_ID"];
                        custProfile.P_id = (Guid)drCustProfile["P_ID"];
                        custProfile.C_name = drCustProfile["C_NAME"].ToString();
                        custProfile.C_is_active = drCustProfile["C_IS_ACTIVE"].ToString();
                        custProfile.C_on_hold = drCustProfile["C_ON_HOLD"].ToString();
                        custProfile.C_path = drCustProfile["C_PATH"].ToString();
                        custProfile.C_ftp_client = drCustProfile["C_FTP_CLIENT"].ToString();
                        custProfile.P_reasoncode = drCustProfile["P_REASONCODE"].ToString();
                        custProfile.P_EventCode = drCustProfile["P_EVENTCODE"].ToString();
                        custProfile.P_server = drCustProfile["P_SERVER"].ToString();
                        custProfile.P_username = drCustProfile["P_USERNAME"].ToString();
                        custProfile.P_delivery = drCustProfile["P_DELIVERY"].ToString();
                        custProfile.P_password = drCustProfile["P_PASSWORD"].ToString();
                        custProfile.P_recipientid = drCustProfile["P_RECIPIENTID"].ToString();
                        custProfile.P_Senderid = drCustProfile["P_SENDERID"].ToString();
                        custProfile.P_description = drCustProfile["P_DESCRIPTION"].ToString();
                        custProfile.P_path = drCustProfile["P_PATH"].ToString();
                        custProfile.P_port = drCustProfile["P_PORT"].ToString();
                        custProfile.C_code = drCustProfile["C_CODE"].ToString();
                        custProfile.P_BillTo = drCustProfile["P_BILLTO"].ToString();
                        custProfile.P_Direction = drCustProfile["P_DIRECTION"].ToString();
                        custProfile.P_MsgType = drCustProfile["P_MSGTYPE"].ToString();
                        custProfile.P_Subject = drCustProfile["P_SUBJECT"].ToString();
                        custProfile.P_EmailAddress = drCustProfile["P_EMAILADDRESS"].ToString();
                        custProfile.P_Method = drCustProfile["P_METHOD"].ToString();
                        if (drCustProfile["P_DTS"].ToString() == "Y")
                        {
                            custProfile.P_DTS = true;
                        }
                        else
                        {
                            custProfile.P_DTS = false;
                        }
                    }
                }
                else
                {
                    SqlCommand execAdd = new SqlCommand();
                    execAdd.Connection = sqlConn;
                    execAdd.CommandText = "Add_Profile";
                    execAdd.CommandType = CommandType.StoredProcedure;
                    SqlParameter PC = execAdd.Parameters.Add("@P_C", SqlDbType.UniqueIdentifier);
                    PC.Value = Globals.gl_CustId;
                    SqlParameter PReasonCode = execAdd.Parameters.Add("@P_REASONCODE", SqlDbType.Char, 3);
                    PReasonCode.Value = reasonCode.ToUpper();
                    SqlParameter PEventCode = execAdd.Parameters.Add("@P_EVENTCODE", SqlDbType.Char, 3);
                    PEventCode.Value = eventCode.ToUpper();
                    SqlParameter PServer = execAdd.Parameters.Add("@P_SERVER", SqlDbType.VarChar, 50);
                    PServer.Value = "";
                    SqlParameter PUsername = execAdd.Parameters.Add("@P_USERNAME", SqlDbType.VarChar, 50);
                    PUsername.Value = "";
                    SqlParameter PPath = execAdd.Parameters.Add("@P_PATH", SqlDbType.VarChar, 100);
                    PPath.Value = "";
                    SqlParameter PPassword = execAdd.Parameters.Add("@P_PASSWORD", SqlDbType.VarChar, 50);
                    PPassword.Value = "";
                    string sDelivery = String.Empty;
                    sDelivery = "R";
                    SqlParameter pDelivery = execAdd.Parameters.Add("@P_DELIVERY", SqlDbType.Char, 1);
                    pDelivery.Value = sDelivery;
                    SqlParameter pMSGType = execAdd.Parameters.Add("@MSGTYPE", SqlDbType.VarChar, 20);
                    pMSGType.Value = "Original";
                    SqlParameter pBillType = execAdd.Parameters.Add("@BILLTO", SqlDbType.VarChar, 15);
                    pBillType.Value = senderID;
                    SqlParameter pChargeable = execAdd.Parameters.Add("@CHARGEABLE", SqlDbType.Char, 1);
                    pChargeable.Value = "Y";
                    SqlParameter PPort = execAdd.Parameters.Add("@P_PORT", SqlDbType.Char, 10);
                    PPort.Value = "";
                    SqlParameter PDescription = execAdd.Parameters.Add("@P_DESCRIPTION", SqlDbType.VarChar, 100);
                    PDescription.Value = "No Transport Profile found. Store in Folder";
                    SqlParameter PRecipientId = execAdd.Parameters.Add("@P_RECIPIENTID", SqlDbType.VarChar, 15);
                    PRecipientId.Value = recipientID;
                    SqlParameter PSenderID = execAdd.Parameters.Add("P_SENDERID", SqlDbType.VarChar, 15);
                    PSenderID.Value = senderID;
                    SqlParameter p_id = execAdd.Parameters.Add("@R_ID", SqlDbType.UniqueIdentifier);
                    SqlParameter PDirection = execAdd.Parameters.Add("@P_DIRECTION", SqlDbType.Char, 1);
                    PDirection.Value = "R";
                    p_id.Direction = ParameterDirection.Output;
                    SqlParameter r_Action = execAdd.Parameters.Add("@R_ACTION", SqlDbType.Char, 1);
                    r_Action.Direction = ParameterDirection.Output;
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                    }
                    sqlConn.Open();
                    execAdd.ExecuteNonQuery();
                    sqlConn.Close();
                    NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Create New Profile Sender ID: " + senderID + " Recipient ID: " + recipientID + " Purpose Code: " + reasonCode + " Event Code:" + eventCode);
                    ProcessingErrors procerror = new ProcessingErrors();
                    CTCErrorCode error = new CTCErrorCode();
                    error.Code = NodeError.e100;
                    error.Description = "Customer Exists but no profile Found.";
                    error.Severity = "Warning";
                    procerror.ErrorCode = error;
                    procerror.SenderId = senderID;
                    procerror.RecipientId = recipientID;
                    procerror.FileName = xmlFile;
                    //                    AddProcessingError(procerror);
                    thisResult.FolderLoc = Globals.glFailPath;
                    thisResult.Processed = false;
                    thisResult.FileName = xmlFile;
                    //                    AddTransactionLog(Guid.Empty, senderID, xmlFile, "Customer Found but no Profile found. Move to Failed Location for retry.", DateTime.Now, "Y", senderID, recipientID);
                    return thisResult;

                }
                drCustProfile.Close();
            }
            //SqlCommand remTodo = new SqlCommand("DELETE FROM TODO WHERE L_ID =@ID", sqlConn);
            //SqlParameter remID = remTodo.Parameters.Add("@ID", SqlDbType.UniqueIdentifier);
            if (custProfile != null)
            {
                //Is Client Active and not on hold
                if (custProfile.C_is_active == "N" | custProfile.C_on_hold == "Y")
                {
                    string f = NodeResources.MoveFile(xmlFile, Path.Combine(custProfile.C_path, "Held"), rtbLog);
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    { xmlFile = f; }

                    NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfile.C_name + " is not Active or is on Hold. Moving to Hold and skipping" + "");
                    thisResult.Processed = true;
                    thisResult.FileName = xmlFile;
                    return thisResult;
                }
                else
                {
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                    }
                    //Send file to Archive folder
                    archiveFile = Globals.ArchiveFile(Globals.glArcLocation, xmlFile);
                    DirectoryInfo di = new DirectoryInfo(Path.Combine(Globals.glPickupPath, recipientID + reasonCode));
                    if (!di.Exists)
                    {
                        di.Create();
                    }

                    string f = NodeResources.MoveFile(xmlFile, Path.Combine(Globals.glPickupPath, recipientID + reasonCode), rtbLog);
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);

                    }
                    else
                    {
                        xmlFile = f;
                        Type custType = this.GetType();
                        MethodInfo custMethodToRun = custType.GetMethod(custProfile.P_Method);
                        var varResult = custMethodToRun.Invoke(this, new Object[] { toConvert, xmlFile });
                        if (varResult != null)
                        {
                            //AddTransaction((Guid)dr["C_ID"], (Guid)dr["P_ID"], xmlFile, msgDir, true, (TransReference)varResult, archiveFile);
                            //                        canDelete = true;
                            //                        processed = true;
                            thisResult.Processed = true;
                        }
                        //Message type will determine whether the message is sending to eAdapter (New Native file Imported/Converted) or sent to customer service. 
                        NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Profile : " + custProfile.P_description);
                    }
                    //AddCargowiseData(consol);

                }
            }


            File.Delete(xmlFile);
            return thisResult;

        }

        public TransReference AddCargowiseData(UniversalInterchange uinter, string fileName)
        {
            Shipment consol = uinter.Body.BodyField.Shipment;
            TransReference trRef = new TransReference();
            SqlCommand sqlAddCW = new SqlCommand("Add_Consol", sqlConn);
            sqlAddCW.CommandType = CommandType.StoredProcedure;
            SqlCommand sqlAddShipment = new SqlCommand("Add_Shipment", sqlConn);
            sqlAddShipment.CommandType = CommandType.StoredProcedure;
            SqlParameter hSid = sqlAddShipment.Parameters.Add("@H_ID", SqlDbType.UniqueIdentifier);
            hSid.Direction = ParameterDirection.Output;
            SqlParameter hbl = sqlAddShipment.Parameters.Add("@H_HBL", SqlDbType.VarChar, 50);
            SqlParameter jid = sqlAddShipment.Parameters.Add("@H_J", SqlDbType.UniqueIdentifier);
            SqlParameter shipmentno = sqlAddShipment.Parameters.Add("@H_SHIPMENT", SqlDbType.NChar, 15);

            SqlParameter obl = sqlAddCW.Parameters.Add("@J_OBL", SqlDbType.VarChar, 50);
            SqlParameter pol = sqlAddCW.Parameters.Add("@J_POL", SqlDbType.Char, 5);
            SqlParameter pod = sqlAddCW.Parameters.Add("@J_POD", SqlDbType.Char, 5);
            SqlParameter contNo = sqlAddCW.Parameters.Add("@J_CONTAINERNO", SqlDbType.VarChar, 20);
            SqlParameter sealNo = sqlAddCW.Parameters.Add("@J_SEALNO", SqlDbType.VarChar, 50);
            SqlParameter voy = sqlAddCW.Parameters.Add("@J_VOYAGE", SqlDbType.VarChar, 50);
            SqlParameter etd = sqlAddCW.Parameters.Add("@J_ETD", SqlDbType.DateTime);
            SqlParameter eta = sqlAddCW.Parameters.Add("@J_ETA", SqlDbType.DateTime);
            SqlParameter mode = sqlAddCW.Parameters.Add("@J_MODE", SqlDbType.Char, 3);
            SqlParameter obd = sqlAddCW.Parameters.Add("@J_ONBOARD", SqlDbType.DateTime);
            SqlParameter contMode = sqlAddCW.Parameters.Add("@J_CONTAINERMODE", SqlDbType.Char, 3);
            SqlParameter consolNo = sqlAddCW.Parameters.Add("@J_CONSOL", SqlDbType.NChar, 15);
            SqlParameter vess = sqlAddCW.Parameters.Add("@J_VESSEL", SqlDbType.VarChar, 100);
            SqlParameter id = sqlAddCW.Parameters.Add("@J_ID", SqlDbType.UniqueIdentifier);
            id.Direction = ParameterDirection.Output;
            try
            {
                trRef.Ref1Type = TransReference.RefType.Master;
                trRef.Reference1 = consol.WayBillNumber;
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.Open();
                if (consol.WayBillType.Code == "MWB")
                {
                    trRef.Ref1Type = TransReference.RefType.Master;
                    trRef.Reference1 = consol.WayBillNumber;
                    obl.Value = consol.WayBillNumber;
                }
                foreach (DataSource ds in consol.DataContext.DataSourceCollection)
                {
                    if (ds.Type == "ForwardingConsol")
                    {
                        ;
                    }

                    consolNo.Value = ds.Key;
                }
                pol.Value = consol.PortOfLoading.Code;
                pod.Value = consol.PortOfDischarge.Code;
                if (!string.IsNullOrEmpty(consol.VesselName))
                {

                    vess.Value = consol.VesselName;
                }

                voy.Value = consol.VoyageFlightNo;

                contMode.Value = consol.TransportMode.Code;
                List<Shipment> shipmentColl = consol.SubShipmentCollection.ToList();
                if (shipmentColl.Count > 0)
                {
                    foreach (Date date in shipmentColl[0].DateCollection)
                    {
                        if (date.Type == DateType.Arrival)
                        {
                            eta.Value = TryParse(date.Value);
                        }

                        if (eta.Value == null)
                        {
                            eta.Value = DBNull.Value;
                        }

                        if (date.Type == DateType.Departure)
                        {
                            etd.Value = TryParse(date.Value);
                        }

                        if (etd.Value == null)
                        {
                            etd.Value = DBNull.Value;
                        }

                        if (date.Type == DateType.ShippedOnBoard)
                        {
                            obd.Value = TryParse(date.Value);
                        }

                        if (obd.Value == null)
                        {
                            obd.Value = DBNull.Value;
                        }
                    }

                }
                else
                {
                    foreach (Date date in consol.DateCollection)
                    {
                        if (date.Type == DateType.Arrival)
                        {
                            eta.Value = TryParse(date.Value);
                        }

                        if (date.Type == DateType.Departure)
                        {
                            etd.Value = TryParse(date.Value);
                        }

                        if (date.Type == DateType.ShippedOnBoard)
                        {
                            obd.Value = TryParse(date.Value);
                        }
                    }
                }

                mode.Value = consol.TransportMode.Code;
                switch (consol.TransportMode.Code)
                {
                    case "SEA":

                        if (consol.ContainerMode.Code == "FCL")
                        {
                            if (consol.ContainerCollection != null)
                            {
                                foreach (Container cn in consol.ContainerCollection.Container)
                                {

                                    contNo.Value = cn.ContainerNumber;
                                    contMode.Value = consol.ContainerMode.Code;
                                    sealNo.Value = cn.Seal;
                                    sqlAddCW.ExecuteNonQuery();
                                    foreach (Shipment shipment in shipmentColl)
                                    {
                                        foreach (DataSource ds in shipment.DataContext.DataSourceCollection)
                                        {
                                            if (ds.Type == "ForwardingShipment")
                                            {
                                                shipmentno.Value = ds.Key;
                                            }
                                        }
                                        hbl.Value = shipment.WayBillNumber;
                                        jid.Value = id.Value;
                                        sqlAddShipment.ExecuteNonQuery();
                                    }
                                }
                            }
                            else
                            {
                                foreach (Shipment shipment in shipmentColl)
                                {
                                    foreach (Container cn in shipment.ContainerCollection.Container)
                                    {

                                        contNo.Value = cn.ContainerNumber;
                                        contMode.Value = shipment.ContainerMode.Code;
                                        sqlAddCW.ExecuteNonQuery();
                                        foreach (DataSource ds in shipment.DataContext.DataSourceCollection)
                                        {
                                            if (ds.Type == "ForwardingShipment")
                                            {
                                                shipmentno.Value = ds.Key;
                                            }
                                        }
                                        hbl.Value = shipment.WayBillNumber;
                                        jid.Value = id.Value;
                                        sqlAddShipment.ExecuteNonQuery();




                                    }


                                }
                            }

                        }

                        else
                        {
                            foreach (Shipment lclShipment in shipmentColl)
                            {
                                foreach (PackingLine pl in lclShipment.PackingLineCollection)
                                {

                                    contNo.Value = pl.ContainerNumber.ToString();
                                    contMode.Value = lclShipment.ContainerMode.Code;
                                    sqlAddCW.ExecuteNonQuery();

                                    foreach (DataSource ds in lclShipment.DataContext.DataSourceCollection)
                                    {
                                        if (ds.Type == "ForwardingShipment")
                                        {
                                            shipmentno.Value = ds.Key;
                                        }
                                    }
                                    hbl.Value = lclShipment.WayBillNumber;
                                    jid.Value = id.Value;
                                    sqlAddShipment.ExecuteNonQuery();



                                }


                            }
                        }
                        break;
                    case "AIR":
                        sqlAddCW.ExecuteNonQuery();
                        foreach (Shipment shipment in shipmentColl)
                        {
                            foreach (DataSource ds in shipment.DataContext.DataSourceCollection)
                            {
                                if (ds.Type == "ForwardingShipment")
                                {
                                    shipmentno.Value = ds.Key;
                                }
                            }
                            hbl.Value = shipment.WayBillNumber;
                            jid.Value = id.Value;
                            sqlAddShipment.ExecuteNonQuery();
                        }
                        break;

                }

            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error found when importing Cargowise data. Error was: " + ex.InnerException.Message);
                ProcessingErrors procerror = new ProcessingErrors();
                CTCErrorCode error = new CTCErrorCode();
                error.Code = NodeError.e110;
                error.Description = "Error importing Cargowise Data File";
                error.Severity = "Warning";
                procerror.ErrorCode = error;
                procerror.SenderId = uinter.Header.SenderID;
                procerror.RecipientId = uinter.Header.RecipientID;
                procerror.FileName = fileName;
                NodeResources.AddProcessingError(procerror);
                // thisResult.FolderLoc = Globals.glFailPath;
                // thisResult.Processed = false;
                // thisResult.FileName = xmlFile;
            }

            return trRef;
        }


        private void ProcessCustomXML(string XMLFile)
        {


        }

        private void customMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEnums Enums = new frmEnums();
            Enums.Show();

        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            this.tslMain.Width = this.Width / 2;
            this.tslSpacer.Width = (this.Width / 2) - (productionToolStripMenuItem.Width + tslCmbMode.Width);
        }

        private void productionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (productionToolStripMenuItem.Checked)
            {
                testingToolStripMenuItem.Checked = false;
                tslMode.Text = "Production";
            }
            else
            {
                testingToolStripMenuItem.Checked = true;
                tslMode.Text = "Testing";
            }
        }

        private void testingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (testingToolStripMenuItem.Checked)
            {
                productionToolStripMenuItem.Checked = false;
                tslMode.Text = "Testing";
            }
            else
            {
                productionToolStripMenuItem.Checked = true;
                tslMode.Text = "Production";
            }
        }

        private void eAdapterTestToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}

