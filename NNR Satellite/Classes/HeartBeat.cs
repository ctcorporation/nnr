﻿using NNR_Satellite.DTO;
using NNR_Satellite.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace NNR_Satellite
{

    public static class HeartBeat
    {
        private static List<Assembly> GetListOfEntryAssemblyWithReferences()
        {
            List<Assembly> listOfAssemblies = new List<Assembly>();
            var mainAsm = Assembly.GetEntryAssembly();
            listOfAssemblies.Add(mainAsm);

            foreach (var refAsmName in mainAsm.GetReferencedAssemblies())
            {
                listOfAssemblies.Add(Assembly.Load(refAsmName));
            }
            return listOfAssemblies;
        }
        public static void RegisterHeartBeat(string custCode, string operation, ParameterInfo[] parameters, string pathParam)
        {
            Process currentProcess = Process.GetCurrentProcess();
            var appParam = Assembly.GetEntryAssembly().GetName().Name;
            Process proc = Process.GetCurrentProcess();
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appDataPath, System.Diagnostics.Process.GetCurrentProcess().ProcessName);

            DateTime? checkinParam = null;
            DateTime? openParam = null;
            int pid = currentProcess.Id;
            if (operation == "Starting")
            {
                checkinParam = DateTime.Now;
                openParam = DateTime.Now;

            }
            if (operation == "Stopping")
            {
                pid = 0;
                openParam = null;
                checkinParam = null;


            }
            string pList = string.Empty;
            if (parameters != null)
            {
                for (int i = 0; i < parameters.Length; i++)
                {
                    pList += parameters[i].Name + ": " + parameters[i].ToString();
                }
            }
            try
            {
                using (ICNodeUnitOfWork uow = new CNodeUnitOfWork(new CNodeEntity()))
                {

                    var heartbeat = (uow.HeartBeat.Find(h => h.HB_NAME == appParam)).FirstOrDefault();
                    var custId = GetCustId(custCode);
                    if (heartbeat == null)
                    {
                        uow.HeartBeat.Add(new Models.HeartBeat
                        {
                            HB_C = custId != null ? custId : Guid.Empty,
                            HB_PATH = pathParam,
                            HB_PID = pid,
                            HB_LASTOPERATIONPARAMS = pList,
                            HB_LASTCHECKIN = checkinParam,
                            HB_LASTOPERATION = operation,
                            HB_NAME = appParam,
                            HB_OPENED = openParam,
                            HB_ISMONITORED = "Y"
                        });

                    }
                    else
                    {
                        if (openParam != null)
                        {
                            heartbeat.HB_OPENED = openParam;
                        }
                        heartbeat.HB_LASTCHECKIN = DateTime.Now;
                        heartbeat.HB_LASTOPERATION = operation;
                        heartbeat.HB_LASTOPERATIONPARAMS = pList;
                        heartbeat.HB_PID = pid;
                        heartbeat.HB_PATH = pathParam;
                    }
                    uow.Complete();

                }


            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;

            }

        }

        private static Guid? GetCustId(string custCode)
        {
            using (ICNodeUnitOfWork uow = new CNodeUnitOfWork(new CNodeEntity()))
            {
                var cust = (uow.CustRepository.Find(c => c.C_CODE == custCode)).FirstOrDefault();
                if (cust != null)
                {
                    return cust.C_ID;
                }
            }
            return null;
        }
    }
    //static class HeartBeat
    //{

    //    public static void RegisterHeartBeat(string custCode, string operation, ParameterInfo[] parameters)
    //    {
    //        try
    //        {
    //            SqlConnection sqlConn = new SqlConnection { ConnectionString = Globals.CTCconnString() };
    //            SqlCommand addHeartbeat = new SqlCommand
    //            {
    //                CommandType = System.Data.CommandType.StoredProcedure,
    //                CommandText = "AddHeartBeat",
    //                Connection = sqlConn
    //            };

    //            Process currentProcess = Process.GetCurrentProcess();
    //            int pid = currentProcess.Id;
    //            SqlParameter pidParam = addHeartbeat.Parameters.Add("@PID", SqlDbType.Int);
    //            SqlParameter checkinParam = addHeartbeat.Parameters.Add("@CHECKIN", SqlDbType.DateTime);
    //            SqlParameter openParam = addHeartbeat.Parameters.Add("@OPENED", SqlDbType.DateTime);
    //            SqlParameter lastopParam = addHeartbeat.Parameters.Add("@LASTOP", SqlDbType.VarChar, 50);
    //            SqlParameter custidParam = addHeartbeat.Parameters.Add("@CUSTID", SqlDbType.UniqueIdentifier);
    //            SqlParameter startParam = addHeartbeat.Parameters.Add("@STARTING", SqlDbType.Bit);
    //            SqlParameter pathParam = addHeartbeat.Parameters.Add("@PATH", SqlDbType.VarChar, 200);
    //            SqlParameter appParam = addHeartbeat.Parameters.Add("@APPNAME", SqlDbType.VarChar, 50);
    //            SqlParameter paramParams = addHeartbeat.Parameters.Add("@PARAMS", SqlDbType.NVarChar, 100);
    //            appParam.Value = Assembly.GetExecutingAssembly().GetName().Name;
    //            pathParam.Value = Path.GetDirectoryName(Application.ExecutablePath);
    //            custidParam.Value = NodeResources.GetCustomer(custCode);
    //            pidParam.Value = pid;
    //            checkinParam.Value = DateTime.Now;

    //            if (operation == "Starting")
    //            {
    //                startParam.Value = 1;
    //                openParam.Value = DateTime.Now;
    //            }
    //            if (operation == "Stopping")
    //            {
    //                startParam.Value = 1;
    //                openParam.Value = null;
    //                checkinParam.Value = null;
    //                pidParam.Value = 0;

    //            }

    //            lastopParam.Value = operation;
    //            string pList = string.Empty;
    //            if (parameters != null)
    //            {
    //                for (int i = 0; i < parameters.Length; i++)
    //                {
    //                    pList += parameters[i].Name + ": " + parameters[i].ToString();
    //                }
    //            }

    //            paramParams.Value = pList;
    //            if (sqlConn.State == System.Data.ConnectionState.Open)
    //            {
    //                sqlConn.Close();
    //            }
    //            sqlConn.Open();
    //            addHeartbeat.ExecuteNonQuery();
    //        }
    //        catch (Exception ex)
    //        {
    //            string strEx = ex.GetType().Name;

    //        }

    //    }
    //}
}
