﻿using System.Collections.Generic;

namespace NNR_Satellite.Classes
{
    public class MasterHouseRef
    {
        public string Master { get; set; }

        public List<string> HouseBills { get; set; }

        public bool CWFileCreated { get; set; }

        public string Country { get; set; }

        public string CWCode { get; set; }
    }
}
