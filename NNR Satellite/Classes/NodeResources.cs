﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using static NNR_Satellite.SatelliteErrors;

namespace NNR_Satellite
{
    class NodeResources
    {
        public static string MoveFile(string att, string folder)
        {

            int icount = 1;

            string filename = Path.GetFileNameWithoutExtension(att);
            string ext = Path.GetExtension(att);
            string newfile = Path.Combine(folder, Path.GetFileName(att));
            while (File.Exists(newfile))
            {
                newfile = Path.Combine(folder, filename + icount + ext);
                icount++;

            }

            try
            {
                File.Move(att, Path.Combine(folder, newfile));
            }
            catch (Exception)
            {


            }
            return newfile;
        }
        public static string MoveFile(string att, string folder, RichTextBox edLog)
        {

            int icount = 1;

            string filename = Path.GetFileNameWithoutExtension(att);
            string ext = Path.GetExtension(att);
            string newfile = Path.Combine(folder, Path.GetFileName(att));
            while (File.Exists(newfile))
            {
                newfile = filename + icount + ext;
                icount++;

            }

            try
            {
                File.Move(att, newfile);
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                AddRTBText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error found while moving " + filename + ". Error details: " + ex.Message, Color.Red);
                newfile = "Warning: " + ex.Message;
            }
            return newfile;
        }

        public static string GetMessageNamespace(string messageFilePath)
        {
            using (var fileStream = GetFileAsStream(messageFilePath))
            {
                using (var reader = XmlReader.Create(fileStream))
                {
                    ReadToNextElement(reader);
                    return reader.NamespaceURI;
                }
            }
        }

        public static string GetSchemaName(string messageNamespace)
        {
            switch (messageNamespace)
            {
                case "http://www.cargowise.com/Schemas/Native":
                case "http://www.cargowise.com/Schemas/Universal":
                case "http://www.cargowise.com/Schemas/Universal/2011/11":
                    return messageNamespace + "#UniversalInterchange";

                case "http://www.edi.com.au/EnterpriseService/":
                    return messageNamespace + "#XmlInterchange";

                default:
                    return messageNamespace;
            }
        }

        static Stream GetFileAsStream(string fileName)
        {
            return new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite);
        }

        public static void ReadToNextElement(XmlReader reader)
        {
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    break;
                }
            }
        }

        public static string GetApplicationCode(string messageNamespace)
        {
            string appCode = string.Empty;
            switch (messageNamespace)
            {
                case "http://www.cargowise.com/Schemas/Universal":
                case "http://www.cargowise.com/Schemas/Universal/2011/11":
                    appCode = "UDM";
                    break;

                case "http://www.cargowise.com/Schemas/Native":
                    appCode = "NDM";
                    break;

                case "http://www.edi.com.au/EnterpriseService/":
                    appCode = "XMS";
                    break;

                default:
                    appCode = "";
                    break;
            }

            return appCode;


        }
        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            StreamReader sr = new StreamReader(strFilePath);
            string[] headers = sr.ReadLine().Split(',');
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            while (!sr.EndOfStream)
            {
                string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                if (headers.Length == rows.Length)
                {
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }
            }
            sr.Close();

            return dt;
        }

        public static void AddLabel(Label lb, string value)
        {
            if (lb.InvokeRequired)
            {
                lb.BeginInvoke(new System.Action(delegate { AddLabel(lb, value); }));
                return;
            }
            lb.Text = value;
        }



        public static void AddText(TextBox tb, string value)
        {
            if (tb.InvokeRequired)
            {
                tb.BeginInvoke(new System.Action(delegate { AddText(tb, value); }));
                return;
            }
            tb.AppendText(Environment.NewLine + value);

        }

        public static XDocument DocumentToXDocumentReader(XmlDocument doc)
        {
            return XDocument.Load(new XmlNodeReader(doc));
        }

        public bool LockedFile(FileInfo file)
        {
            try
            {
                string filePath = file.FullName;
                FileStream fs = File.OpenWrite(filePath);
                fs.Close();
                return false;
            }
            catch (Exception) { return true; }
        }

        public static void AddRTBText(RichTextBox rtb, string value)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new Action(delegate { AddRTBText(rtb, value); }));
                return;
            }

            rtb.AppendText((value) + Environment.NewLine);
            rtb.ScrollToCaret();

        }

        public static Guid GetCustomer(string custCode)
        {
            Guid result = Guid.NewGuid();
            SqlConnection sqlCTCConn = new SqlConnection
            {
                ConnectionString = Globals.CTCconnString()
            };
            SqlCommand sqlCust = new SqlCommand("Select C_ID FROM CUSTOMER WHERE C_CODE = @C_CODE", sqlCTCConn);

            sqlCust.Parameters.AddWithValue("@C_CODE", custCode);
            try
            {
                if (sqlCTCConn.State == ConnectionState.Open)
                {
                    sqlCTCConn.Close();
                }

                sqlCTCConn.Open();
                SqlDataReader dr = sqlCust.ExecuteReader(CommandBehavior.CloseConnection);
                if (dr.HasRows)
                {
                    dr.Read();
                    result = (Guid)dr["C_ID"];

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error :" + ex.Message, "Error Locating Records", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;

        }
        public static void AddRTBText(RichTextBox rtb, string value, Color color)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new Action(delegate { AddRTBText(rtb, value, color); }));
                return;
            }
            string[] str = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            rtb.DeselectAll();
            rtb.SelectionColor = color;
            rtb.AppendText(value + Environment.NewLine);
            rtb.SelectionFont = new Font(rtb.SelectionFont, FontStyle.Regular);
            rtb.SelectionColor = Color.Black;
            rtb.ScrollToCaret();

        }

        public static void AddProcessingError(ProcessingErrors procError)
        {
            SqlConnection sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.connString();
            SqlCommand sqlProcErrors = new SqlCommand("Add_ProcessingError", sqlConn);
            sqlProcErrors.CommandType = CommandType.StoredProcedure;
            if (sqlConn.State != ConnectionState.Closed)
            {
                sqlConn.Close();
            }
            sqlConn.Open();
            SqlParameter senderID = sqlProcErrors.Parameters.Add("@SENDERID", SqlDbType.VarChar, 15);
            SqlParameter recipientID = sqlProcErrors.Parameters.Add("@RECIPIENTID", SqlDbType.VarChar, 15);
            SqlParameter filename = sqlProcErrors.Parameters.Add("@FILENAME", SqlDbType.VarChar, -1);
            SqlParameter procDate = sqlProcErrors.Parameters.Add("@ProcDate", SqlDbType.DateTime);
            SqlParameter errorCode = sqlProcErrors.Parameters.Add("@ERRORCODE", SqlDbType.NChar, 10);
            SqlParameter errorDesc = sqlProcErrors.Parameters.Add("@ERRORDESC", SqlDbType.VarChar, 200);
            SqlParameter procId = sqlProcErrors.Parameters.Add("@PROCID", SqlDbType.UniqueIdentifier);
            senderID.Value = procError.SenderId;
            recipientID.Value = procError.RecipientId;
            filename.Value = Path.GetFileName(procError.FileName);
            errorCode.Value = procError.ErrorCode.Code;
            errorDesc.Value = procError.ErrorCode.Description;
            procDate.Value = DateTime.Now;
            procId.Value = procError.ProcId;
            sqlProcErrors.ExecuteNonQuery();

        }

        public static void AddTransaction(Guid custid, Guid profile, string fileName, string direction, bool original, TransReference trRefs, string archive)
        {
            SqlConnection sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.CTCconnString();
            SqlCommand sqlTransaction = new SqlCommand();
            sqlTransaction.CommandType = CommandType.StoredProcedure;
            sqlTransaction.CommandText = "Add_Transaction";
            sqlTransaction.Connection = sqlConn;
            try
            {
                sqlTransaction.Parameters.AddWithValue("@T_C", custid);
                sqlTransaction.Parameters.AddWithValue("@T_P", profile);
                sqlTransaction.Parameters.AddWithValue("@T_FILENAME", Path.GetFileName(fileName));
                sqlTransaction.Parameters.AddWithValue("@T_DATETIME", DateTime.Now);
                sqlTransaction.Parameters.AddWithValue("@T_TRIAL", "N");
                sqlTransaction.Parameters.AddWithValue("@DIRECTION", direction);
                if (original)
                {
                    sqlTransaction.Parameters.AddWithValue("@ORIGINAL", 1);
                }
                else
                {
                    sqlTransaction.Parameters.AddWithValue("@ORIGINAL", 0);
                }

                sqlTransaction.Parameters.AddWithValue("T_REF1", trRefs.Reference1);
                sqlTransaction.Parameters.AddWithValue("T_REF2", trRefs.Reference2);
                sqlTransaction.Parameters.AddWithValue("T_REF3", trRefs.Reference3);
                sqlTransaction.Parameters.AddWithValue("T_REF1TYPE", trRefs.Ref1Type.ToString());
                sqlTransaction.Parameters.AddWithValue("T_REF2TYPE", trRefs.Ref2Type.ToString());
                sqlTransaction.Parameters.AddWithValue("T_REF3TYPE", trRefs.Ref3Type.ToString());
                sqlTransaction.Parameters.AddWithValue("T_ARCHIVE", Path.GetFileName(archive));
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.ConnectionString = Globals.connString();
                sqlConn.Open();
                sqlTransaction.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                ProcessingErrors procerror = new ProcessingErrors();
                CTCErrorCode error = new CTCErrorCode();
                error.Code = NodeError.e105;
                error.Description = "SQL Connection Error:" + ex.Message;
                error.Severity = "Fatal";
                procerror.ErrorCode = error;
                procerror.FileName = fileName;
                procerror.ProcId = profile;
                AddProcessingError(procerror);
            }
            finally
            {
                sqlConn.Close();
            }


        }

        public static void AddTransactionLog(Guid p_id, String senderid, String filename, String lastresult, DateTime lastdate, String success, String cust, String recipientid)
        {
            SqlConnection sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.CTCconnString();
            Guid custid = Guid.Empty;
            try
            {
                SqlCommand addTodo = new SqlCommand("ADD_TransactionLog", sqlConn);
                addTodo.CommandType = CommandType.StoredProcedure;
                SqlParameter l_id = addTodo.Parameters.Add("@P_ID", SqlDbType.UniqueIdentifier);
                SqlParameter l_filename = addTodo.Parameters.Add("@FILENAME", SqlDbType.VarChar, 200);
                SqlParameter l_date = addTodo.Parameters.Add("@LASTDATE", SqlDbType.DateTime);
                SqlParameter l_lastResult = addTodo.Parameters.Add("@LASTRESULT", SqlDbType.VarChar, 200);
                SqlParameter l_success = addTodo.Parameters.Add("@SUCCESS", SqlDbType.Char, 1);
                SqlParameter c_id = addTodo.Parameters.Add("@C_ID", SqlDbType.UniqueIdentifier);
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.ConnectionString = Globals.connString();
                sqlConn.Open();
                l_id.Value = p_id;
                l_filename.Value = Path.GetFileName(filename);
                l_date.Value = lastdate;
                l_lastResult.Value = lastresult;
                custid = GetCustID(cust);
                if (custid != Guid.Empty)
                {
                    c_id.Value = custid;
                }
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.Open();
                addTodo.ExecuteNonQuery();
                sqlConn.Close();
            }
            catch (SqlException exSql)
            {
                CTCErrorCode error = new CTCErrorCode();
                ProcessingErrors procerror = new ProcessingErrors();
                if (custid != Guid.Empty)
                {
                    error.Code = NodeError.e106;
                }
                else
                {
                    error.Code = NodeError.e107;
                }
                error.Description = "SQL Connection Error:" + exSql.Message;
                procerror.ErrorCode = error;
                procerror.SenderId = senderid;
                procerror.RecipientId = cust;
                procerror.FileName = filename;
                procerror.ProcId = p_id;
                AddProcessingError(procerror);
                // NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Error updating Transaction Log: " + exSql.Message);

            }
            catch (Exception ex)
            {
                ProcessingErrors procerror = new ProcessingErrors();
                CTCErrorCode error = new CTCErrorCode();
                if (custid != Guid.Empty)
                {
                    error.Code = NodeError.e106;
                }
                else
                {
                    error.Code = NodeError.e111;
                }
                error.Description = ex.Message;
                procerror.ErrorCode = error;
                procerror.SenderId = senderid;
                procerror.RecipientId = cust;
                procerror.ErrorCode.Description = ex.Message;
                procerror.FileName = filename;
                procerror.ProcId = p_id;
                AddProcessingError(procerror);
                // NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Error updating Transaction Log(2): " + ex.Message);
                // String file = Path.GetFileName(filename);
                // File.Move(filename, Path.Combine(Globals.FailLoc, file));

            }
            finally
            {
                sqlConn.Close();
            }
        }
        public static Guid GetCustID(String senderID)
        {
            SqlConnection sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.CTCconnString();
            SqlCommand sqlCustID = new SqlCommand("SELECT @C_ID=C_ID from CUSTOMER where C_CODE = @C_CODE", sqlConn);
            if (sqlConn.State != ConnectionState.Closed)
            {
                sqlConn.Close();
            }
            sqlConn.Open();
            sqlCustID.Parameters.AddWithValue("@C_CODE", senderID);
            SqlParameter CUSTID = sqlCustID.Parameters.Add("@C_ID", SqlDbType.UniqueIdentifier);
            CUSTID.Direction = ParameterDirection.Output;
            sqlCustID.CommandType = CommandType.Text;
            sqlCustID.ExecuteNonQuery();
            if (CUSTID.Value != DBNull.Value)
            {
                return (Guid)CUSTID.Value;
            }
            else
            {
                return Guid.Empty;
            }
        }

        public static string GetEnum(string enumType, string mapValue)
        {
            SqlConnection sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.connString();
            string result = string.Empty;
            SqlCommand sqlEnum = new SqlCommand("GetCWEnum", sqlConn);
            sqlEnum.CommandType = CommandType.StoredProcedure;
            SqlParameter varEnum = sqlEnum.Parameters.Add("@ENUM", SqlDbType.NChar, 50);
            SqlParameter varEnumType = sqlEnum.Parameters.Add("@ENUMTYPE", SqlDbType.VarChar, 100);
            SqlParameter varMapVal = sqlEnum.Parameters.Add("@MAPVALUE", SqlDbType.VarChar, 100);
            varEnum.Direction = ParameterDirection.Output;
            varEnumType.Value = enumType;
            varMapVal.Value = mapValue;
            try
            {
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.Open();
                sqlEnum.ExecuteNonQuery();
                result = varEnum.Value.ToString().Trim();


            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Accessing Database: " + ex.Message);
            }
            return result;

        }

    }


}
