﻿using NNR_Satellite.DTO;
using NNR_Satellite.Models;
using NNR_Satellite.XUS;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NNR_Satellite
{
    public class ConfirmShipments
    {
        #region members
        string _connString { get; set; }
        string _errSTring { get; set; }
        #endregion

        #region properties
        public string ConnString
        {
            get
            {
                return _connString;
            }
        }
        public string ErrString
        {
            get
            {
                return _errSTring;
            }
            set
            {
                _errSTring = value;
            }
        }

        #endregion

        #region constructors
        public ConfirmShipments(string connstring)
        {
            _connString = connstring;
        }
        #endregion

        #region Methods
        public Imported_Consol CheckObl(Imported_Consol checkConsol)
        {
            if (string.IsNullOrEmpty(checkConsol.J_OBL))
            {
                return null;
            }

            using (IUnitOfWork uow = new UnitOfWork(new SatelliteEntity(_connString)))
            {
                return (uow.ImportedConsols.Find(c => c.J_OBL == checkConsol.J_OBL)).FirstOrDefault();


            }
        }
        public Imported_Shipments CheckShipment(Imported_Shipments chkShipment)
        {
            if (string.IsNullOrEmpty(chkShipment.H_HBL))
            {
                return null;
            }

            using (IUnitOfWork uow = new UnitOfWork(new SatelliteEntity(_connString)))
            {
                return (uow.ImportedShipments.Find(s => s.H_HBL == chkShipment.H_HBL)).FirstOrDefault();

            }
        }

        public Guid AddConsolData(Shipment consol)
        {
            Imported_Consol cwConsol = new Imported_Consol
            {
                J_CONSOLNO = GetShipmentNo(consol.DataContext.DataSourceCollection, "ForwardingConsol"),
                J_CONTAINERMODE = consol.ContainerMode.Code,
                J_MODE = consol.TransportMode.Code,
                J_OBL = consol.WayBillType.Code == "MWB" ? consol.WayBillNumber : string.Empty,
                J_VESSEL = consol.VesselName,
                J_VOYAGE = consol.VoyageFlightNo,
                J_POL = consol.PortOfLoading != null ? consol.PortOfLoading.Code : string.Empty,
                J_POD = consol.PortOfDischarge != null ? consol.PortOfDischarge.Code : string.Empty,
                J_ETA = GetDate(consol.DateCollection, DateType.Arrival),
                J_ETD = GetDate(consol.DateCollection, DateType.Departure),
                J_ID = Guid.NewGuid()
            };
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteEntity(_connString)))
            {
                if (CheckObl(cwConsol) != null)
                {
                    return cwConsol.J_ID;
                }
                else
                {
                    uow.ImportedConsols.Add(cwConsol);
                }
                uow.Complete();
            }
            var newconsol = CheckObl(cwConsol);
            return cwConsol.J_ID;
        }



        public void AddShipmentData(Shipment[] subShipmentCollection, Guid consolId)
        {
            List<Imported_Shipments> shipColl = new List<Imported_Shipments>();
            foreach (var shipment in subShipmentCollection)
            {
                Imported_Shipments cwShipment = new Imported_Shipments
                {
                    H_ID = Guid.NewGuid(),
                    H_HBL = GetBillNo(shipment, "HWB"),
                    H_SHIPMENT = GetShipmentNo(shipment.DataContext.DataSourceCollection, "ForwardingShipment"),
                    H_J = consolId
                };
                if (CheckShipment(cwShipment) == null)
                {
                    shipColl.Add(cwShipment);
                }


            }
            if (shipColl.Count > 0)
            {
                using (IUnitOfWork uow = new UnitOfWork(new SatelliteEntity(_connString)))
                {
                    uow.ImportedShipments.AddRange(shipColl);
                    uow.Complete();
                }
            }
        }
        #endregion

        #region Helpers
        private string GetShipmentNo(DataSource[] source, string ShipmentType)
        {
            return (from s in source
                    where s.Type == ShipmentType
                    select s.Key).FirstOrDefault();

        }

        private DateTime? GetDate(Date[] dateCollection, DateType dateType)
        {
            DateTime dt;
            var date = (from d in dateCollection
                        where d.Type == dateType
                        select d.Value).FirstOrDefault();
            if (DateTime.TryParse(date, out dt))
            {
                return dt;
            }

            return null;
        }

        private string GetBillNo(Shipment consol, string billType)
        {
            var wbType = consol.WayBillType.Code;
            if (wbType == billType)
            {
                return consol.WayBillNumber;
            }
            return null;
        }
        #endregion
    }
}
