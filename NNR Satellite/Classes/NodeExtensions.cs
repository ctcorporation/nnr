﻿using System.Collections.Generic;

namespace NNR_Satellite.Classes
{

    public static class NodeExtensions
    {

        public static string ToCommaList(this List<string> value)
        {
            string commaList = string.Empty;
            if (value == null)
            {
                return null;
            }
            if (value.Count > 0)
            {
                foreach (var s in value)
                {
                    commaList += s + ",";
                }
                return commaList.Substring(0, commaList.Length - 1);
            }
            return null;

        }
    }

}
