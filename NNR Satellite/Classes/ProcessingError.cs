﻿using NNR_Satellite.DTO;
using NNR_Satellite.Models;
using System.Collections.Generic;
using System.Linq;

namespace NNR_Satellite
{
    public class ProcessingError
    {
        #region Members
        string _connString;
        #endregion

        #region Properties
        public string ConnString
        {
            get { return _connString; }
        }
        #endregion

        #region Constructors
        public ProcessingError(string connString)
        {
            _connString = connString;
        }
        #endregion

        #region Methods

        public List<Processing_Errors> GetProcErrorList()
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteEntity(Globals.ConnString.ConnString)))
            {
                return uow.ProcessingErrors.GetAll().ToList();
            }
        }
        #endregion
    }
}
