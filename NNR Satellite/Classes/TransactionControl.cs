﻿using NNR_Satellite.DTO;
using NNR_Satellite.Models;
using System;

namespace NNR_Satellite.Classes
{
    class TransactionControl
    {
        #region Members
        string _connString;
        #endregion

        #region Properties
        public string ConnString
        {
            get { return _connString; }
            set { _connString = value; }
        }
        #endregion
        #region Constructors
        public TransactionControl(string connString)
        {
            ConnString = connString;
        }
        #endregion

        #region Methods
        public void AddTransaction(Transaction trans)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(new SatelliteEntity(_connString)))
                {
                    uow.Transactions.Add(trans);
                    uow.Complete();
                };
            }

            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;

            }

        }


        #endregion

        #region Helpers

        #endregion
    }
}
