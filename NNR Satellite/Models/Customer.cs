namespace NNR_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Customer")]
    public partial class Customer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Customer()
        {
            HeartBeats = new HashSet<HeartBeat>();
        }

        [Key]
        public Guid C_ID { get; set; }

        [StringLength(50)]
        public string C_NAME { get; set; }

        [Required]
        [StringLength(1)]
        public string C_IS_ACTIVE { get; set; }

        [Required]
        [StringLength(1)]
        public string C_ON_HOLD { get; set; }

        [StringLength(80)]
        public string C_PATH { get; set; }

        [Required]
        [StringLength(1)]
        public string C_FTP_CLIENT { get; set; }

        [StringLength(15)]
        public string C_CODE { get; set; }

        [Required]
        [StringLength(1)]
        public string C_TRIAL { get; set; }

        public DateTime? C_TRIALSTART { get; set; }

        public DateTime? C_TRIALEND { get; set; }

        [StringLength(10)]
        public string C_SHORTNAME { get; set; }

        [Required]
        [StringLength(1)]
        public string C_SATELLITE { get; set; }

        [Required]
        [StringLength(1)]
        public string C_INVOICED { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HeartBeat> HeartBeats { get; set; }
    }
}
