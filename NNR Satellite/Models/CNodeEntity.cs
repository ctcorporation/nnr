namespace NNR_Satellite.Models
{
    using System.Data.Entity;

    public partial class CNodeEntity : DbContext
    {
        public CNodeEntity()
            : base("name=CNodeEntity")
        {
        }
        public CNodeEntity(string connstring)
            : base(connstring)
        {

        }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Models.HeartBeat> HeartBeats { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .Property(e => e.C_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_IS_ACTIVE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_ON_HOLD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_FTP_CLIENT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_TRIAL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_SHORTNAME)
                .IsFixedLength();

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_SATELLITE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_INVOICED)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.HeartBeats)
                .WithOptional(e => e.Customer)
                .HasForeignKey(e => e.HB_C);

            modelBuilder.Entity<HeartBeat>()
                .Property(e => e.HB_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<HeartBeat>()
                .Property(e => e.HB_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<HeartBeat>()
                .Property(e => e.HB_ISMONITORED)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<HeartBeat>()
                .Property(e => e.HB_LASTOPERATION)
                .IsUnicode(false);
        }
    }
}
