namespace NNR_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Imported_Shipments
    {
        [Key]
        public Guid H_ID { get; set; }

        [StringLength(50)]
        public string H_HBL { get; set; }

        public Guid? H_J { get; set; }

        [StringLength(15)]
        public string H_SHIPMENT { get; set; }
    }
}
