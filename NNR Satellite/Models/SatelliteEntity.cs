namespace NNR_Satellite.Models
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;

    public partial class SatelliteEntity : DbContext
    {
        public SatelliteEntity(string connString)
            : base(connString)
        {

        }

        public SatelliteEntity()
            : base("name=SatelliteEntity")
        {
        }

        public virtual DbSet<Cargowise_Enums> Cargowise_Enums { get; set; }
        public virtual DbSet<CargowiseContext> CargowiseContexts { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<DT> DTS { get; set; }
        public virtual DbSet<Processing_Errors> Procesing_Errors { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<TRANSACTION_LOG> TRANSACTION_LOG { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<Imported_Consol> Imported_Consol { get; set; }
        public virtual DbSet<Imported_Shipments> Imported_Shipments { get; set; }
        public virtual DbSet<TODO> TODO { get; set; }
        public virtual DbSet<vw_CustomerProfile> vw_CustomerProfile { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
     //       modelBuilder.Entity<Cargowise_Enums>()
     //           .Property(e => e.CW_ENUMTYPE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Cargowise_Enums>()
     //           .Property(e => e.CW_ENUM)
     //           .IsFixedLength();

     //       modelBuilder.Entity<Cargowise_Enums>()
     //           .Property(e => e.CW_MAPVALUE)
     //           .IsFixedLength();

     //       modelBuilder.Entity<CargowiseContext>()
     //           .Property(e => e.CC_Context)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<CargowiseContext>()
     //           .Property(e => e.CC_Description)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Customer>()
     //           .Property(e => e.C_NAME)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Customer>()
     //           .Property(e => e.C_IS_ACTIVE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Customer>()
     //           .Property(e => e.C_ON_HOLD)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Customer>()
     //           .Property(e => e.C_PATH)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Customer>()
     //           .Property(e => e.C_FTP_CLIENT)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Customer>()
     //           .Property(e => e.C_CODE)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Customer>()
     //           .Property(e => e.C_TRIAL)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Customer>()
     //           .Property(e => e.C_SHORTNAME)
     //           .IsFixedLength();

     //       modelBuilder.Entity<DT>()
     //           .Property(e => e.D_FINALPROCESSING)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<DT>()
     //           .Property(e => e.D_FILETYPE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<DT>()
     //           .Property(e => e.D_DTSTYPE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<DT>()
     //           .Property(e => e.D_DTS)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<DT>()
     //           .Property(e => e.D_SEARCHPATTERN)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<DT>()
     //           .Property(e => e.D_NEWVALUE)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<DT>()
     //           .Property(e => e.D_QUALIFIER)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<DT>()
     //           .Property(e => e.D_TARGET)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<DT>()
     //           .Property(e => e.D_CURRENTVALUE)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Processing_Errors>()
     //           .Property(e => e.E_SENDERID)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Processing_Errors>()
     //           .Property(e => e.E_RECIPIENTID)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Processing_Errors>()
     //           .Property(e => e.E_FILENAME)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Processing_Errors>()
     //           .Property(e => e.E_ERRORDESC)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Processing_Errors>()
     //           .Property(e => e.E_ERRORCODE)
     //           .IsFixedLength();

     //       modelBuilder.Entity<Processing_Errors>()
     //           .Property(e => e.E_IGNORE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_REASONCODE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_SERVER)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_USERNAME)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_PASSWORD)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_DELIVERY)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_PORT)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_DESCRIPTION)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_PATH)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_DIRECTION)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_LIBNAME)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_MESSAGETYPE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_RECIPIENTID)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_MSGTYPE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_CHARGEABLE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_BILLTO)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_SENDERID)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_DTS)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_EMAILADDRESS)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_ACTIVE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_SSL)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_SENDEREMAIL)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_SUBJECT)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_FILETYPE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_MESSAGEDESCR)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_EVENTCODE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_CUSTOMERCOMPANYNAME)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_GROUPCHARGES)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_XSD)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_PARAMLIST)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Profile>()
     //           .Property(e => e.P_METHOD)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<TRANSACTION_LOG>()
     //           .Property(e => e.X_FILENAME)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<TRANSACTION_LOG>()
     //           .Property(e => e.X_SUCCESS)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<TRANSACTION_LOG>()
     //           .Property(e => e.X_LASTRESULT)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Transaction>()
     //           .Property(e => e.T_ID)
     //           .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);


     //       modelBuilder.Entity<Transaction>()
     //           .Property(e => e.T_FILENAME)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Transaction>()
     //           .Property(e => e.T_MSGTYPE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Transaction>()
     //.Property(e => e.T_DIRECTION)
     //.IsFixedLength()
     //.IsUnicode(false);

     //       modelBuilder.Entity<Transaction>()
     //           .Property(e => e.T_REF1TYPE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Transaction>()
     //           .Property(e => e.T_REF2TYPE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Transaction>()
     //           .Property(e => e.T_REF3TYPE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Imported_Consol>()
     //           .Property(e => e.J_OBL)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Imported_Consol>()
     //           .Property(e => e.J_POL)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Imported_Consol>()
     //           .Property(e => e.J_POD)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Imported_Consol>()
     //           .Property(e => e.J_VESSEL)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Imported_Consol>()
     //           .Property(e => e.J_VOYAGE)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Imported_Consol>()
     //           .Property(e => e.J_MODE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Imported_Consol>()
     //           .Property(e => e.J_CONTAINERNO)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Imported_Consol>()
     //           .Property(e => e.J_SEALNO)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Imported_Consol>()
     //           .Property(e => e.J_CONTAINERMODE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Imported_Consol>()
     //           .Property(e => e.J_CONSOLNO)
     //           .IsFixedLength();

     //       modelBuilder.Entity<Imported_Shipments>()
     //           .Property(e => e.H_HBL)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<Imported_Shipments>()
     //           .Property(e => e.H_SHIPMENT)
     //           .IsFixedLength();

     //       modelBuilder.Entity<TODO>()
     //           .Property(e => e.L_FILENAME)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<TODO>()
     //           .Property(e => e.L_LASTRESULT)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.C_NAME)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.C_CODE)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.C_IS_ACTIVE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.C_ON_HOLD)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.C_PATH)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.C_FTP_CLIENT)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_REASONCODE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_SERVER)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_USERNAME)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_PASSWORD)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_DESCRIPTION)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_PORT)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_DELIVERY)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_PATH)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_DIRECTION)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_XSD)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_LIBNAME)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_RECIPIENTID)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_SENDERID)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_DTS)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_BILLTO)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_CHARGEABLE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_MSGTYPE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_MESSAGETYPE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_ACTIVE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_FILETYPE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_EMAILADDRESS)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_SSL)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_SENDEREMAIL)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_SUBJECT)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_MESSAGEDESCR)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_EVENTCODE)
     //           .IsFixedLength()
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_CUSTOMERCOMPANYNAME)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.C_SHORTNAME)
     //           .IsFixedLength();

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_METHOD)
     //           .IsUnicode(false);

     //       modelBuilder.Entity<vw_CustomerProfile>()
     //           .Property(e => e.P_PARAMLIST)
     //           .IsUnicode(false);
        }
    }
}
