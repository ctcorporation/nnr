namespace NNR_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Imported_Consol
    {
        [Key]
        public Guid J_ID { get; set; }

        [StringLength(50)]
        public string J_OBL { get; set; }

        public DateTime? J_ETD { get; set; }

        public DateTime? J_ETA { get; set; }

        [StringLength(5)]
        public string J_POL { get; set; }

        [StringLength(5)]
        public string J_POD { get; set; }

        [StringLength(100)]
        public string J_VESSEL { get; set; }

        [StringLength(50)]
        public string J_VOYAGE { get; set; }

        public DateTime? J_ONBOARD { get; set; }

        [StringLength(3)]
        public string J_MODE { get; set; }

        [StringLength(20)]
        public string J_CONTAINERNO { get; set; }

        [StringLength(50)]
        public string J_SEALNO { get; set; }

        [StringLength(3)]
        public string J_CONTAINERMODE { get; set; }

        [StringLength(15)]
        public string J_CONSOLNO { get; set; }
    }
}
