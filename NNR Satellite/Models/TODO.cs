namespace NNR_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TODO")]
    public partial class TODO
    {
        [Key]
        public Guid L_ID { get; set; }

        public Guid? L_P { get; set; }

        public string L_FILENAME { get; set; }

        public string L_LASTRESULT { get; set; }

        public DateTime? L_DATE { get; set; }
    }
}
