namespace NNR_Satellite.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Transaction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid T_ID { get; set; }

        public Guid? T_C { get; set; }

        public Guid? T_P { get; set; }

        public DateTime? T_DATETIME { get; set; }

        [StringLength(100)]
        public string T_FILENAME { get; set; }

        [StringLength(10)]
        public string T_MSGTYPE { get; set; }

        [StringLength(1)]
        public string T_DIRECTION { get; set; }

        [StringLength(50)]
        public string T_REF1 { get; set; }

        [StringLength(50)]
        public string T_REF2 { get; set; }

        [StringLength(50)]
        public string T_REF3 { get; set; }

        [StringLength(100)]
        public string T_ARCHIVE { get; set; }

        [StringLength(10)]
        public string T_REF1TYPE { get; set; }

        [StringLength(10)]
        public string T_REF2TYPE { get; set; }

        [StringLength(10)]
        public string T_REF3TYPE { get; set; }

        public bool T_CWFILECREATED { get; set; }

        [StringLength(2)]
        public string T_COUNTRY { get; set; }

        [StringLength(15)]
        public string T_CWCODE { get; set; }

    }
}
