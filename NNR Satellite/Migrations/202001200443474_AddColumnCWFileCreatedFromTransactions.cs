﻿namespace NNR_Satellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnCWFileCreatedFromTransactions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "T_CWFILECREATED", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transactions", "T_CWFILECREATED");
        }
    }
}
