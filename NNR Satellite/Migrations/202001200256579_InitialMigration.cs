﻿namespace NNR_Satellite.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.Cargowise_Enums",
            //    c => new
            //        {
            //            CW_ID = c.Guid(nullable: false),
            //            CW_ENUMTYPE = c.String(maxLength: 50, fixedLength: true, unicode: false),
            //            CW_ENUM = c.String(maxLength: 100, fixedLength: true),
            //            CW_MAPVALUE = c.String(maxLength: 100, fixedLength: true),
            //        })
            //    .PrimaryKey(t => t.CW_ID);

            //CreateTable(
            //    "dbo.CargowiseContext",
            //    c => new
            //        {
            //            CC_ID = c.Guid(nullable: false),
            //            CC_Context = c.String(maxLength: 50, unicode: false),
            //            CC_Description = c.String(unicode: false),
            //        })
            //    .PrimaryKey(t => t.CC_ID);

            //CreateTable(
            //    "dbo.Customer",
            //    c => new
            //        {
            //            C_ID = c.Guid(nullable: false),
            //            C_NAME = c.String(maxLength: 50, unicode: false),
            //            C_IS_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_ON_HOLD = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_PATH = c.String(maxLength: 80, unicode: false),
            //            C_FTP_CLIENT = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_CODE = c.String(maxLength: 15, unicode: false),
            //            C_TRIAL = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_TRIALSTART = c.DateTime(),
            //            C_TRIALEND = c.DateTime(),
            //            C_SHORTNAME = c.String(maxLength: 10, fixedLength: true),
            //            C_SATELLITE = c.String(nullable: false, maxLength: 1),
            //            C_INVOICED = c.String(nullable: false, maxLength: 1),
            //        })
            //    .PrimaryKey(t => t.C_ID);

            //CreateTable(
            //    "dbo.HEARTBEAT",
            //    c => new
            //        {
            //            HB_ID = c.Guid(nullable: false),
            //            HB_C = c.Guid(),
            //            HB_NAME = c.String(maxLength: 50),
            //            HB_PATH = c.String(maxLength: 200),
            //            HB_ISMONITORED = c.String(nullable: false, maxLength: 1),
            //            HB_PID = c.Int(),
            //            HB_LASTOPERATION = c.String(maxLength: 50),
            //            HB_LASTCHECKIN = c.DateTime(),
            //            HB_OPENED = c.DateTime(),
            //            HB_LASTOPERATIONPARAMS = c.String(maxLength: 100),
            //            Customer_C_ID = c.Guid(),
            //        })
            //    .PrimaryKey(t => t.HB_ID)
            //    .ForeignKey("dbo.Customer", t => t.Customer_C_ID)
            //    .Index(t => t.Customer_C_ID);

            //CreateTable(
            //    "dbo.DTS",
            //    c => new
            //        {
            //            D_ID = c.Guid(nullable: false),
            //            D_C = c.Guid(),
            //            D_INDEX = c.Int(),
            //            D_FINALPROCESSING = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            D_P = c.Guid(),
            //            D_FILETYPE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            D_DTSTYPE = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            D_DTS = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            D_SEARCHPATTERN = c.String(unicode: false),
            //            D_NEWVALUE = c.String(unicode: false),
            //            D_QUALIFIER = c.String(unicode: false),
            //            D_TARGET = c.String(unicode: false),
            //            D_CURRENTVALUE = c.String(unicode: false),
            //        })
            //    .PrimaryKey(t => t.D_ID);

            //CreateTable(
            //    "dbo.Imported_Consol",
            //    c => new
            //        {
            //            J_ID = c.Guid(nullable: false),
            //            J_OBL = c.String(maxLength: 50, unicode: false),
            //            J_ETD = c.DateTime(),
            //            J_ETA = c.DateTime(),
            //            J_POL = c.String(maxLength: 5, fixedLength: true, unicode: false),
            //            J_POD = c.String(maxLength: 5, fixedLength: true, unicode: false),
            //            J_VESSEL = c.String(maxLength: 100, unicode: false),
            //            J_VOYAGE = c.String(maxLength: 50, unicode: false),
            //            J_ONBOARD = c.DateTime(),
            //            J_MODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            J_CONTAINERNO = c.String(maxLength: 20, unicode: false),
            //            J_SEALNO = c.String(maxLength: 50, unicode: false),
            //            J_CONTAINERMODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            J_CONSOLNO = c.String(maxLength: 15, fixedLength: true),
            //        })
            //    .PrimaryKey(t => t.J_ID);

            //CreateTable(
            //    "dbo.Imported_Shipments",
            //    c => new
            //        {
            //            H_ID = c.Guid(nullable: false),
            //            H_HBL = c.String(maxLength: 50, unicode: false),
            //            H_J = c.Guid(),
            //            H_SHIPMENT = c.String(maxLength: 15, fixedLength: true),
            //        })
            //    .PrimaryKey(t => t.H_ID);

            //CreateTable(
            //    "dbo.PROCESSING_ERRORS",
            //    c => new
            //        {
            //            E_PK = c.Guid(nullable: false),
            //            E_SENDERID = c.String(maxLength: 15, unicode: false),
            //            E_RECIPIENTID = c.String(maxLength: 15, unicode: false),
            //            E_PROCDATE = c.DateTime(),
            //            E_FILENAME = c.String(unicode: false),
            //            E_ERRORDESC = c.String(unicode: false),
            //            E_ERRORCODE = c.String(maxLength: 10, fixedLength: true),
            //            E_P = c.Guid(),
            //            E_IGNORE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //        })
            //    .PrimaryKey(t => t.E_PK);

            //CreateTable(
            //    "dbo.PROFILE",
            //    c => new
            //        {
            //            P_ID = c.Guid(nullable: false),
            //            P_C = c.Guid(),
            //            P_REASONCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_SERVER = c.String(unicode: false),
            //            P_USERNAME = c.String(unicode: false),
            //            P_PASSWORD = c.String(unicode: false),
            //            P_DELIVERY = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_PORT = c.String(maxLength: 10, fixedLength: true, unicode: false),
            //            P_DESCRIPTION = c.String(unicode: false),
            //            P_PATH = c.String(maxLength: 100, unicode: false),
            //            P_DIRECTION = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_LIBNAME = c.String(maxLength: 50, unicode: false),
            //            P_MESSAGETYPE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_RECIPIENTID = c.String(maxLength: 15, unicode: false),
            //            P_MSGTYPE = c.String(maxLength: 20, fixedLength: true, unicode: false),
            //            P_CHARGEABLE = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_BILLTO = c.String(maxLength: 15, fixedLength: true, unicode: false),
            //            P_SENDERID = c.String(maxLength: 15, unicode: false),
            //            P_DTS = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_EMAILADDRESS = c.String(maxLength: 100, unicode: false),
            //            P_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_SSL = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_SENDEREMAIL = c.String(maxLength: 100, unicode: false),
            //            P_SUBJECT = c.String(maxLength: 100, unicode: false),
            //            P_FILETYPE = c.String(maxLength: 5, fixedLength: true, unicode: false),
            //            P_MESSAGEDESCR = c.String(maxLength: 20, unicode: false),
            //            P_EVENTCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_CUSTOMERCOMPANYNAME = c.String(maxLength: 50, unicode: false),
            //            P_GROUPCHARGES = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_XSD = c.String(maxLength: 50, unicode: false),
            //            P_PARAMLIST = c.String(unicode: false),
            //            P_METHOD = c.String(maxLength: 50, unicode: false),
            //            P_PCTCNODE = c.Guid(),
            //        })
            //    .PrimaryKey(t => t.P_ID);

            //CreateTable(
            //    "dbo.TODO",
            //    c => new
            //        {
            //            L_ID = c.Guid(nullable: false),
            //            L_P = c.Guid(),
            //            L_FILENAME = c.String(unicode: false),
            //            L_LASTRESULT = c.String(unicode: false),
            //            L_DATE = c.DateTime(),
            //        })
            //    .PrimaryKey(t => t.L_ID);

            //CreateTable(
            //    "dbo.TRANSACTION_LOG",
            //    c => new
            //        {
            //            X_ID = c.Guid(nullable: false),
            //            X_P = c.Guid(),
            //            X_FILENAME = c.String(maxLength: 150, unicode: false),
            //            X_DATE = c.DateTime(),
            //            X_SUCCESS = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            X_C = c.Guid(),
            //            X_LASTRESULT = c.String(maxLength: 120, unicode: false),
            //        })
            //    .PrimaryKey(t => t.X_ID);

            //CreateTable(
            //    "dbo.Transactions",
            //    c => new
            //        {
            //            T_ID = c.Guid(nullable: false),
            //            T_C = c.Guid(),
            //            T_P = c.Guid(),
            //            T_DATETIME = c.DateTime(),
            //            T_FILENAME = c.String(maxLength: 100, unicode: false),
            //            T_TRIAL = c.String(nullable: false, maxLength: 1),
            //            T_INVOICED = c.String(nullable: false, maxLength: 1),
            //            T_INVOICEDATE = c.DateTime(),
            //            T_MSGTYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
            //            T_BILLTO = c.String(maxLength: 15, fixedLength: true, unicode: false),
            //            T_DIRECTION = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            T_CHARGEABLE = c.String(maxLength: 1),
            //            T_REF1 = c.String(maxLength: 50),
            //            T_REF2 = c.String(maxLength: 50),
            //            T_REF3 = c.String(maxLength: 50),
            //            T_ARCHIVE = c.String(maxLength: 100),
            //            T_REF1TYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
            //            T_REF2TYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
            //            T_REF3TYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
            //        })
            //    .PrimaryKey(t => t.T_ID);

            //CreateTable(
            //    "dbo.vw_CustomerProfile",
            //    c => new
            //        {
            //            C_IS_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_ON_HOLD = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_FTP_CLIENT = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_ID = c.Guid(nullable: false),
            //            C_ID = c.Guid(nullable: false),
            //            P_DIRECTION = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_DTS = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_NAME = c.String(maxLength: 50, unicode: false),
            //            C_CODE = c.String(maxLength: 15, unicode: false),
            //            C_PATH = c.String(maxLength: 50, unicode: false),
            //            P_C = c.Guid(),
            //            P_REASONCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_SERVER = c.String(unicode: false),
            //            P_USERNAME = c.String(unicode: false),
            //            P_PASSWORD = c.String(unicode: false),
            //            P_DESCRIPTION = c.String(unicode: false),
            //            P_PORT = c.String(maxLength: 10, fixedLength: true, unicode: false),
            //            P_DELIVERY = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_PATH = c.String(maxLength: 100, unicode: false),
            //            P_XSD = c.String(maxLength: 50, unicode: false),
            //            P_LIBNAME = c.String(maxLength: 50, unicode: false),
            //            P_RECIPIENTID = c.String(maxLength: 15, unicode: false),
            //            P_SENDERID = c.String(maxLength: 15, unicode: false),
            //            P_BILLTO = c.String(maxLength: 15, fixedLength: true, unicode: false),
            //            P_CHARGEABLE = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_MSGTYPE = c.String(maxLength: 20, fixedLength: true, unicode: false),
            //            P_MESSAGETYPE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_FILETYPE = c.String(maxLength: 5, fixedLength: true, unicode: false),
            //            P_EMAILADDRESS = c.String(maxLength: 100, unicode: false),
            //            P_SSL = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_SENDEREMAIL = c.String(maxLength: 100, unicode: false),
            //            P_SUBJECT = c.String(maxLength: 100, unicode: false),
            //            P_MESSAGEDESCR = c.String(maxLength: 20, unicode: false),
            //            P_EVENTCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_CUSTOMERCOMPANYNAME = c.String(maxLength: 50, unicode: false),
            //            C_SHORTNAME = c.String(maxLength: 10, fixedLength: true),
            //            P_METHOD = c.String(maxLength: 50, unicode: false),
            //            P_PARAMLIST = c.String(unicode: false),
            //        })
            //    .PrimaryKey(t => new { t.C_IS_ACTIVE, t.C_ON_HOLD, t.C_FTP_CLIENT, t.P_ID, t.C_ID, t.P_DIRECTION, t.P_DTS, t.P_ACTIVE });

        }

        public override void Down()
        {
            DropForeignKey("dbo.HEARTBEAT", "Customer_C_ID", "dbo.Customer");
            DropIndex("dbo.HEARTBEAT", new[] { "Customer_C_ID" });
            DropTable("dbo.vw_CustomerProfile");
            DropTable("dbo.Transactions");
            DropTable("dbo.TRANSACTION_LOG");
            DropTable("dbo.TODO");
            DropTable("dbo.PROFILE");
            DropTable("dbo.PROCESSING_ERRORS");
            DropTable("dbo.Imported_Shipments");
            DropTable("dbo.Imported_Consol");
            DropTable("dbo.DTS");
            DropTable("dbo.HEARTBEAT");
            DropTable("dbo.Customer");
            DropTable("dbo.CargowiseContext");
            DropTable("dbo.Cargowise_Enums");
        }
    }
}
