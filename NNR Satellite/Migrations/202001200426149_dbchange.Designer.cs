﻿// <auto-generated />
namespace NNR_Satellite.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.3.0")]
    public sealed partial class dbchange : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(dbchange));
        
        string IMigrationMetadata.Id
        {
            get { return "202001200426149_dbchange"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
