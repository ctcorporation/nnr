﻿// <auto-generated />
namespace NNR_Satellite.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.3.0")]
    public sealed partial class RemoveAccountingFieldsFromTransaction : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(RemoveAccountingFieldsFromTransaction));
        
        string IMigrationMetadata.Id
        {
            get { return "202001200311278_RemoveAccountingFieldsFromTransaction"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
