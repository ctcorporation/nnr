﻿namespace NNR_Satellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateCWFiledName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "T_FILECREATED", c => c.Boolean(nullable: false));
            DropColumn("dbo.Transactions", "T_CARGOWISEFILECREATED");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transactions", "T_CARGOWISEFILECREATED", c => c.Boolean(nullable: false));
            DropColumn("dbo.Transactions", "T_FILECREATED");
        }
    }
}
