﻿namespace NNR_Satellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCargowiseFlagInTransactions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "T_CARGOWISEFILECREATED", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transactions", "T_CARGOWISEFILECREATED");
        }
    }
}
