﻿namespace NNR_Satellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeChargeableFromTransaction : DbMigration
    {
        public override void Up()
        {
            //DropPrimaryKey("dbo.Transactions");
            //AlterColumn("dbo.Transactions", "T_ID", c => c.Guid(nullable: false, identity: true));
            //AddPrimaryKey("dbo.Transactions", "T_ID");
            DropColumn("dbo.Transactions", "T_CHARGEABLE");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transactions", "T_CHARGEABLE", c => c.String(maxLength: 1));
            DropPrimaryKey("dbo.Transactions");
            AlterColumn("dbo.Transactions", "T_ID", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Transactions", "T_ID");
        }
    }
}
