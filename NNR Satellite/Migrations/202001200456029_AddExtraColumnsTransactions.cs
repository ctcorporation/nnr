﻿namespace NNR_Satellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddExtraColumnsTransactions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "T_COUNTRY", c => c.String(maxLength: 2));
            AddColumn("dbo.Transactions", "T_CWCODE", c => c.String(maxLength: 15));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transactions", "T_CWCODE");
            DropColumn("dbo.Transactions", "T_COUNTRY");
        }
    }
}
