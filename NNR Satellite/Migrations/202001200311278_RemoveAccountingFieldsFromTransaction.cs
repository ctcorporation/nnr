﻿namespace NNR_Satellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveAccountingFieldsFromTransaction : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Transactions", "T_TRIAL");
            DropColumn("dbo.Transactions", "T_INVOICED");
            DropColumn("dbo.Transactions", "T_INVOICEDATE");
            DropColumn("dbo.Transactions", "T_BILLTO");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transactions", "T_BILLTO", c => c.String(maxLength: 15, fixedLength: true, unicode: false));
            AddColumn("dbo.Transactions", "T_INVOICEDATE", c => c.DateTime());
            AddColumn("dbo.Transactions", "T_INVOICED", c => c.String(nullable: false, maxLength: 1));
            AddColumn("dbo.Transactions", "T_TRIAL", c => c.String(nullable: false, maxLength: 1));
        }
    }
}
