﻿namespace NNR_Satellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveColumnCWFileCreatedFromTransactions : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Transactions", "T_FILECREATED");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transactions", "T_FILECREATED", c => c.Boolean(nullable: false));
        }
    }
}
