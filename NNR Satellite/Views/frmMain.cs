﻿using NNR_Satellite.Classes;
using NNR_Satellite.DTO;
using NNR_Satellite.Models;
using NNR_Satellite.XUS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using static NNR_Satellite.SatelliteErrors;

namespace NNR_Satellite
{

    public partial class frmMain : Form
    {

        //public SqlConnection sqlConn, sqlCTCConn;
        public int totfilesRx;
        public int totfilesTx;
        public int cwRx;
        public int cwTx;
        public int filesRx;
        public int filesTx;
        private string flightPrefix = string.Empty;
        private string pceType = string.Empty;
        private string _errorMessage = string.Empty;
        public System.Timers.Timer tmrMain;
        public frmMain()
        {
            InitializeComponent();
            tmrMain = new System.Timers.Timer();
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);

        }

        private void OnProcessExit(object sender, EventArgs e)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, "Stopping", m.GetParameters(), Path.GetDirectoryName(Application.ExecutablePath));
        }

        public static DateTime? TryParse(string text) =>
    DateTime.TryParse(text, out var date) ? date : (DateTime?)null;

        private void tmrMain_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (btnTimer.Text == "&Stop")
            {
                tmrMain.Stop();
                GetFiles("Message_MLASYDSYD *.xml");
                GetFiles("*.*");
                tmrMain.Start();
            }
            else
            {
                tmrMain.Stop();
            }


        }

        private void btnTimer_Click(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "&Start")
            {
                ((Button)sender).Text = "&Stop";
                ((Button)sender).BackColor = Color.LightCoral;
                tslMain.Text = "Timed Processes Started";
                GetFiles("Message_MLASYDSYD*.xml");
                GetFiles("*.*");
                tmrMain.Start();
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Waiting for Files.");
            }
            else
            {
                ((Button)sender).Text = "&Start";
                ((Button)sender).BackColor = Color.LightGreen;
                tslMain.Text = "Timed Processes Stopped";
                tmrMain.Stop();
            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void clearLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbLog.Text = "";
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout AboutBox = new frmAbout();
            AboutBox.ShowDialog();
            LoadSettings();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

            LoadSettings();
            tslMode.Text = "Production";
            productionToolStripMenuItem.Checked = true;
            HeartBeat.RegisterHeartBeat(Globals.glCustCode, "Starting", null, Path.GetDirectoryName(Application.ExecutablePath));
            this.tslMain.Width = this.Width / 2;
            this.tslSpacer.Width = (this.Width / 2) - (productionToolStripMenuItem.Width + tslCmbMode.Width);


        }

        private void LoadSettings()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appDataPath, System.Diagnostics.Process.GetCurrentProcess().ProcessName);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            tmrMain.Interval = (1000 * 5) * 60;
            tmrMain.Elapsed += new ElapsedEventHandler(tmrMain_Elapsed);
            Globals.glAppConfig = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".XML";
            Globals.glAppConfig = Path.Combine(path, Globals.glAppConfig);
            frmSettings Settings = new frmSettings();
            if (!File.Exists(Globals.glAppConfig))
            {
                XDocument xmlConfig = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "Yes"),
                    new XElement("Satellite",
                    new XElement("Customer"),
                    new XElement("Database"),
                    new XElement("CTCNode"),
                    new XElement("Communications")));
                xmlConfig.Save(Globals.glAppConfig);
                Settings.ShowDialog();
            }
            else
            {
                try
                {
                    bool loadConfig = false;
                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load(Globals.glAppConfig);
                    XmlNodeList nodeList = xmlConfig.SelectNodes("/Satellite/Database");
                    XmlNode nodeCat = nodeList[0].SelectSingleNode("Catalog");
                    if (nodeCat != null)
                    {
                        Globals.glDbInstance = nodeCat.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlServer = nodeList[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.glDbServer = xmlServer.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlUser = nodeList[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.glDbUserName = xmlUser.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlPassword = nodeList[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.glDbPassword = xmlPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }
                    Globals.ConnString = new ConnectionManager(Globals.glDbServer, Globals.glDbInstance, Globals.glDbUserName, Globals.glDbPassword);
                    //sqlConn = new SqlConnection();
                    //sqlConn.ConnectionString = Globals.connString();
                    nodeList = null;
                    nodeList = xmlConfig.SelectNodes("/Satellite/Customer");
                    XmlNode xCustCode = nodeList[0].SelectSingleNode("Code");
                    if (xCustCode != null)
                    {
                        Globals.glCustCode = xCustCode.InnerText;
                        Globals.Customer cust = new Globals.Customer();
                        cust = Globals.GetCustomer(Globals.glCustCode);
                        if (cust != null)
                        {
                            Globals.glCustCode = cust.Code;
                            Globals.gl_CustId = cust.Id;
                        }
                        else
                        {
                            loadConfig = true;
                        }
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xPath = nodeList[0].SelectSingleNode("ProfilePath");
                    if (xPath != null)
                    {
                        Globals.glProfilePath = xPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xOutPath = nodeList[0].SelectSingleNode("OutputPath");
                    if (xOutPath != null)
                    {
                        Globals.glOutputDir = xOutPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xInPath = nodeList[0].SelectSingleNode("PickupPath");
                    if (xInPath != null)
                    {
                        Globals.glPickupPath = xInPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xLib = nodeList[0].SelectSingleNode("LibraryPath");
                    if (xLib != null)
                    {
                        Globals.glLibPath = xLib.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xFail = nodeList[0].SelectSingleNode("FailPath");
                    if (xFail != null)
                    {
                        Globals.glFailPath = xFail.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xArchive = nodeList[0].SelectSingleNode("ArchiveLocation");
                    if (xArchive != null)
                    {
                        Globals.glArcLocation = xArchive.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xTesting = nodeList[0].SelectSingleNode("TestPath");
                    if (xTesting != null)
                    {
                        Globals.glTestLocation = xTesting.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    nodeList = xmlConfig.SelectNodes("/Satellite/Communications");
                    XmlNode xmlAlertsTo = nodeList[0].SelectSingleNode("AlertsTo");
                    if (xmlAlertsTo != null)
                    {
                        Globals.glAlertsTo = xmlAlertsTo.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xSmtp = nodeList[0].SelectSingleNode("SMTPServer");
                    if (xSmtp != null)
                    {
                        SMTPServer.Server = xSmtp.InnerText;
                        SMTPServer.Port = xSmtp.Attributes[0].InnerXml;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xEmail = nodeList[0].SelectSingleNode("EmailAddress");
                    if (xEmail != null)
                    { SMTPServer.Email = xEmail.InnerText; }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xUserName = nodeList[0].SelectSingleNode("SMTPUsername");
                    if (xUserName != null)
                    {
                        SMTPServer.Username = xUserName.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xPassword = nodeList[0].SelectSingleNode("SMTPPassword");
                    if (xPassword != null)
                    {
                        SMTPServer.Password = xPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    if (loadConfig)
                    {
                        throw new System.Exception("Mandatory Settings missing");
                    }
                    //CTC Node Database Connection
                    nodeList = null;
                    nodeList = xmlConfig.SelectNodes("/Satellite/CTCNode");
                    nodeCat = null;
                    nodeCat = nodeList[0].SelectSingleNode("Catalog");
                    if (nodeCat != null)
                    {
                        Globals.glCTCDbInstance = nodeCat.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlServer = null;
                    xmlServer = nodeList[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.glCTCDbServer = xmlServer.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlUser = null;
                    xmlUser = nodeList[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.glCTCDbUserName = xmlUser.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlPassword = null;
                    xmlPassword = nodeList[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.glCTCDbPassword = xmlPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    //sqlCTCConn = new SqlConnection();
                    //sqlCTCConn.ConnectionString = Globals.CTCconnString();
                }

                catch (Exception)
                {
                    MessageBox.Show("There was an error loading the Config files. Please check the settings", "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    frmSettings FormSettings = new frmSettings();
                    FormSettings.ShowDialog();
                    DialogResult dr = FormSettings.DialogResult;
                    if (dr == DialogResult.OK)
                    {
                        LoadSettings();
                    }
                }
                var Cust = Globals.GetCustomer(Globals.glCustCode);
                this.Text = "CTC Satellite - " + Cust.CustomerName;
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "System Ready");
            }
        }

        private void profilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProfiles Profiles = new frmProfiles();
            Profiles.ShowDialog();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSettings FormSettings = new frmSettings();
            FormSettings.ShowDialog();
            DialogResult dr = FormSettings.DialogResult;
            if (dr == DialogResult.OK)
            {
                LoadSettings();
            }
        }

        private void GetFiles(string pattern)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters(), Path.GetDirectoryName(Application.ExecutablePath));
            string filesPath = string.Empty;
            if (tslMode.Text == "Production")
            {
                filesPath = Globals.glPickupPath;
            }
            else
            {
                filesPath = Globals.glTestLocation;
            }
            DirectoryInfo diTodo = new DirectoryInfo(filesPath);
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var list = diTodo.GetFiles();

            if (list.Length > 0)
            {
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Now Processing " + list.Length + " Files");
                foreach (var fileName in diTodo.GetFiles(pattern))
                {

                    ProcessResult thisResult = new ProcessResult();
                    try
                    {
                        if (fileName.Extension.ToLower() == ".xml")
                        {
                            string messageNamespace = NodeResources.GetMessageNamespace(fileName.FullName);
                            if (!string.IsNullOrEmpty(messageNamespace))
                            {
                                ProcessCargowiseFiles(fileName.FullName);
                            }
                            else
                            {
                                thisResult = ProcessCustomFiles(fileName.FullName);
                            }
                            if (thisResult.Processed)
                            {
                                try
                                {
                                    if (File.Exists(fileName.FullName))
                                    {
                                        System.GC.Collect();
                                        System.GC.WaitForPendingFinalizers();
                                        File.Delete(fileName.FullName);
                                    }

                                }
                                catch (Exception ex)
                                {
                                    NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Unable to Delete File. Error :" + ex.Message);
                                }
                            }
                        }

                    }
                    catch (XmlException ex)
                    {
                        string exStr = ex.GetType().Name;
                        NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + exStr + " Error Found while procesing " + fileName.Name + " Moving to Failed", Color.Red);
                        MailModule.sendMsg(fileName.FullName, Globals.glAlertsTo, "Error Processing file. " + exStr + " error found", ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.StackTrace);
                        NodeResources.MoveFile(fileName.FullName, Path.Combine(Globals.glFailPath));



                    }
                    catch (Exception ex)
                    {
                        string exStr = ex.GetType().Name;
                        NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + exStr + " Error Found while procesing " + fileName.Name + " Moving to Failed", Color.Red);
                        MailModule.sendMsg(fileName.FullName, Globals.glAlertsTo, "Error Processing file. " + exStr + " error found", ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.StackTrace);
                        NodeResources.MoveFile(fileName.FullName, Path.Combine(Globals.glFailPath));



                    }


                }
                stopwatch.Stop();
                double t = stopwatch.Elapsed.TotalSeconds;
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Processing Complete. " + list.Length + " files processed in " + t + " seconds");
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Waiting for Files.");
            }

        }

        private ProcessResult ProcessCustomFiles(String xmlFile)
        {
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;
            NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File " + Path.GetFileName(xmlFile) + " Is not a Cargowise File. Checking for Against Customer.");
            FileInfo processingFile = new FileInfo(xmlFile);
            filesRx++;
            NodeResources.AddLabel(lblFilesRx, filesRx.ToString());
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            TransReference trRef = new TransReference();
            String archiveFile;
            string recipientID = "";
            string senderID = "";
            archiveFile = Globals.ArchiveFile(Globals.glArcLocation, xmlFile);
            if (processingFile.Extension.ToUpper() == ".XML")
            {
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " New XML file found " + Path.GetFileName(processingFile.FullName) + ". Processing" + "");
                CustomerProfile customerProfile = new CustomerProfile(Globals.ConnString.ConnString);
                var xmlProfList = customerProfile.GetXMLProfiles();

                //SqlDataAdapter daXML = new SqlDataAdapter("SELECT C_CODE, C_ID, C_PATH, P_XSD, P_DESCRIPTION, P_ID, P_RECIPIENTID, P_SENDERID, P_SERVER, P_USERNAME, P_PASSWORD, P_LIBNAME, P_BILLTO,  P_PARAMLIST, P_METHOD from VW_CustomerProfile where P_XSD is not null and P_ACTIVE='Y'", sqlConn);
                //DataSet dsXML = new DataSet();
                //daXML.Fill(dsXML, "XMLFile");
                Boolean processed = false;
                int iLineno = 0;
                while (!processed)
                {
                    foreach (var profile in xmlProfList)
                    {
                        try
                        {
                            var jobDet = ProcessJapanNNR(xmlFile);
                            //}
                            if (jobDet != null)
                            {
                                Transaction trans = new Transaction
                                {
                                    T_DATETIME = DateTime.Now,
                                    T_FILENAME = xmlFile,
                                    T_REF1 = jobDet.Master,
                                    T_REF2 = jobDet.HouseBills.ToCommaList(),
                                    T_REF1TYPE = "Master",
                                    T_REF2TYPE = "House",
                                    T_DIRECTION = "S",
                                    T_CWFILECREATED = jobDet.CWFileCreated,
                                    T_COUNTRY = jobDet.Country,
                                    T_CWCODE = jobDet.CWCode

                                };
                                TransactionControl tc = new TransactionControl(Globals.connString());
                                tc.AddTransaction(trans);
                                recipientID = profile.P_RECIPIENTID;
                                senderID = profile.P_SENDERID;
                            }

                            processed = true;
                            thisResult.Processed = true;
                        }
                        catch (XmlSchemaValidationException ex)
                        {

                            //MailModule.sendMsg("", Globals.glAlertsTo, "XML Schema Validation issue", "Invalid schema definition error:" + ex.Message);
                            ProcessingErrors procerror = new ProcessingErrors();
                            CTCErrorCode error = new CTCErrorCode();
                            error.Code = NodeError.e111;
                            error.Description = "Customer XML Error :" + ex.Message;
                            error.Severity = "Warning";
                            procerror.ErrorCode = error;
                            procerror.SenderId = senderID;
                            procerror.RecipientId = recipientID;
                            procerror.FileName = xmlFile;

                            MailModule.sendMsg(xmlFile, Globals.glAlertsTo, "MLASYDSYD: " + ex.Message + "Line No: " + iLineno.ToString(), ex);
                            NodeResources.MoveFile(xmlFile, Path.Combine(Globals.glFailPath));
                            NodeResources.AddProcessingError(procerror);
                            thisResult.FolderLoc = Globals.glFailPath;
                            thisResult.Processed = true;
                            processed = true;
                            NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Invalid schema definition error:" + ex.Message + ". " + "", Color.Red);


                        }
                        catch (XmlException ex)
                        {


                            ProcessingErrors procerror = new ProcessingErrors();

                            CTCErrorCode error = new CTCErrorCode();
                            error.Code = NodeError.e111;
                            error.Description = "Customer XML Error :" + ex.Message;
                            error.Severity = "Warning";
                            procerror.ErrorCode = error;
                            procerror.SenderId = senderID;
                            procerror.RecipientId = recipientID;
                            procerror.FileName = xmlFile;
                            NodeResources.MoveFile(xmlFile, Path.Combine(Globals.glFailPath));
                            NodeResources.AddProcessingError(procerror);
                            thisResult.FolderLoc = Globals.glFailPath;
                            thisResult.Processed = true;
                            processed = true;
                            NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node Exception Found: Custom Processing:" + ex.Message + ". " + "", Color.Red);
                            //   return thisResult;

                        }
                        catch (Exception ex)
                        {
                            ProcessingErrors procerror = new ProcessingErrors();
                            CTCErrorCode error = new CTCErrorCode();
                            error.Code = NodeError.e111;
                            error.Description = "Customer XML Error :" + ex.Message;
                            error.Severity = "Warning";
                            procerror.ErrorCode = error;
                            procerror.SenderId = senderID;
                            procerror.RecipientId = recipientID;
                            procerror.FileName = xmlFile;
                            NodeResources.AddProcessingError(procerror);
                            // File.Move(xmlFile, Path.Combine(Globals.glFailPath, Path.GetFileName(xmlFile)));
                            thisResult.FolderLoc = Globals.glFailPath;
                            thisResult.Processed = false;
                            processed = true;
                            NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node Exception Found: Custom Processing:" + ex.Message + ". " + "", Color.Red);
                            //   return thisResult;
                        }
                    }
                    if (!processed)
                    {
                        ProcessingErrors procerror = new ProcessingErrors();
                        CTCErrorCode error = new CTCErrorCode();
                        error.Code = NodeError.e111;
                        error.Description = "Unable to process " + Path.GetFileName(xmlFile) + "No matching routines found. Moving to Failed.";
                        error.Severity = "Warning";
                        procerror.ErrorCode = error;
                        procerror.SenderId = senderID;
                        procerror.RecipientId = recipientID;
                        procerror.FileName = xmlFile;
                        NodeResources.AddProcessingError(procerror);
                        thisResult.FolderLoc = Globals.glFailPath;
                        thisResult.Processed = false;
                        NodeResources.MoveFile(xmlFile, Path.Combine(Globals.glFailPath));
                        NodeResources.AddProcessingError(procerror);
                        thisResult.Processed = false;
                        thisResult.FolderLoc = Globals.glFailPath;
                        NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Unable to process " + Path.GetFileName(xmlFile) + "No matching routines found. Moving to Failed." + "", Color.Red);
                        processed = true;
                        //  return thisResult;
                    }

                }
            }
            return thisResult;
        }

        private void GetProcErrors()
        {
            ProcessingError procErrors = new ProcessingError(Globals.ConnString.ConnString);
            var procerrorList = procErrors.GetProcErrorList();
            ;
            dgProcessingErrors.AutoGenerateColumns = false;

            dgProcessingErrors.DataSource = procerrorList;



        }


        private MasterHouseRef ProcessJapanNNR(string xmlFile)
        {
            MasterHouseRef result = new MasterHouseRef();

            string cwCode = string.Empty;
            MethodBase m = MethodBase.GetCurrentMethod();
            _errorMessage = string.Empty;
            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters(), Path.GetDirectoryName(Application.ExecutablePath));

            root nnrJapan = new root();
            FileInfo fi = new FileInfo(xmlFile);
            NodeResources.AddRTBText(rtbLog, "Now Processing NNR Global (JPN) file :" + fi.Name);
            Globals.ArchiveFile(Globals.glArcLocation, xmlFile);
            string flightPrefix = string.Empty;
            using (StreamReader reader = new StreamReader(xmlFile))
            {
                try
                {
                    XmlSerializer serJapan = new XmlSerializer(typeof(root));
                    nnrJapan = (root)serJapan.Deserialize(reader);
                }
                catch (Exception ex)
                {
                    string strEx = ex.GetType().Name;
                    NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node " + strEx + " Exception Found: Housebill Error:" + ex.Message + ". " + "", Color.Red);
                    string errMsg = "Exception " + ex.GetType().Name + Environment.NewLine +
                        ex.StackTrace;

                    MailModule.sendMsg("", Globals.glAlertsTo, "Error in " + m.Name, errMsg);
                    _errorMessage = "Exception Found: " + m.Name + "Params: " + m.GetParameters();
                    return null;
                }

            }

            string fileType = string.Empty;
            //Create the XUS Interchange Header
            Shipment consol = new Shipment();
            DataContext consolDataContext = new DataContext();
            // TODO Need to determine SYD/AKL
            var nnrCompany = nnrJapan.Master.Parties != null ? GetCompanyFromParty(nnrJapan.Master.Parties, "IMFORW") : null;
            result.Country = nnrCompany.country_code;
            Company company = GetNewCompany(nnrCompany.country_code);
            List<DataTarget> dataConsolTargetColl = new List<DataTarget>();
            DataTarget dataConsolTarget = new DataTarget();
            CodeDescriptionPair transMode = new CodeDescriptionPair();
            CodeDescriptionPair paymentMode = new CodeDescriptionPair();
            CodeDescriptionPair cdPlace = new CodeDescriptionPair();
            WayBillType wayBill = new WayBillType();
            Date date = new Date();
            rootMaster master = nnrJapan.Master;
            consolDataContext.DataTargetCollection = dataConsolTargetColl.ToArray();
            List<Shipment> shipmentColl = new List<Shipment>();
            //Create the Shipment Object to hold the constructing Consol and shipments
            result.Master = master.bill_no;
            switch (master.mode_code.ToUpper().Trim())
            {
                case "OCEAN":
                    // Create the Shipment(House Bill Level)
                    if (nnrCompany.country_code == "NZ")
                    {
                        return null;
                    }
                    dataConsolTarget.Type = "ForwardingShipment";
                    dataConsolTargetColl.Add(dataConsolTarget);
                    transMode.Code = "SEA";
                    transMode.Description = "Sea Freight";
                    //consol.ContainerMode = new ContainerMode { Code = nnrJapan.Master.load_type };
                    shipmentColl = ProcessNNRShipment(nnrJapan.houses);
                    if (shipmentColl == null)
                    {
                        if (_errorMessage != string.Empty)
                        {
                            NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                            return result;
                        }
                    }
                    consol = null;
                    fileType = "S";
                    break;
                case "AIR":
                    // Create the Consol (Master House Bill Level)
                    dataConsolTarget.Type = "ForwardingConsol";
                    dataConsolTargetColl.Add(dataConsolTarget);
                    transMode.Code = "AIR";
                    transMode.Description = "Air Freight";
                    fileType = "A";
                    consol = ProcessNNRConsol(master);
                    Shipment subShipment = new Shipment();
                    if (nnrJapan.houses == null)
                    {
                        NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "(B/L: " + master.bill_no + "No Houses contained in Master. " + "", Color.Blue);
                    }
                    else
                    {
                        shipmentColl = ProcessNNRShipment(nnrJapan.houses);
                        if (shipmentColl == null)
                        {
                            if (_errorMessage != string.Empty)
                            {
                                NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                                return result;
                            }
                        }
                    }
                    break;
            }

            if (consol == null)
            {
                var ships = new List<string>();
                foreach (Shipment shipment in shipmentColl)
                {
                    CreateSeaShipmentFile(shipment, GetCompanyFromParty(master.Parties, "IMFORW"));
                    ships.Add(shipment.WayBillNumber);
                }
                result.HouseBills = ships;
                return result;

            }
            else
            {
                if (shipmentColl != null)
                {
                    Imported_Consol checkConsol = new Imported_Consol
                    {
                        J_POL = consol.PortOfLoading.Code,
                        J_POD = consol.PortOfDischarge.Code,
                        J_MODE = consol.TransportMode.Code,
                        J_OBL = consol.WayBillType.Code == "MWB" ? consol.WayBillNumber : null,
                        J_VOYAGE = consol.VoyageFlightNo,
                        J_VESSEL = consol.VesselName

                    };
                    cwCode = GetCWEnums("CUSTCODE", nnrCompany.company_code);
                    result.CWCode = cwCode;
                    var ships = new List<string>();
                    foreach (Shipment sh in shipmentColl)
                    {
                        dataConsolTarget = new DataTarget();
                        dataConsolTarget.Type = "ForwardingShipment";
                        dataConsolTargetColl.Add(dataConsolTarget);
                        if (sh.DateCollection != null)
                        {
                            checkConsol.J_ETD = GetDate(sh.DateCollection, DateType.Departure);
                            checkConsol.J_ETA = GetDate(sh.DateCollection, DateType.Arrival);
                        }
                        ConfirmShipments confirmConsol = new ConfirmShipments(Globals.ConnString.ConnString);
                        var cwConsol = confirmConsol.CheckObl(checkConsol);
                        string ignoreObl = string.Empty;
                        ships.Add(sh.WayBillNumber);
                        if (cwConsol != null)
                        {
                            ignoreObl = "Master Bill Number has been found and will be ignored.";
                            if (cwConsol.J_ETA != GetDate(consol.DateCollection, DateType.Arrival) ||
                                cwConsol.J_ETD != GetDate(consol.DateCollection, DateType.Departure))
                            {
                                ignoreObl += "\n Found that the Dates were not the same as the Cargowse data.";
                            }
                            if (cwConsol.J_POL != consol.PortOfLoading.Code ||
                                cwConsol.J_POD != consol.PortOfDischarge.Code)
                            {
                                ignoreObl += "\n Found that the Ports were not the same as the Cargowise data.";
                            }
                            if (cwConsol.J_VESSEL != consol.VesselName || cwConsol.J_VOYAGE != consol.VoyageFlightNo)
                            {
                                ignoreObl += "\n Found that the Vessel/ or Voyage details were not the same as Cargowise Data";
                            }
                            string msg = "Master Details found in a previously Registered Job. (" + cwConsol.J_CONSOLNO.Trim() + ") and will be rejected because: \n " + ignoreObl;
                            try
                            {
                                MailModule.sendMsg(xmlFile, Globals.glAlertsTo, "NNR Japan File Rejected: " + nnrJapan.Master.bill_no, msg);
                            }
                            catch (Exception)
                            {

                            }
                            NodeResources.AddRTBText(rtbLog, msg);
                            result.HouseBills = ships;
                            return result;
                        }


                    }
                    result.HouseBills = ships;

                }
                consol.SubShipmentCollection = shipmentColl.ToArray();

            }
            if (dataConsolTargetColl.Count > 0)
            {
                consolDataContext.DataTargetCollection = dataConsolTargetColl.ToArray();
                consol.DataContext = consolDataContext;
            }
            consol.DataContext.Company = company;
            consol.DataContext.DataProvider = "NNRGLOJPN";
            consol.DataContext.ServerID = company.Code;
            consol.TransportMode = transMode;
            UniversalShipmentData bodyField = new UniversalShipmentData();
            bodyField.Shipment = consol;
            UniversalInterchangeBody body = new UniversalInterchangeBody();
            body.BodyField = bodyField;

            UniversalInterchangeHeader header = new UniversalInterchangeHeader
            {
                RecipientID = cwCode,
                SenderID = "NNRGLOJPN"
            };

            UniversalInterchange interchange = new UniversalInterchange();
            interchange.Body = body;
            interchange.version = "1.1";
            interchange.Header = header;

            String cwXML = Path.Combine(Globals.glOutputDir, "NNR-" + company.Country.Code + "-" + fileType + "-" + FixNumber(nnrJapan.Master.bill_no) + ".xml");
            int iFileCount = 0;
            while (File.Exists(cwXML))
            {
                iFileCount++;
                cwXML = Path.Combine(Globals.glOutputDir, "NNR-" + company.Country.Code + "-" + fileType + "-" + FixNumber(nnrJapan.Master.bill_no) + iFileCount + ".xml");
            }
            Stream outputCW = File.Open(cwXML, FileMode.Create);
            StringWriter writer = new StringWriter();
            interchange.Body = body;
            XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
            var cwNSUniversal = new XmlSerializerNamespaces();
            cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
            xSer.Serialize(outputCW, interchange, cwNSUniversal);
            outputCW.Flush();
            outputCW.Close();
            Globals.ArchiveFile(Globals.glArcLocation, cwXML);
            result.CWFileCreated = true;
            return result;
        }

        private string GetCWEnums(string enumType, string value)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteEntity(Globals.ConnString.ConnString)))
            {
                var enums = (uow.CargowiseEnums.Find(cw => cw.CW_ENUMTYPE == enumType && cw.CW_MAPVALUE == value)).FirstOrDefault();
                if (enums != null)
                {
                    return enums.CW_ENUM.Trim();
                }
                return null;

            }

        }

        private Company GetNewCompany(string country_code)
        {
            var company = new Company();
            switch (country_code)
            {
                case "AU":
                    company.Code = "SYD";
                    company.Name = "NNR Global Logistics Australia Pty Ltd";
                    company.Country = new Country { Code = "AU", Name = "Australia" };
                    break;
                case "NZ":
                    company.Code = "AKL";
                    company.Name = "NNR Global Logistics New Zealand Pty Ltd";
                    company.Country = new Country { Code = "NZ", Name = "New Zealand" };

                    break;
            }
            return company;
        }

        private PartiesPartyCompany GetCompanyFromParty(PartiesParty[] parties, string partyType)
        {
            return (from c in parties
                    where c.party_type == partyType
                    select c.Company).FirstOrDefault();
        }

        private DateTime? GetDate(Date[] dateCollection, DateType dateType)
        {
            var returndate = (from d in dateCollection
                              where d.Type == dateType
                              select d.Value).FirstOrDefault();
            DateTime T;
            if (DateTime.TryParse(returndate, out T))
            {
                return T;
            }

            return null;
        }

        private void CreateSeaShipmentFile(Shipment shipment, PartiesPartyCompany nnrCompany)
        {
            string fileType = string.Empty;
            //Create the XUS Interchange Header
            fileType = shipment.TransportMode.Code[0].ToString().Trim();
            Shipment consol = new Shipment();
            DataContext consolDataContext = new DataContext();
            Company company = new Company();
            switch (nnrCompany.country_code)
            {
                case "AU":
                    company.Code = "SYD";
                    company.Name = "NNR Global Logistics Australia Pty Ltd";
                    company.Country = new Country { Code = "AU", Name = "Australia" };
                    break;
                case "NZ":
                    company.Code = "AKL";
                    company.Name = "NNR Global Logistics New Zealand Pty Ltd";
                    company.Country = new Country { Code = "NZ", Name = "New Zealand" };
                    break;
            }

            List<DataTarget> dataConsolTargetColl = new List<DataTarget>();
            DataTarget dataConsolTarget = new DataTarget();
            CodeDescriptionPair transMode = new CodeDescriptionPair();
            CodeDescriptionPair paymentMode = new CodeDescriptionPair();
            CodeDescriptionPair cdPlace = new CodeDescriptionPair();
            WayBillType wayBill = new WayBillType();
            Date date = new Date();
            //try
            //{
            // Create the Consol (Master House Bill Level)
            consol = shipment;

            consolDataContext.DataTargetCollection = dataConsolTargetColl.ToArray();
            if (dataConsolTargetColl.Count > 0)
            {
                consolDataContext.DataTargetCollection = dataConsolTargetColl.ToArray();
                consol.DataContext = consolDataContext;
            }
            consol.DataContext.Company = company;
            consol.DataContext.DataProvider = "NNRGLOJPN";
            consol.DataContext.ServerID = "SYD";
            //consolDataContext.DataTargetCollection = dataConsolTargetColl.ToArray();
            //consol.DataContext = consolDataContext;
            UniversalShipmentData bodyField = new UniversalShipmentData();
            bodyField.Shipment = consol;
            UniversalInterchangeBody body = new UniversalInterchangeBody();
            body.BodyField = bodyField;
            UniversalInterchangeHeader header = new UniversalInterchangeHeader
            {
                RecipientID = GetCWEnums("CUSTCODE", nnrCompany.company_code),
                SenderID = "NNRGLOJPN"
            };
            UniversalInterchange interchange = new UniversalInterchange();
            interchange.Body = body;
            interchange.version = "1.1";
            interchange.Header = header;

            String cwXML = Path.Combine(Globals.glOutputDir, "NNR-" + nnrCompany.country_code + "-" + fileType + "-" + FixNumber(shipment.WayBillNumber) + ".xml");
            int iFileCount = 0;
            while (File.Exists(cwXML))
            {
                iFileCount++;
                cwXML = Path.Combine(Globals.glOutputDir, "NNR-" + nnrCompany.country_code + "-" + FixNumber(shipment.WayBillNumber) + iFileCount + ".xml");
            }
            Stream outputCW = File.Open(cwXML, FileMode.Create);
            StringWriter writer = new StringWriter();
            interchange.Body = body;
            XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
            var cwNSUniversal = new XmlSerializerNamespaces();
            cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
            xSer.Serialize(outputCW, interchange, cwNSUniversal);
            outputCW.Flush();
            outputCW.Close();
        }

        private Shipment ProcessNNRConsol(rootMaster master)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            flightPrefix = "";
            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters(), Path.GetDirectoryName(Application.ExecutablePath));
            Shipment result = new Shipment();
            CodeDescriptionPair paymentMode = new CodeDescriptionPair();
            if (master.mode_code == "AIR")
            {
                result.ContainerMode = new ContainerMode { Code = "LSE" };
                result.TransportMode = new CodeDescriptionPair { Code = "AIR" };
            }
            else
            {
                result.ContainerMode = new ContainerMode { Code = master.load_type };
                result.TransportMode = new CodeDescriptionPair { Code = "SEA" };
            }

            switch (master.freight_term_code)
            {
                case "PPD":
                    paymentMode.Code = "PPD";
                    paymentMode.Description = "Pre-Paid";
                    break;
                default:
                    paymentMode.Code = "CCX";
                    paymentMode.Description = "Collect";
                    break;
            }
            result.DocumentedChargeable = master.chargeable_weight;
            result.DocumentedChargeableSpecified = true;
            result.DocumentedVolume = master.volume;
            try
            {
                try
                {
                    if (master.no_of_originals == "EXPRESS")
                    {
                        result.ReleaseType = new CodeDescriptionPair { Code = "EBL", Description = "Express Bill of Lading" };
                    }
                    else
                    {
                        if (master.no_of_originals != null)
                        {
                            result.NoOriginalBills = (sbyte)int.Parse(master.no_of_originals);
                            result.NoOriginalBillsSpecified = true;
                        }

                    }

                }
                catch (Exception)
                {


                }
                result.PaymentMethod = paymentMode;
                Currency payCurrency = new Currency
                {
                    Code = master.currency_code
                };
                result.FreightRateCurrency = payCurrency;
                result.BookingConfirmationReference = string.IsNullOrEmpty(master.booking_no) ? master.booking_no : string.Empty;
                result.CoLoadMasterBillNumber = string.IsNullOrEmpty(master.second_bill_no) ? master.second_bill_no : string.Empty;
                result.AgentsReference = master.file_no;
                result.WayBillNumber = master.bill_no;
                WayBillType wayBill = new WayBillType();
                switch (master.bill_type)
                {
                    case "D":
                        wayBill.Code = "MWB";
                        wayBill.Description = "Master Waybill";
                        break;

                    case "M":
                        wayBill.Code = "MWB";
                        wayBill.Description = "Master Waybill";
                        break;
                }
                result.WayBillType = wayBill;
                result.TotalNoOfPieces = master.total_pieces;
                result.TotalNoOfPiecesSpecified = true;
                decimal allocVol = 0;
                if (decimal.TryParse(master.allocated_volume, out allocVol))
                {
                    result.TotalPreallocatedVolume = allocVol;
                }

                result.TotalPreallocatedVolumeSpecified = true;
                result.TotalVolume = master.volume;
                result.TotalVolumeSpecified = true;
                result.TotalWeight = master.gross_weight;

            }
            catch (Exception ex)
            {
                ProcessingErrors procerror = new ProcessingErrors();
                string strEx = ex.GetType().Name;
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node " + strEx + " Exception Found: Housebill Error:" + ex.Message + ". " + "", Color.Red);
                string errMsg = "Exception " + ex.GetType().Name + Environment.NewLine +
                    ex.StackTrace;

                MailModule.sendMsg("", Globals.glAlertsTo, "Error in " + m.Name, errMsg);
                _errorMessage = "Exception Found: " + m.Name + "Params: " + m.GetParameters();

            }

            try
            {
                //Loop through Locations to create the locations in the Consol. 
                if (master.Locations != null)
                {
                    foreach (LocationsLocation loc in master.Locations)
                    {
                        switch (loc.location_type)
                        {
                            case "DELIVERY":
                                result.PlaceOfDelivery = new UNLOCO { Code = loc.location_code };
                                break;
                            case "LOADING":
                                result.PortOfLoading = new UNLOCO { Code = loc.location_code };
                                break;
                            case "RECEIPT":
                                result.PlaceOfReceipt = new UNLOCO { Code = loc.location_code };
                                break;
                            case "DISCHARGE":
                                result.PortOfDischarge = new UNLOCO { Code = loc.location_code };
                                break;
                            case "DEST":
                                result.PortOfDestination = new UNLOCO { Code = loc.location_code };
                                break;
                            case "ORG":
                                result.PortOfOrigin = new UNLOCO { Code = loc.location_code };
                                break;
                        }
                    }
                }

            }
            catch (NullReferenceException)
            {
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "(B/L: " + master.bill_no + " No Locations found on Master. " + "", Color.Blue);
            }
            // Loop through the Parties to create the Organization collection for the consol
            List<OrganizationAddress> masterOrgColl = GetOrgListFromParties(master.Parties);
            result.OrganizationAddressCollection = masterOrgColl.ToArray();

            // Loop through the Dates element to create the DateCollection element
            List<Date> masterDates = new List<Date>();
            if (master.Dates == null)
            {

                masterDates = GetDatesFromMasterRoutes(master.Routings);

            }
            else
            {
                masterDates = GetDatesFromMasterDates(master.Dates);
            }
            result.DateCollection = masterDates.ToArray();
            List<RoutingsRouting> masterRoutings = master.Routings.ToList();
            int routes = masterRoutings.Count;

            // Loop through the Transport Entries element to create the TrnasportLeg Collection element
            List<TransportLeg> consolTransportCollection = GetRoutesFromMaster(master.Routings);
            result.PortOfOrigin = GetPOOFromRoutes(master.Routings);
            result.PortOfLoading = GetPOOFromRoutes(master.Routings);
            result.PlaceOfReceipt = result.PortOfLoading;
            result.PortOfDischarge = GetPODFromRoutes(master.Routings);
            result.VoyageFlightNo = GetVoyFromRoutes(master.Routings);
            result.VesselName = GetVesselFromRoutes(master.Routings);
            //TransportLeg consolTransportLeg = new TransportLeg();
            result.TransportLegCollection = consolTransportCollection.ToArray();
            return result;
        }

        private string GetVoyFromRoutes(RoutingsRouting[] routings)
        {
            var route = (from r in routings
                         orderby r.sequence_id
                         select r).FirstOrDefault();
            if (route != null)
            {

                return route.mode_code == "OCEAN" ? route.flight_voyage_no : flightPrefix + route.flight_voyage_no;
            }
            return null;
        }

        private string GetVesselFromRoutes(RoutingsRouting[] routings)
        {
            var route = (from r in routings
                         orderby r.sequence_id
                         select r).FirstOrDefault();
            if (route != null)
            {

                return route.mode_code == "OCEAN" ? route.vessel_name : string.Empty;
            }
            return null;
        }

        private UNLOCO GetPODFromRoutes(RoutingsRouting[] routings)
        {
            var route = (from r in routings
                         orderby r.sequence_id
                         select r).LastOrDefault();
            if (route != null)
            {
                return new UNLOCO { Code = route.destination_locode };
            }
            return null;
        }

        private UNLOCO GetPOOFromRoutes(RoutingsRouting[] routings)
        {

            var route = (from r in routings
                         orderby r.sequence_id
                         select r).FirstOrDefault();
            if (route != null)
            {
                if (route.sequence_id == 1)
                {
                    return new UNLOCO { Code = route.departure_locode };
                }
            }
            return null;
        }

        private List<TransportLeg> GetRoutesFromMaster(RoutingsRouting[] routings)
        {
            List<TransportLeg> transportLegColl = new List<TransportLeg>();

            foreach (var route in routings)
            {
                TransportLeg transportLeg = new TransportLeg();
                switch (route.routing_type.ToUpper())
                {
                    case "MAIN":
                        if (route.mode_code == "AIR")
                        {
                            switch (route.sequence_id)
                            {
                                case 1:
                                    transportLeg.LegType = TransportLegLegType.Flight1;
                                    break;
                                case 2:
                                    transportLeg.LegType = TransportLegLegType.Flight2;
                                    break;
                                case 3:
                                    transportLeg.LegType = TransportLegLegType.Flight3;
                                    break;
                            }

                        }
                        else
                        {
                            transportLeg.LegType = TransportLegLegType.Main;
                        }
                        break;
                    case "SECOND":
                        if (route.mode_code == "AIR")
                        {
                            transportLeg.LegType = TransportLegLegType.Flight2;
                        }
                        else
                        {
                            transportLeg.LegType = TransportLegLegType.OnForwarding;
                        }
                        break;
                }
                switch (route.mode_code)
                {
                    case "AIR":
                        transportLeg.TransportMode = TransportLegTransportMode.Air;
                        transportLeg.VoyageFlightNo = flightPrefix + route.flight_voyage_no;
                        break;
                    case "OCEAN":
                        transportLeg.TransportMode = TransportLegTransportMode.Sea;
                        transportLeg.VoyageFlightNo = route.flight_voyage_no;
                        transportLeg.VesselName = route.vessel_name;
                        break;
                }
                transportLeg.LegOrder = (sbyte)route.sequence_id;
                transportLeg.PortOfLoading = new UNLOCO { Code = route.departure_locode };
                transportLeg.PortOfDischarge = new UNLOCO { Code = route.destination_locode };
                if (!string.IsNullOrEmpty(route.etd))
                {
                    transportLeg.EstimatedDeparture = route.etd;
                }

                if (!string.IsNullOrEmpty(route.eta))
                {
                    transportLeg.EstimatedArrival = route.eta;
                }

                transportLegColl.Add(transportLeg);
            }
            return transportLegColl;
        }

        private List<Date> GetDatesFromMasterRoutes(RoutingsRouting[] routings)
        {
            List<Date> dates = new List<Date>();
            Date date = new Date();
            var route = (from r in routings
                         where r.sequence_id == 1
                         select r).FirstOrDefault();
            if (route != null)
            {
                date.Type = DateType.Departure;
                date.Value = route.etd;
                dates.Add(date);
            }

            route = (from r in routings
                     orderby r.sequence_id
                     select r).LastOrDefault();

            if (route != null)
            {
                date = new Date();
                date.Type = DateType.Arrival;
                date.Value = route.eta;
                dates.Add(date);
            }
            return dates;

        }

        private List<Date> GetDatesFromMasterDates(DatesDate[] dates)
        {
            List<Date> dateColl = new List<Date>();

            foreach (var nnrDate in dates)
            {
                Date date = new Date();
                date.Value = nnrDate.date_value;
                var dateType = GetCWEnums("DATETYPE", nnrDate.date_type);
                if (dateType != null)
                {
                    date.Type = (DateType)Enum.Parse(typeof(DateType), dateType);
                    dateColl.Add(date);
                }


            }
            return dateColl;
        }

        private List<OrganizationAddress> GetOrgListFromParties(PartiesParty[] parties)
        {
            List<OrganizationAddress> orgColl = new List<OrganizationAddress>();
            OrganizationAddress org = new OrganizationAddress();

            foreach (PartiesParty party in parties)
            {
                org = new OrganizationAddress();
                var orgType = GetCWEnums("ORGTYPE", party.party_type);
                if (orgType == null)
                {
                    continue;
                }
                org.AddressType = orgType;
                if (org.AddressType == "ShippingLineAddress")
                {
                    flightPrefix = party.Carrier.air_iata_carrier_code;

                }
                org.CompanyName = party.Company.name;
                org.Address1 = party.Address.street_address_1;
                org.Address2 = party.Address.street_address_2;
                org.City = party.Address.city;
                org.State = party.Address.state_province;
                org.Postcode = party.Address.postal_code;
                org.Country = new Country { Code = party.Address.country_code };
                org.Phone = party.Address.tel_no;
                List<RegistrationNumber> regoNumberColl = new List<RegistrationNumber>();
                RegistrationNumber regonumber = new RegistrationNumber();
                regonumber.CountryOfIssue = new Country { Code = "AU" };
                regonumber.Type = new RegistrationNumberType { Code = "LSC" };
                regonumber.Value = party.Company.company_code;
                regoNumberColl.Add(regonumber);
                org.RegistrationNumberCollection = regoNumberColl.ToArray();
                orgColl.Add(org);
            }
            return orgColl;
        }

        private List<Shipment> ProcessNNRShipment(rootHousesHouse[] houses)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            _errorMessage = string.Empty;
            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters(), Path.GetDirectoryName(Application.ExecutablePath));
            Shipment subShipment = new Shipment();
            string flightPrefix = string.Empty;
            List<Shipment> result = new List<Shipment>();
            string transportMode = string.Empty;
            foreach (rootHousesHouse house in houses)
            {
                try
                {
                    pceType = string.Empty;
                    // Add the Datacontext to the Shipment and Add the ForwardingShipment to the Consol
                    List<DataTarget> dsShipmentTargetColl = new List<DataTarget>();
                    DataTarget dsShipmentTarget = new DataTarget();
                    dsShipmentTarget.Type = "ForwardingShipment";
                    //dataConsolTarget = new DataTarget { Type = "ForwardingShipment" };
                    //dataConsolTargetColl.Add(dataConsolTarget);
                    dsShipmentTargetColl.Add(dsShipmentTarget);
                    subShipment = new Shipment();
                    subShipment.DataContext = new DataContext { DataTargetCollection = dsShipmentTargetColl.ToArray() };
                    bool containsString = house.bill_no.Substring(0, 3).Any(char.IsLetter);
                    if (!containsString)
                    {
                        subShipment.WayBillNumber = "NNR" + house.bill_no.Replace(" ", string.Empty);
                    }
                    else
                    {
                        subShipment.WayBillNumber = house.bill_no;
                    }

                    if (house.bill_type == "H")
                    {
                        subShipment.WayBillType = new WayBillType { Code = "HWB", Description = "House WayBill" };
                    }
                    if (house.Routings[0] != null)
                    {
                        switch (house.mode_code)
                        {
                            case "OCEAN":

                                subShipment.TransportMode = new CodeDescriptionPair { Code = "SEA" };
                                subShipment.ContainerMode = new ContainerMode { Code = house.load_type };
                                break;
                            case "AIR":

                                subShipment.TransportMode = new CodeDescriptionPair { Code = "AIR" };
                                subShipment.ContainerMode = new ContainerMode { Code = "LSE" };
                                break;
                        }
                    }


                    subShipment.DocumentedChargeable = house.chargeable_weight;
                    subShipment.DocumentedChargeableSpecified = true;
                    subShipment.DocumentedVolume = house.volume;
                    subShipment.PaymentMethod = new CodeDescriptionPair { Code = GetCWEnums("FREIGHTTERM", house.freight_term_code) };

                    try
                    {
                        if (house.no_of_originals == "EXPRESS")
                        {
                            subShipment.ReleaseType = new CodeDescriptionPair { Code = "EBL", Description = "Express Bill of Lading" };
                        }
                        else
                        {
                            if (house.no_of_originals != null)
                            {
                                int noBills;
                                if (int.TryParse(house.no_of_originals, out noBills))
                                {
                                    subShipment.NoOriginalBills = (sbyte)noBills;
                                    subShipment.NoOriginalBillsSpecified = true;
                                }

                            }
                        }
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        //Loop through Locations to create the locations in the Shipment. 
                        foreach (LocationsLocation loc in house.Locations)
                        {
                            switch (loc.location_type.ToUpper())
                            {
                                case "DELIVERY":
                                    subShipment.PlaceOfDelivery = new UNLOCO { Code = loc.location_code };
                                    break;
                                case "LOADING":
                                    subShipment.PortOfLoading = new UNLOCO { Code = loc.location_code };
                                    if (subShipment.TransportMode.Code == "SEA")
                                    {
                                        subShipment.PortOfOrigin = new UNLOCO { Code = loc.location_code };
                                    }
                                    break;
                                case "RECEIPT":
                                    subShipment.PlaceOfReceipt = new UNLOCO { Code = loc.location_code };
                                    break;
                                case "DISCHARGE":
                                    subShipment.PortOfDischarge = new UNLOCO { Code = loc.location_code };
                                    if (subShipment.TransportMode.Code == "SEA")
                                    {
                                        subShipment.PortOfDestination = new UNLOCO { Code = loc.location_code };
                                    }
                                    break;
                                case "DEST":
                                    subShipment.PortOfDestination = new UNLOCO { Code = loc.location_code };
                                    break;
                                case "ORG":
                                    subShipment.PortOfOrigin = new UNLOCO { Code = loc.location_code };
                                    break;
                            }
                        }
                    }
                    catch (NullReferenceException)
                    {
                        NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "(B/L: " + house.bill_no + " No Locations found on HouseBill. " + "", Color.Blue);
                    }
                    subShipment.AgentsReference = house.file_no;
                    //try
                    //{
                    //    subShipment.BookingConfirmationReference = house.booking_no.ToString();
                    //}
                    //catch (NullReferenceException ex)
                    //{
                    //    NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "(B/L: " + house.bill_no + " No Booking Reference found on HouseBill. " + "", Color.Blue);
                    //}

                    //   subShipment.BookingConfirmationReference = house.booking_no;
                    subShipment.WayBillType = new WayBillType { Code = GetCWEnums("BILLTYPE", house.bill_type) };

                    OrganizationAddress houserOrg = new OrganizationAddress();
                    List<Date> shipmentDateColl = new List<Date>();
                    if (house.Dates != null)
                    {
                        shipmentDateColl = GetDatesFromMasterDates(house.Dates);
                        subShipment.DateCollection = shipmentDateColl.ToArray();
                    }



                    //try
                    //{
                    //    Date date = new Date();
                    //    foreach (DatesDate shipDate in house.Dates)
                    //    {
                    //        date = new Date();
                    //        date.Value = shipDate.date_value;
                    //        date.Type = (DateType)Enum.Parse(typeof(DateType), NodeResources.GetEnum("DATETYPE", shipDate.date_type));
                    //        shipmentDateColl.Add(date);
                    //    }


                    //}
                    //catch (NullReferenceException)
                    //{
                    //    NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "(B/L: " + house.bill_no + " No Dates found on HouseBill. " + "", Color.Blue);
                    //}

                    decimal houseallocVol = 0;
                    if (decimal.TryParse(house.allocated_volume, out houseallocVol))
                    {
                        subShipment.TotalPreallocatedVolume = houseallocVol;
                    }


                    subShipment.TotalPreallocatedVolumeSpecified = true;
                    subShipment.TotalVolume = house.volume;
                    subShipment.TotalVolumeSpecified = true;
                    subShipment.TotalWeight = house.gross_weight;
                    subShipment.TotalWeightSpecified = true;


                    subShipment.TotalWeightUnit = new UnitOfWeight { Code = GetCWEnums("UOM", house.weight_unit) };
                    subShipment.FreightRateCurrency = new Currency { Code = house.currency_code };
                    subShipment.DocumentedWeight = house.gross_weight;
                    subShipment.DocumentedWeightSpecified = true;
                    subShipment.ManifestedWeight = house.gross_weight;
                    subShipment.ManifestedWeightSpecified = true;
                    subShipment.ManifestedVolume = house.volume;
                    subShipment.ManifestedVolumeSpecified = true;
                    subShipment.ActualChargeable = house.chargeable_weight;
                    subShipment.ActualChargeableSpecified = true;
                    List<OrganizationAddress> houserOrgColl = GetOrgListFromParties(house.Parties);
                    subShipment.OrganizationAddressCollection = houserOrgColl.ToArray();
                    subShipment.ShipmentIncoTerm = new IncoTerm { Code = house.incoterm_code };
                    subShipment.GoodsDescription = house.commodity;
                    subShipment.OuterPacks = house.total_pieces;
                    subShipment.TotalVolume = house.volume;
                    //foreach (PartiesParty party in house.Parties)
                    //{
                    //    houserOrg = new OrganizationAddress();
                    //    switch (party.party_type)
                    //    {
                    //        case "OVSADD":

                    //            if (party.type_description == "Overseas Address (Exp)")
                    //            {
                    //                houserOrg.AddressType = NodeResources.GetEnum("ORGTYPE", party.party_type);
                    //                houserOrg.CompanyName = party.Address.name;
                    //            }
                    //            break;
                    //        case "CARRIER":
                    //            houserOrg.AddressType = NodeResources.GetEnum("ORGTYPE", party.party_type);
                    //            houserOrg.CompanyName = party.Address.name;
                    //            flightPrefix = party.Carrier.air_iata_carrier_code;
                    //            break;
                    //        default:
                    //            houserOrg.AddressType = NodeResources.GetEnum("ORGTYPE", party.party_type);
                    //            houserOrg.CompanyName = party.Company.name;
                    //            break;
                    //    }
                    //    if (string.IsNullOrEmpty(houserOrg.AddressType))
                    //    {
                    //        NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + house.bill_no + " - Party Type: " + party.party_type + " has not been mapped. " + "", Color.Red);
                    //        continue;
                    //    }
                    //    else
                    //    {
                    //        houserOrg.Address1 = party.Address.street_address_1;
                    //        houserOrg.Address2 = party.Address.street_address_2;
                    //        houserOrg.City = party.Address.city;
                    //        houserOrg.State = party.Address.state_province;
                    //        houserOrg.Postcode = party.Address.postal_code;
                    //        houserOrg.Country = new Country { Code = party.Address.country_code };
                    //        List<RegistrationNumber> regoNumberColl = new List<RegistrationNumber>();
                    //        RegistrationNumber regonumber = new RegistrationNumber();
                    //        regonumber.CountryOfIssue = new Country { Code = "AU" };
                    //        regonumber.Type = new RegistrationNumberType { Code = "LSC" };
                    //        regonumber.Value = party.Address.company_code;
                    //        regoNumberColl.Add(regonumber);
                    //        houserOrg.RegistrationNumberCollection = regoNumberColl.ToArray();
                    //        houserOrgColl.Add(houserOrg);
                    //    }

                    ////}
                    //subShipment.OrganizationAddressCollection = houserOrgColl.ToArray();
                    switch (house.volume_unit.ToUpper())
                    {
                        case "M":
                            subShipment.TotalVolumeUnit = new UnitOfVolume { Code = "M3" };
                            break;
                        case "F":
                            subShipment.TotalVolumeUnit = new UnitOfVolume { Code = "CF" };
                            break;
                    }


                    //List<RoutingsRouting> shipRoutings = new List<RoutingsRouting>();
                    //shipRoutings = house.Routings.ToList();
                    int shipRoutes = house.Routings.Length;
                    List<TransportLeg> shipmentRouteColl = GetRoutesFromMaster(house.Routings);
                    if (house.mode_code == "AIR")
                    {

                    }
                    //foreach (RoutingsRouting shipRoute in house.Routings)
                    //{
                    //    TransportLeg shipmentRoute = new TransportLeg();
                    //    switch (shipRoute.routing_type.ToUpper())
                    //    {
                    //        case "MAIN":
                    //            if (house.mode_code.ToUpper() == "AIR")
                    //            {
                    //                shipmentRoute.TransportMode = TransportLegTransportMode.Air;

                    //                if (shipRoute.sequence_id == 1)
                    //                {
                    //                    shipmentRoute.LegType = TransportLegLegType.Flight1;


                    //                }
                    //                else
                    //                {
                    //                    shipmentRoute.LegType = TransportLegLegType.Flight2;
                    //                }

                    //            }
                    //            else
                    //            {
                    //                shipmentRoute.TransportMode = TransportLegTransportMode.Sea;
                    //                shipmentRoute.LegType = TransportLegLegType.Main;

                    //            }
                    //            break;
                    //        case "SECOND":
                    //            if (subShipment.TransportMode.Code == "AIR")
                    //            {
                    //                shipmentRoute.LegType = TransportLegLegType.Flight2;
                    //                shipmentRoute.TransportMode = TransportLegTransportMode.Air;

                    //            }
                    //            else
                    //            {
                    //                shipmentRoute.LegType = TransportLegLegType.OnForwarding;
                    //                shipmentRoute.TransportMode = TransportLegTransportMode.Sea;

                    //            }
                    //            break;
                    //    }
                    //    shipmentRoute.TransportModeSpecified = true;
                    //    if (shipRoutes == 1)
                    //    {
                    //        Date date = new Date();
                    //        shipmentRoute.EstimatedArrival = shipRoute.eta;
                    //        shipmentRoute.EstimatedDeparture = shipRoute.etd;
                    //        date.Value = shipRoute.etd;
                    //        date.Type = DateType.Departure;
                    //        date.IsEstimate = true;
                    //        date.IsEstimateSpecified = true;
                    //        shipmentDateColl.Add(date);
                    //        date = new Date();
                    //        shipmentRoute.EstimatedArrival = shipRoute.eta;
                    //        shipmentRoute.EstimatedDeparture = shipRoute.etd;
                    //        date.Value = shipRoute.eta;
                    //        date.Type = DateType.Arrival;
                    //        date.IsEstimate = true;
                    //        date.IsEstimateSpecified = true;
                    //        shipmentDateColl.Add(date);
                    //    }
                    //    else if (shipRoute.sequence_id == shipRoutes)
                    //    {
                    //        //     subShipment.PortOfDestination = new UNLOCO { Code = route.destination_locode };
                    //        Date date = new Date();
                    //        shipmentRoute.EstimatedArrival = shipRoute.eta;
                    //        shipmentRoute.EstimatedDeparture = shipRoute.etd;
                    //        date.Value = shipRoute.eta;
                    //        date.Type = DateType.Arrival;
                    //        date.IsEstimate = true;
                    //        date.IsEstimateSpecified = true;
                    //        shipmentDateColl.Add(date);
                    //    }
                    //    if ((shipRoute.sequence_id == 1 && shipRoutes > 1))
                    //    {
                    //        shipmentRoute.EstimatedArrival = shipRoute.eta;
                    //        //   subShipment.PortOfOrigin = new UNLOCO { Code = route.departure_locode };
                    //        shipmentRoute.EstimatedDeparture = shipRoute.etd;
                    //        Date date = new Date();
                    //        date.Value = shipRoute.etd;
                    //        date.Type = DateType.Departure;
                    //        date.IsEstimate = true;
                    //        date.IsEstimateSpecified = true;
                    //        shipmentDateColl.Add(date);
                    //    }
                    //    shipmentRoute.LegOrder = (sbyte)shipRoute.sequence_id;
                    //    //subShipment.PortOfDestination = new UNLOCO { Code = route.destination_locode };
                    //    shipmentRoute.PortOfLoading = new UNLOCO { Code = shipRoute.departure_locode };
                    //    shipmentRoute.PortOfDischarge = new UNLOCO { Code = shipRoute.destination_locode };
                    //    shipmentRoute.LegTypeSpecified = true;
                    //    subShipment.DateCollection = shipmentDateColl.ToArray();
                    //    switch (shipRoute.mode_code.ToUpper())
                    //    {
                    //        case "OCEAN":
                    //            shipmentRoute.TransportMode = TransportLegTransportMode.Sea;
                    //            break;
                    //        case "AIR":
                    //            shipmentRoute.TransportMode = TransportLegTransportMode.Air;

                    //            break;
                    //        case "RAIL":
                    //            shipmentRoute.TransportMode = TransportLegTransportMode.Rail;
                    //            break;
                    //    }
                    //    shipmentRoute.TransportModeSpecified = true;
                    //    shipRoutes = house.Routings.Length;
                    //    shipmentRoute.VesselName = shipRoute.vessel_name;
                    //    shipmentRoute.VesselLloydsIMO = shipRoute.vessel_code;
                    //    shipmentRoute.VoyageFlightNo = flightPrefix + shipRoute.flight_voyage_no;
                    //    //shipmentRouteColl.Add(shipmentRoute);
                    //}

                    subShipment.TransportLegCollection = shipmentRouteColl.ToArray();
                    if (house.load_type != "FCL")
                    {

                    }

                    List<PackingLine> packCollection = GetHouseItems(house.Items);
                    //foreach (ItemsItem houseItem in house.Items)
                    //{
                    //    PackingLine pl = new PackingLine();
                    //    pl.GoodsDescription = houseItem.item_description;
                    //    pl.PackQty = houseItem.pieces;
                    //    pcCount += houseItem.pieces;
                    //    if (string.IsNullOrEmpty(houseItem.pieces_unit_type))
                    //    {
                    //        pl.PackType = new PackageType { Code = "PCE" };
                    //        pkgType = "PCE";
                    //    }
                    //    else
                    //    {
                    //        pl.PackType = new PackageType { Code = NodeResources.GetEnum("PACKAGE", houseItem.pieces_unit_type) };
                    //        pkgType = houseItem.pieces_unit_type;

                    //    }
                    //    subShipment.OuterPacksSpecified = true;
                    //    pl.MarksAndNos = houseItem.marks_numbers;
                    //    pl.PackQtySpecified = true;
                    //    pl.Weight = houseItem.gross_weight;
                    //    pl.WeightSpecified = true;
                    //    switch (house.weight_unit.ToUpper())
                    //    {
                    //        case "K":

                    //            pl.WeightUnit = new UnitOfWeight { Code = "KG" };
                    //            pl.WeightSpecified = true;
                    //            break;
                    //        case "L":

                    //            pl.WeightUnit = new UnitOfWeight { Code = "LB" };
                    //            pl.WeightSpecified = true;
                    //            break;
                    //        default:
                    //            pl.WeightUnit = new UnitOfWeight { Code = "KG" };
                    //            break;
                    //    }
                    //    if (houseItem.volume != 0)
                    //    {
                    //        pl.Volume = houseItem.volume;
                    //    }
                    //    else
                    //    {
                    //        pl.Volume = houseItem.cubic_volume / 1000000;
                    //    }
                    //    pl.VolumeSpecified = true;
                    //    try
                    //    {
                    //        switch (house.volume_unit.ToUpper())
                    //        {
                    //            case "M":
                    //                pl.VolumeUnit = new UnitOfVolume { Code = "M3" };

                    //                break;
                    //            case "F":
                    //                pl.VolumeUnit = new UnitOfVolume { Code = "CF" };

                    //                break;
                    //            default:
                    //                pl.VolumeUnit = new UnitOfVolume { Code = "M3" };
                    //                break;
                    //        }
                    //    }
                    //    catch (Exception)
                    //    {
                    //        pl.VolumeUnit = new UnitOfVolume { Code = "M3" };
                    //    }
                    //    if (houseItem.container_no != null && house.mode_code == "AIR")
                    //    {
                    //        Container cnt = new Container();
                    //        pl.ContainerNumber = houseItem.container_no.Replace(" ", string.Empty);
                    //        cnt.ContainerNumber = pl.ContainerNumber;
                    //        cnt.Seal = houseItem.seal_no.ToString().Trim();
                    //        if (!string.IsNullOrEmpty(cnt.ContainerNumber))
                    //        {
                    //            contCollection.Add(cnt);
                    //        }
                    //    }
                    //    if (houseItem.Dimensions != null)
                    //    {

                    //        foreach (ItemsItem.ItemsItemDimensionsDimension dim in houseItem.Dimensions)
                    //        //foreach (Items. dim in houseItem.Dimensions)
                    //        {
                    //            if (dim.unit_type.Trim() == "C")
                    //            {
                    //                if (dim.height > 0)
                    //                {
                    //                    pl.Height = dim.height / 100;
                    //                }
                    //                else
                    //                {
                    //                    pl.Height = 0;
                    //                }

                    //                if (dim.width > 0)
                    //                {
                    //                    pl.Width = dim.width / 100;
                    //                }
                    //                else
                    //                {
                    //                    pl.Width = 0;
                    //                }

                    //                if (pl.Length > 0)
                    //                {
                    //                    pl.Length = dim.length / 100;
                    //                }
                    //                else
                    //                {
                    //                    pl.Length = 0;
                    //                }

                    //                pl.LengthUnit = new UnitOfLength { Code = "M", Description = "Metres" };
                    //                pl.LengthSpecified = true;
                    //                pl.HeightSpecified = true;
                    //                pl.WidthSpecified = true;
                    //            }
                    //            else
                    //            {
                    //            }
                    //        }
                    //    }
                    //    packCollection.Add(pl);
                    //}
                    subShipment.PackingLineCollection = packCollection.ToArray();
                    result.Add(subShipment);
                    subShipment.OuterPacks = TallyPieces(house.Items);
                    subShipment.OuterPacksPackageType = new PackageType { Code = pceType };
                    subShipment.OuterPacksSpecified = true;
                    subShipment.TotalNoOfPieces = subShipment.OuterPacks;
                    subShipment.TotalNoOfPiecesSpecified = true;
                    if (house.Items != null)
                    {
                        List<Container> contCollection = UpdateContainerDetails(GetContainersFromLines(house.Items), house.containers);

                        if (contCollection.Count > 0)
                        {
                            subShipment.ContainerCollection = new ShipmentContainerCollection { Container = contCollection.ToArray() };
                            subShipment.ContainerCount = contCollection.Count;
                            subShipment.ContainerCollection.Content = CollectionContent.Partial;
                            subShipment.ContainerCollection.ContentSpecified = true;
                        }
                    }

                }
                catch (Exception ex)
                {
                    //ProcessingErrors procerror = new ProcessingErrors();
                    //CTCErrorCode error = new CTCErrorCode();
                    //error.Code = NodeError.e111;
                    //error.Description = "Serialization Error :" + ex.Message;
                    //error.Severity = "Warning";
                    //procerror.ErrorCode = error;
                    //procerror.SenderId = string.Empty;
                    //procerror.RecipientId = string.Empty;
                    //procerror.FileName = xmlFile;
                    //NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                    ////NodeResources.AddProcessingError(procerror);
                    ////thisResult.FolderLoc = Globals.glFailPath;
                    ////thisResult.Processed = true;
                    ////processed = true;
                    string strEx = ex.GetType().Name;
                    NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node " + strEx + " Exception Found: Housebill Error:" + ex.Message + ". " + "", Color.Red);
                    string errMsg = "Exception " + ex.GetType().Name + Environment.NewLine +
                        ex.StackTrace;

                    MailModule.sendMsg("", Globals.glAlertsTo, "Error in " + m.Name, errMsg);
                    _errorMessage = "Exception Found: " + m.Name + "Params: " + m.GetParameters();

                }
            }
            return result;
        }

        private List<Container> UpdateContainerDetails(List<Container> containerList, containersContainer[] containers)
        {
            if (containers != null)
            {


                foreach (var container in containerList)
                {
                    var powerCont = (from c in containers
                                     where c.container_no == container.ContainerNumber
                                     select c).FirstOrDefault();
                    if (powerCont != null)
                    {
                        container.GrossWeight = powerCont.gross_weight;
                        container.GrossWeightSpecified = true;
                        container.VolumeUnit = new UnitOfVolume { Code = "M3" };

                    }
                }
            }
            return containerList;
        }

        private List<Container> GetContainersFromLines(ItemsItem[] items)
        {
            List<Container> contColl = new List<Container>();

            foreach (var item in items)
            {
                if (item.container_no != null)
                {
                    Container cnt = new Container();


                    cnt.ContainerNumber = item.container_no.Replace(" ", string.Empty);
                    cnt.Seal = item.seal_no.ToString().Trim();
                    cnt.ContainerType = new ContainerContainerType { Code = GetCWEnums("CONTAINER", item.container_code) };

                    if (!string.IsNullOrEmpty(cnt.ContainerNumber))
                    {
                        contColl.Add(cnt);
                    }
                }
            }
            return contColl;

        }

        private int TallyPieces(ItemsItem[] items)
        {
            var lines = from l in items
                        select l.pieces;
            int count = 0;
            foreach (var l in lines)
            {
                count += l;
            }
            return count;
        }
        private List<PackingLine> GetHouseItems(ItemsItem[] items)
        {
            List<PackingLine> plColl = new List<PackingLine>();
            foreach (var item in items)
            {
                int pcCount = 0;
                string pkgType = string.Empty;
                PackingLine pl = new PackingLine();
                pl.GoodsDescription = item.item_description;
                pl.PackQty = item.pieces;
                pcCount += item.pieces;
                if (string.IsNullOrEmpty(item.pieces_unit_type))
                {
                    pl.PackType = new PackageType { Code = "PCE" };
                    pkgType = "PCE";
                }
                else
                {
                    pceType = GetCWEnums("PACKAGE", item.pieces_unit_type);
                    pl.PackType = new PackageType { Code = pceType };
                    pkgType = item.pieces_unit_type;

                }
                pl.MarksAndNos = item.marks_numbers;
                pl.PackQtySpecified = true;
                pl.Weight = item.gross_weight;
                pl.WeightSpecified = true;
                switch (item.weight_unit.ToUpper())
                {
                    case "K":

                        pl.WeightUnit = new UnitOfWeight { Code = "KG" };
                        pl.WeightSpecified = true;
                        break;
                    case "L":

                        pl.WeightUnit = new UnitOfWeight { Code = "LB" };
                        pl.WeightSpecified = true;
                        break;
                    default:
                        pl.WeightUnit = new UnitOfWeight { Code = "KG" };
                        break;
                }
                if (item.volume != 0)
                {
                    pl.Volume = item.volume;
                }
                else
                {
                    pl.Volume = item.cubic_volume / 1000000;
                }
                pl.VolumeSpecified = true;
                try
                {
                    switch (item.volume_unit.ToUpper())
                    {
                        case "M":
                            pl.VolumeUnit = new UnitOfVolume { Code = "M3" };

                            break;
                        case "F":
                            pl.VolumeUnit = new UnitOfVolume { Code = "CF" };

                            break;
                        default:
                            pl.VolumeUnit = new UnitOfVolume { Code = "M3" };
                            break;
                    }
                }
                catch (Exception)
                {
                    pl.VolumeUnit = new UnitOfVolume { Code = "M3" };
                }

                if (item.Dimensions != null)
                {

                    foreach (ItemsItem.ItemsItemDimensionsDimension dim in item.Dimensions)
                    //foreach (Items. dim in houseItem.Dimensions)
                    {
                        if (dim.unit_type.Trim() == "C")
                        {
                            if (dim.height > 0)
                            {
                                pl.Height = dim.height / 100;
                            }
                            else
                            {
                                pl.Height = 0;
                            }

                            if (dim.width > 0)
                            {
                                pl.Width = dim.width / 100;
                            }
                            else
                            {
                                pl.Width = 0;
                            }

                            if (pl.Length > 0)
                            {
                                pl.Length = dim.length / 100;
                            }
                            else
                            {
                                pl.Length = 0;
                            }

                            pl.LengthUnit = new UnitOfLength { Code = "M", Description = "Metres" };
                            pl.LengthSpecified = true;
                            pl.HeightSpecified = true;
                            pl.WidthSpecified = true;
                        }
                        else
                        {
                        }
                    }
                }
                plColl.Add(pl);
            }
            return plColl;
        }

        private string FixNumber(string bl)
        {
            string result;
            if (bl.Contains("/"))
            {
                result = bl.Replace("/", "-");
            }
            else
            {
                result = bl;
            }

            return result;
        }

        private ProcessResult ProcessCargowiseFiles(string xmlFile)
        {
            FileInfo fi = new FileInfo(xmlFile);

            NodeResources.AddRTBText(rtbLog, "Cargowise File Processing: " + fi.Name);
            ProcessResult thisResult = new ProcessResult();
            FileInfo processingFile = new FileInfo(xmlFile);
            thisResult.Processed = true;
            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            String eventCode = String.Empty;
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            cwRx++;
            NodeResources.AddLabel(lblCwRx, cwRx.ToString());
            TransReference trRef = new TransReference();
            String archiveFile;
            String nativeString = String.Empty;
            UniversalInterchange toConvert;
            Shipment consol = new Shipment();
            try
            {
                using (FileStream fStream = new FileStream(xmlFile, FileMode.Open))
                {
                    XmlSerializer cwConvert = new XmlSerializer(typeof(UniversalInterchange));
                    toConvert = (UniversalInterchange)cwConvert.Deserialize(fStream);
                    fStream.Close();
                }
                consol = toConvert.Body.BodyField.Shipment;
                senderID = toConvert.Header.SenderID;
                recipientID = toConvert.Header.RecipientID;
                reasonCode = consol.DataContext.ActionPurpose.Code;
                eventCode = consol.DataContext.EventType.Code;
            }
            catch (Exception ex)
            {
                ProcessingErrors procerror = new ProcessingErrors();
                CTCErrorCode error = new CTCErrorCode();
                error.Code = NodeError.e110;
                error.Description = "Error Processing NDM File. :" + ex.Message;
                error.Severity = "Warning";
                procerror.ErrorCode = error;
                procerror.SenderId = senderID;
                procerror.RecipientId = recipientID;
                procerror.FileName = xmlFile;
                //                AddProcessingError(procerror);
                thisResult.FolderLoc = Globals.glFailPath;
                thisResult.Processed = false;
                thisResult.FileName = xmlFile;
                return thisResult;
            }
            CustomerProfile customerProfile = new CustomerProfile(Globals.ConnString.ConnString);
            var custProfile = customerProfile.GetCustomerProfile(senderID, recipientID, reasonCode, eventCode, "Y");
            if (custProfile != null)
            {
                if (custProfile.C_IS_ACTIVE == "N" | custProfile.C_ON_HOLD == "Y")
                {
                    string f = NodeResources.MoveFile(xmlFile, Path.Combine(custProfile.C_PATH, "Held"), rtbLog);
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    { xmlFile = f; }

                    NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfile.C_NAME + " is not Active or is on Hold. Moving to Hold and skipping" + "");
                    thisResult.Processed = true;
                    thisResult.FileName = xmlFile;
                    return thisResult;
                }
                else
                {
                    //Send file to Archive folder
                    archiveFile = Globals.ArchiveFile(Globals.glArcLocation, xmlFile);
                    DirectoryInfo di = new DirectoryInfo(Path.Combine(Globals.glPickupPath, recipientID + reasonCode));
                    if (!di.Exists)
                    {
                        di.Create();
                    }



                    Type custType = this.GetType();
                    MethodInfo custMethodToRun = custType.GetMethod(custProfile.P_METHOD);
                    var varResult = custMethodToRun.Invoke(this, new Object[] { toConvert, xmlFile });
                    if (varResult != null)
                    {
                        thisResult.Processed = true;
                    }
                    NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Profile : " + custProfile.P_DESCRIPTION);
                    string f = NodeResources.MoveFile(xmlFile, Path.Combine(Globals.glPickupPath, recipientID + reasonCode), rtbLog);
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);

                    }
                    xmlFile = f;

                }
            }

            File.Delete(xmlFile);
            return thisResult;

        }

        public TransReference AddCargowiseData(UniversalInterchange uinter, string fileName)
        {
            Shipment consol = uinter.Body.BodyField.Shipment;
            TransReference trRef = new TransReference();
            MasterHouseRef mhRef = new MasterHouseRef();
            ConfirmShipments confirmShipments = new ConfirmShipments(Globals.ConnString.ConnString);
            var consolNo = GetKeyNo(consol.DataContext.DataSourceCollection, "ForwardingConsol");
            if (consolNo == null)
            {
                // No consol in file Just Shipments. 
                var bl = GetBillNo(consol, "HWB");
                trRef.Reference1 = bl != null ? bl : string.Empty;
                trRef.Ref1Type = TransReference.RefType.Housebill;
                trRef.Reference2 = GetKeyNo(consol.DataContext.DataSourceCollection, "ForwardingShipment");
                trRef.Ref2Type = TransReference.RefType.Shipment;
                return trRef;
            }
            var newConsol = confirmShipments.AddConsolData(consol);
            if (consol.SubShipmentCollection != null)
            {
                confirmShipments.AddShipmentData(consol.SubShipmentCollection, newConsol);
                List<Shipment> shipmentColl = consol.SubShipmentCollection.ToList();
                mhRef.HouseBills = GetHouseBillsFromShipments(shipmentColl);
            }

            try
            {

                mhRef.Master = consol.WayBillNumber;
                mhRef.CWCode = uinter.Header.SenderID;
                mhRef.Country = consol.DataContext.Company.Country.Code;
                TransactionControl tc = new TransactionControl(Globals.connString());
                //var 
                Transaction trans = new Transaction
                {
                    T_COUNTRY = consol.DataContext.Company.Country.Code,
                    T_CWCODE = uinter.Header.SenderID,
                    T_CWFILECREATED = false,
                    T_DATETIME = DateTime.Now,
                    T_DIRECTION = "R",
                    T_FILENAME = fileName,
                    T_REF1 = consol.WayBillNumber,
                    T_REF2 = mhRef.HouseBills != null ? mhRef.HouseBills.ToCommaList() : string.Empty,
                    T_REF1TYPE = "Master",
                    T_REF2TYPE = "House",
                    T_REF3 = consolNo,
                    T_REF3TYPE = "Consol"
                };
                tc.AddTransaction(trans);

                trRef.Ref1Type = TransReference.RefType.Master;
                trRef.Reference1 = consol.WayBillNumber;


                if (consol.WayBillType.Code == "MWB")
                {
                    trRef.Ref1Type = TransReference.RefType.Master;
                    trRef.Reference1 = consol.WayBillNumber;
                    trRef.Ref1Type = TransReference.RefType.Master;
                }




            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                NodeResources.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error found when importing Cargowise data. Error was: " + ex.InnerException.Message);

                MailModule.sendMsg("", Globals.glAlertsTo, "Error Converting file to PCFS XML", strEx + " Error found when importing Cargowise data. Error was: " + ex.InnerException.Message);

                // thisResult.FolderLoc = Globals.glFailPath;
                // thisResult.Processed = false;
                // thisResult.FileName = xmlFile;
            }

            return trRef;
        }

        private List<string> GetHouseBillsFromShipments(List<Shipment> shipmentColl)
        {
            List<string> hbills = new List<string>();
            hbills = (from x in shipmentColl
                      where x.WayBillType.Code == "HWB"
                      select x.WayBillNumber).ToList();
            return hbills;
        }

        private string GetBillNo(Shipment consol, string billType)
        {
            var wbType = consol.WayBillType.Code;
            if (wbType == billType)
            {
                return consol.WayBillNumber;
            }
            return null;
        }

        private string GetKeyNo(DataSource[] dataSourceCollection, string dsType)
        {
            return (from d in dataSourceCollection
                    where d.Type == dsType
                    select d.Key).FirstOrDefault();
        }

        private void ProcessCustomXML(string XMLFile)
        {


        }

        private void customMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEnums Enums = new frmEnums();
            Enums.Show();

        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            this.tslMain.Width = this.Width / 2;
            this.tslSpacer.Width = (this.Width / 2) - (productionToolStripMenuItem.Width + tslCmbMode.Width);
        }

        private void productionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (productionToolStripMenuItem.Checked)
            {
                testingToolStripMenuItem.Checked = false;
                tslMode.Text = "Production";
            }
            else
            {
                testingToolStripMenuItem.Checked = true;
                tslMode.Text = "Testing";
            }
        }

        private void testingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (testingToolStripMenuItem.Checked)
            {
                productionToolStripMenuItem.Checked = false;
                tslMode.Text = "Testing";
            }
            else
            {
                productionToolStripMenuItem.Checked = true;
                tslMode.Text = "Production";
            }
        }

        private void eAdapterTestToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}

