﻿using NNR_Satellite.DTO;
using NNR_Satellite.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace NNR_Satellite
{
    public partial class frmEnums : Form
    {

        public Guid cw_ID;
        public frmEnums()
        {
            InitializeComponent();
        }

        private void frmEnums_Load(object sender, EventArgs e)
        {
            LoadData();
        }


        private void LoadData()
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteEntity(Globals.ConnString.ConnString)))
            {
                List<Cargowise_Enums> enums = uow.CargowiseEnums.GetAll().OrderBy(x => x.CW_ENUMTYPE).ToList();
                dgEnums.DataSource = enums;

            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private Cargowise_Enums FindEnums(Cargowise_Enums enums)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteEntity(Globals.ConnString.ConnString)))
            {
                var found = (uow.CargowiseEnums.Find(x => x.CW_ENUM == enums.CW_ENUM
                && x.CW_ENUMTYPE == x.CW_ENUMTYPE
                && x.CW_MAPVALUE == x.CW_MAPVALUE)).FirstOrDefault();
                return found;
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {

            using (IUnitOfWork uow = new UnitOfWork(new SatelliteEntity(Globals.ConnString.ConnString)))
            {
                if (!string.IsNullOrEmpty(edCargowiseValue.Text) && !string.IsNullOrEmpty(edCustomValue.Text) && !string.IsNullOrEmpty(edDatatype.Text))
                {
                    Cargowise_Enums enums = new Cargowise_Enums
                    {
                        CW_ID = cw_ID != Guid.Empty ? cw_ID : Guid.NewGuid(),
                        CW_ENUM = edCargowiseValue.Text,
                        CW_ENUMTYPE = edDatatype.Text,
                        CW_MAPVALUE = edCustomValue.Text
                    };
                    if (FindEnums(enums) == null)
                    {
                        uow.CargowiseEnums.Add(enums);
                    }
                    else
                    {
                        var editEnums = uow.CargowiseEnums.Get(cw_ID);
                        editEnums.CW_ENUM = edCargowiseValue.Text;
                        editEnums.CW_ENUMTYPE = edDatatype.Text;
                        editEnums.CW_MAPVALUE = edCustomValue.Text;
                    }
                    uow.Complete();

                    cw_ID = Guid.Empty;
                    MessageBox.Show("Record Added Successfully", "Custom Mapping Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadData();
                }
            }

        }

        private void dgEnums_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            cw_ID = (Guid)dgEnums[0, e.RowIndex].Value;
            edCargowiseValue.Text = dgEnums["CWValue", e.RowIndex].Value.ToString();
            edCustomValue.Text = dgEnums["Custom", e.RowIndex].Value.ToString();
            edDatatype.Text = dgEnums["Type", e.RowIndex].Value.ToString();
        }

        private void bbNew_Click(object sender, EventArgs e)
        {
            edDatatype.Text = "";
            edCustomValue.Text = "";
            edCargowiseValue.Text = "";
            cw_ID = Guid.Empty;
            edDatatype.Focus();
        }

        private void edDatatype_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ActiveControl != null)
                {
                    this.SelectNextControl(this.ActiveControl, true, true, true, true);
                }
                e.Handled = true;
            }
        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
